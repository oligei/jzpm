//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.vo.jzpm.pa07;

import nc.itf.jz.pub.IBillCode;
import nc.vo.pub.SuperVO;
import nc.vo.pub.lang.UFDate;
import nc.vo.pub.lang.UFDateTime;
import nc.vo.pub.lang.UFDouble;

/**
 * Decompile by HMX 2019-3-28
 * 添加了个序列号、vreserve50
 * 注意：该VO的字段和数据表jzpm_pa_pettyfee中未同步,代码明显遗失
 *       对应的操作类包名也是不规范的
 * */
public class PaPettyfeeVO extends SuperVO implements IBillCode {
 
	private static final long serialVersionUID = 1L;//add by HMX 2019-3-28
	private String pk_corp;
    private String vdef4;
    private String vreserve5;
    private String vreserve50;//add by HMX 2019-3-28
    private String vsourcebillno;
    private UFDate dsourcebilldate;
    private Integer dr;
    private String pk_project;
    private String voperatorid;
    private String vapprovenote;
    private UFDateTime ts;
    private String vreserve9;
    private String pk_basetype;
    private String cfreecustid;
    private String pk_deptdoc;
    private Integer ipaytype;
    private String pk_cubasdoc;
    private String vdef3;
    private String pk_ouraccount;
    private String vreserve6;
    private String vreserve10;
    private String pk_settle;
    private String vreserve3;
    private String vreserve4;
    private String vdef10;
    private String pk_inuser;
    private String vdef5;
    private String pk_otheraccount;
    private String vsourcepay;
    private UFDouble nbaserate;
    private String pk_psndoc;
    private String vreserve8;
    private UFDate dhappendate;
    private Integer ifeebilltype;
    private UFDate dmakedate;
    private UFDouble nbasemny;
    private String vdef7;
    private String pk_billtype;
    private Integer vbillstatus;
    private String vreserve2;
    private String vdef2;
    private String vmemo;
    private String pk_busitype;
    private String vdef1;
    private String pk_pettyfee;
    private String vbillno;
    private UFDate dbilldate;
    private String vreserve1;
    private String pk_origintype;
    private String pk_noconttype;
    private String vreserve7;
    private String vdef9;
    private String pk_cumandoc;
    private UFDouble noriginmny;
    private String vdef8;
    private UFDate dapprovedate;
    private String vapproveid;
    private String vdef6;
    private String vexplain;
    private String pk_inbasuser;
    private String vmodulecode;
    public static final String VMODULECODE = "vmodulecode";
    private String vtranstype;
    public static final String VTRANSTYPE = "vtranstype";
    private UFDouble npaidoriginmny;
    private UFDouble npaidbasemny;
    public static final String PK_CORP = "pk_corp";
    public static final String VDEF4 = "vdef4";
    public static final String VRESERVE5 = "vreserve5";
    public static final String VRESERVE50 = "vreserve50";
    public static final String VSOURCEBILLNO = "vsourcebillno";
    public static final String DSOURCEBILLDATE = "dsourcebilldate";
    public static final String PK_PROJECT = "pk_project";
    public static final String VOPERATORID = "voperatorid";
    public static final String VAPPROVENOTE = "vapprovenote";
    public static final String VRESERVE9 = "vreserve9";
    public static final String PK_BASETYPE = "pk_basetype";
    public static final String CFREECUSTID = "cfreecustid";
    public static final String PK_DEPTDOC = "pk_deptdoc";
    public static final String IPAYTYPE = "ipaytype";
    public static final String PK_CUBASDOC = "pk_cubasdoc";
    public static final String VDEF3 = "vdef3";
    public static final String PK_OURACCOUNT = "pk_ouraccount";
    public static final String VRESERVE6 = "vreserve6";
    public static final String VRESERVE10 = "vreserve10";
    public static final String PK_SETTLE = "pk_settle";
    public static final String VRESERVE3 = "vreserve3";
    public static final String VRESERVE4 = "vreserve4";
    public static final String VDEF10 = "vdef10";
    public static final String PK_INUSER = "pk_inuser";
    public static final String VDEF5 = "vdef5";
    public static final String PK_OTHERACCOUNT = "pk_otheraccount";
    public static final String VSOURCEPAY = "vsourcepay";
    public static final String NBASERATE = "nbaserate";
    public static final String PK_PSNDOC = "pk_psndoc";
    public static final String VRESERVE8 = "vreserve8";
    public static final String DHAPPENDATE = "dhappendate";
    public static final String IFEEBILLTYPE = "ifeebilltype";
    public static final String DMAKEDATE = "dmakedate";
    public static final String NBASEMNY = "nbasemny";
    public static final String VDEF7 = "vdef7";
    public static final String PK_BILLTYPE = "pk_billtype";
    public static final String VBILLSTATUS = "vbillstatus";
    public static final String VRESERVE2 = "vreserve2";
    public static final String VDEF2 = "vdef2";
    public static final String VMEMO = "vmemo";
    public static final String PK_BUSITYPE = "pk_busitype";
    public static final String VDEF1 = "vdef1";
    public static final String PK_PETTYFEE = "pk_pettyfee";
    public static final String VBILLNO = "vbillno";
    public static final String DBILLDATE = "dbilldate";
    public static final String VRESERVE1 = "vreserve1";
    public static final String PK_ORIGINTYPE = "pk_origintype";
    public static final String PK_NOCONTTYPE = "pk_noconttype";
    public static final String VRESERVE7 = "vreserve7";
    public static final String VDEF9 = "vdef9";
    public static final String PK_CUMANDOC = "pk_cumandoc";
    public static final String NORIGINMNY = "noriginmny";
    public static final String VDEF8 = "vdef8";
    public static final String DAPPROVEDATE = "dapprovedate";
    public static final String VAPPROVEID = "vapproveid";
    public static final String VDEF6 = "vdef6";
    public static final String VEXPLAIN = "vexplain";
    public static final String PK_INBASUSER = "pk_inbasuser";
    public static final String NPAIDORIGINMNY = "npaidoriginmny";
    public static final String NPAIDBASEMNY = "npaidbasemny";

    public UFDouble getNpaidoriginmny() {
        return this.npaidoriginmny;
    }

    public void setNpaidoriginmny(UFDouble npaidoriginmny) {
        this.npaidoriginmny = npaidoriginmny;
    }

    public UFDouble getNpaidbasemny() {
        return this.npaidbasemny;
    }

    public void setNpaidbasemny(UFDouble npaidbasemny) {
        this.npaidbasemny = npaidbasemny;
    }

    public String getVmodulecode() {
        return this.vmodulecode;
    }

    public void setVmodulecode(String vmodulecode) {
        this.vmodulecode = vmodulecode;
    }

    public String getVtranstype() {
        return this.vtranstype;
    }

    public void setVtranstype(String vtranstype) {
        this.vtranstype = vtranstype;
    }

    public String getPk_corp() {
        return this.pk_corp;
    }

    public void setPk_corp(String newPk_corp) {
        this.pk_corp = newPk_corp;
    }

    public String getVdef4() {
        return this.vdef4;
    }

    public void setVdef4(String newVdef4) {
        this.vdef4 = newVdef4;
    }

    public String getVreserve5() {
        return this.vreserve5;
    }

    public void setVreserve5(String newVreserve5) {
        this.vreserve5 = newVreserve5;
    }
  //add by HMX 2019-3-28
    public String getVreserve50() {
        return this.vreserve50;
    }

    public void setVreserve50(String newVreserve50) {
        this.vreserve50 = newVreserve50;
    }
    //add end
    public String getVsourcebillno() {
        return this.vsourcebillno;
    }

    public void setVsourcebillno(String newVsourcebillno) {
        this.vsourcebillno = newVsourcebillno;
    }

    public UFDate getDsourcebilldate() {
        return this.dsourcebilldate;
    }

    public void setDsourcebilldate(UFDate newDsourcebilldate) {
        this.dsourcebilldate = newDsourcebilldate;
    }

    public Integer getDr() {
        return this.dr;
    }

    public void setDr(Integer newDr) {
        this.dr = newDr;
    }

    public String getPk_project() {
        return this.pk_project;
    }

    public void setPk_project(String newPk_project) {
        this.pk_project = newPk_project;
    }

    public String getVoperatorid() {
        return this.voperatorid;
    }

    public void setVoperatorid(String newVoperatorid) {
        this.voperatorid = newVoperatorid;
    }

    public String getVapprovenote() {
        return this.vapprovenote;
    }

    public void setVapprovenote(String newVapprovenote) {
        this.vapprovenote = newVapprovenote;
    }

    public UFDateTime getTs() {
        return this.ts;
    }

    public void setTs(UFDateTime newTs) {
        this.ts = newTs;
    }

    public String getVreserve9() {
        return this.vreserve9;
    }

    public void setVreserve9(String newVreserve9) {
        this.vreserve9 = newVreserve9;
    }

    public String getPk_basetype() {
        return this.pk_basetype;
    }

    public void setPk_basetype(String newPk_basetype) {
        this.pk_basetype = newPk_basetype;
    }

    public String getCfreecustid() {
        return this.cfreecustid;
    }

    public void setCfreecustid(String newCfreecustid) {
        this.cfreecustid = newCfreecustid;
    }

    public String getPk_deptdoc() {
        return this.pk_deptdoc;
    }

    public void setPk_deptdoc(String newPk_deptdoc) {
        this.pk_deptdoc = newPk_deptdoc;
    }

    public Integer getIpaytype() {
        return this.ipaytype;
    }

    public void setIpaytype(Integer newIpaytype) {
        this.ipaytype = newIpaytype;
    }

    public String getPk_cubasdoc() {
        return this.pk_cubasdoc;
    }

    public void setPk_cubasdoc(String newPk_cubasdoc) {
        this.pk_cubasdoc = newPk_cubasdoc;
    }

    public String getVdef3() {
        return this.vdef3;
    }

    public void setVdef3(String newVdef3) {
        this.vdef3 = newVdef3;
    }

    public String getPk_ouraccount() {
        return this.pk_ouraccount;
    }

    public void setPk_ouraccount(String newPk_ouraccount) {
        this.pk_ouraccount = newPk_ouraccount;
    }

    public String getVreserve6() {
        return this.vreserve6;
    }

    public void setVreserve6(String newVreserve6) {
        this.vreserve6 = newVreserve6;
    }

    public String getVreserve10() {
        return this.vreserve10;
    }

    public void setVreserve10(String newVreserve10) {
        this.vreserve10 = newVreserve10;
    }

    public String getPk_settle() {
        return this.pk_settle;
    }

    public void setPk_settle(String newPk_settle) {
        this.pk_settle = newPk_settle;
    }

    public String getVreserve3() {
        return this.vreserve3;
    }

    public void setVreserve3(String newVreserve3) {
        this.vreserve3 = newVreserve3;
    }

    public String getVreserve4() {
        return this.vreserve4;
    }

    public void setVreserve4(String newVreserve4) {
        this.vreserve4 = newVreserve4;
    }

    public String getVdef10() {
        return this.vdef10;
    }

    public void setVdef10(String newVdef10) {
        this.vdef10 = newVdef10;
    }

    public String getPk_inuser() {
        return this.pk_inuser;
    }

    public void setPk_inuser(String newPk_inuser) {
        this.pk_inuser = newPk_inuser;
    }

    public String getVdef5() {
        return this.vdef5;
    }

    public void setVdef5(String newVdef5) {
        this.vdef5 = newVdef5;
    }

    public String getPk_otheraccount() {
        return this.pk_otheraccount;
    }

    public void setPk_otheraccount(String newPk_otheraccount) {
        this.pk_otheraccount = newPk_otheraccount;
    }

    public String getVsourcepay() {
        return this.vsourcepay;
    }

    public void setVsourcepay(String newVsourcepay) {
        this.vsourcepay = newVsourcepay;
    }

    public UFDouble getNbaserate() {
        return this.nbaserate;
    }

    public void setNbaserate(UFDouble newNbaserate) {
        this.nbaserate = newNbaserate;
    }

    public String getPk_psndoc() {
        return this.pk_psndoc;
    }

    public void setPk_psndoc(String newPk_psndoc) {
        this.pk_psndoc = newPk_psndoc;
    }

    public String getVreserve8() {
        return this.vreserve8;
    }

    public void setVreserve8(String newVreserve8) {
        this.vreserve8 = newVreserve8;
    }

    public UFDate getDhappendate() {
        return this.dhappendate;
    }

    public void setDhappendate(UFDate newDhappendate) {
        this.dhappendate = newDhappendate;
    }

    public Integer getIfeebilltype() {
        return this.ifeebilltype;
    }

    public void setIfeebilltype(Integer newIfeebilltype) {
        this.ifeebilltype = newIfeebilltype;
    }

    public UFDate getDmakedate() {
        return this.dmakedate;
    }

    public void setDmakedate(UFDate newDmakedate) {
        this.dmakedate = newDmakedate;
    }

    public UFDouble getNbasemny() {
        return this.nbasemny;
    }

    public void setNbasemny(UFDouble newNbasemny) {
        this.nbasemny = newNbasemny;
    }

    public String getVdef7() {
        return this.vdef7;
    }

    public void setVdef7(String newVdef7) {
        this.vdef7 = newVdef7;
    }

    public String getPk_billtype() {
        return this.pk_billtype;
    }

    public void setPk_billtype(String newPk_billtype) {
        this.pk_billtype = newPk_billtype;
    }

    public Integer getVbillstatus() {
        return this.vbillstatus;
    }

    public void setVbillstatus(Integer newVbillstatus) {
        this.vbillstatus = newVbillstatus;
    }

    public String getVreserve2() {
        return this.vreserve2;
    }

    public void setVreserve2(String newVreserve2) {
        this.vreserve2 = newVreserve2;
    }

    public String getVdef2() {
        return this.vdef2;
    }

    public void setVdef2(String newVdef2) {
        this.vdef2 = newVdef2;
    }

    public String getVmemo() {
        return this.vmemo;
    }

    public void setVmemo(String newVmemo) {
        this.vmemo = newVmemo;
    }

    public String getPk_busitype() {
        return this.pk_busitype;
    }

    public void setPk_busitype(String newPk_busitype) {
        this.pk_busitype = newPk_busitype;
    }

    public String getVdef1() {
        return this.vdef1;
    }

    public void setVdef1(String newVdef1) {
        this.vdef1 = newVdef1;
    }

    public String getPk_pettyfee() {
        return this.pk_pettyfee;
    }

    public void setPk_pettyfee(String newPk_pettyfee) {
        this.pk_pettyfee = newPk_pettyfee;
    }

    public String getVbillno() {
        return this.vbillno;
    }

    public void setVbillno(String newVbillno) {
        this.vbillno = newVbillno;
    }

    public UFDate getDbilldate() {
        return this.dbilldate;
    }

    public void setDbilldate(UFDate newDbilldate) {
        this.dbilldate = newDbilldate;
    }

    public String getVreserve1() {
        return this.vreserve1;
    }

    public void setVreserve1(String newVreserve1) {
        this.vreserve1 = newVreserve1;
    }

    public String getPk_origintype() {
        return this.pk_origintype;
    }

    public void setPk_origintype(String newPk_origintype) {
        this.pk_origintype = newPk_origintype;
    }

    public String getPk_noconttype() {
        return this.pk_noconttype;
    }

    public void setPk_noconttype(String newPk_noconttype) {
        this.pk_noconttype = newPk_noconttype;
    }

    public String getVreserve7() {
        return this.vreserve7;
    }

    public void setVreserve7(String newVreserve7) {
        this.vreserve7 = newVreserve7;
    }

    public String getVdef9() {
        return this.vdef9;
    }

    public void setVdef9(String newVdef9) {
        this.vdef9 = newVdef9;
    }

    public String getPk_cumandoc() {
        return this.pk_cumandoc;
    }

    public void setPk_cumandoc(String newPk_cumandoc) {
        this.pk_cumandoc = newPk_cumandoc;
    }

    public UFDouble getNoriginmny() {
        return this.noriginmny;
    }

    public void setNoriginmny(UFDouble newNoriginmny) {
        this.noriginmny = newNoriginmny;
    }

    public String getVdef8() {
        return this.vdef8;
    }

    public void setVdef8(String newVdef8) {
        this.vdef8 = newVdef8;
    }

    public UFDate getDapprovedate() {
        return this.dapprovedate;
    }

    public void setDapprovedate(UFDate newDapprovedate) {
        this.dapprovedate = newDapprovedate;
    }

    public String getVapproveid() {
        return this.vapproveid;
    }

    public void setVapproveid(String newVapproveid) {
        this.vapproveid = newVapproveid;
    }

    public String getVdef6() {
        return this.vdef6;
    }

    public void setVdef6(String newVdef6) {
        this.vdef6 = newVdef6;
    }

    public String getVexplain() {
        return this.vexplain;
    }

    public void setVexplain(String newVexplain) {
        this.vexplain = newVexplain;
    }

    public String getPk_inbasuser() {
        return this.pk_inbasuser;
    }

    public void setPk_inbasuser(String pk_inbasuser) {
        this.pk_inbasuser = pk_inbasuser;
    }

    public String getParentPKFieldName() {
        return null;
    }

    public String getPKFieldName() {
        return "pk_pettyfee";
    }

    public String getTableName() {
        return "jzpm_pa_pettyfee";
    }

    public PaPettyfeeVO() {
    }

    public String[] getBillCodeArray() {
        return null;
    }
}
