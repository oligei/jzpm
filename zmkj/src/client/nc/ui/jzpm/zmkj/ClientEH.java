package nc.ui.jzpm.zmkj;

import java.math.BigDecimal;
import java.util.HashMap;

import javax.swing.JOptionPane;

import nc.ui.trade.base.IBillOperate;
import nc.bs.framework.common.NCLocator;
import nc.bs.logging.Logger;
import nc.bs.pub.billcodemanage.BillcodeGenerater;
import nc.itf.uap.IUAPQueryBS;
import nc.itf.uap.pf.IWorkflowMachine;
import nc.jdbc.framework.processor.ArrayProcessor;
import nc.jdbc.framework.processor.MapProcessor;
import nc.pub.jzpm.btn.ICustomerButton;
import nc.ui.pub.ButtonObject;
import nc.ui.pub.ClientEnvironment;
import nc.ui.pub.pf.PfUtilClient;
import nc.ui.trade.controller.IControllerBase;
import nc.ui.trade.manage.BillManageUI;
import nc.ui.trade.manage.ManageEventHandler;
import nc.vo.jzpm.pa05.PaPayapplyBVO;
import nc.vo.jzpm.pa05.PaPayapplyVO;
import nc.vo.jzpm.pa05.PayapplyAggVO;
import nc.vo.jzpm.zmkj.AggProjPayVO;
import nc.vo.jzpm.zmkj.ProjPayBVO;
import nc.vo.jzpm.zmkj.ProjPayVO;
import nc.vo.pub.AggregatedValueObject;
import nc.vo.pub.BusinessException;
import nc.vo.pub.CircularlyAccessibleValueObject;
import nc.vo.pub.billcodemanage.BillCodeObjValueVO;
import nc.vo.pub.lang.UFBoolean;
import nc.vo.pub.lang.UFDouble;

/**
 * 资金计划
 * */
public class ClientEH extends ManageEventHandler {

	public ClientEH(BillManageUI billUI, IControllerBase control) {
		super(billUI, control);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onBoLineDel() throws Exception {
		// TODO 自动生成的方法存根
		
		super.onBoLineDel();
		int rowcount=getBillCardPanelWrapper().getBillCardPanel().getBillModel().getRowCount();
		UFDouble appamountsum=new UFDouble(0);
		for(int i=0;i<rowcount;i++)
		{
			Object whetherag= getBillCardPanelWrapper().getBillCardPanel().getBillModel().getValueAt(i, "whetherag");
			if((Boolean) whetherag)
			{
				UFDouble appamount=(UFDouble) getBillCardPanelWrapper().getBillCardPanel().getBillModel().getValueAt(i, "appamount");
				if(appamount!=null)
					appamountsum=appamountsum.add(appamount);
			}
		}
		getBillCardPanelWrapper().getBillCardPanel().setHeadItem("appamountsum", appamountsum);
	
	}
	
	protected void onBoElse(int intBtn) throws Exception{
		super.onBoElse(intBtn);
		switch(intBtn){
		case ICustomerButton.RefAdd:
			onBillRef(intBtn);
			break;
		default:
			break;
		}
	}
	
	
	@Override
	public void onBoApproveInfo() throws Exception {
		// TODO 自动生成的方法存根
		super.onBoApproveInfo();
	}
	
	@Override
	protected void onBoSave() throws Exception {
		// TODO 自动生成的方法存根
		getBillCardPanelWrapper().getBillCardPanel().dataNotNullValidate();//验证必填项
		String itemno = (String) getBillCardPanelWrapper().getBillCardPanel().getHeadItem("itemno").getValueObject();
		String billdate = (String) getBillCardPanelWrapper().getBillCardPanel().getHeadItem("dbilldate").getValueObject();
		String vbillno = (String) getBillCardPanelWrapper().getBillCardPanel().getHeadItem("vbillno").getValueObject();
		
		if("".equals(itemno)||itemno==null){
			throw new Exception( "项目编码不能为空");
		}
		if("".equals(billdate)||itemno==null){
			throw new Exception( "单据日期不能为空");
		}
		if(vbillno==null||vbillno.length()<=0)
		{
			String billcode=(new BillcodeGenerater()).getBillCode("zfjh", ClientEnvironment.getInstance().getCorporation().getPrimaryKey(), null, null);

			getBillCardPanelWrapper().getBillCardPanel().setHeadItem("vbillno", billcode);
		}
		
		
		super.onBoSave();
		
		
	}
	@Override
	protected void onBoEdit() throws Exception {
		// TODO 自动生成的方法存根
		canUpdate();
		super.onBoEdit();
	}
	
	private void canUpdate() throws BusinessException 
	{
		
		Integer vbillstatus = (Integer) getBillCardPanelWrapper().getBillCardPanel().getHeadItem("vbillstatus").getValueObject();
		
		if(vbillstatus==2)
		{
			String pk = getBillUI().getBufferData().getCurrentVOClone().getParentVO().getPrimaryKey();
			String sql="select count(1)"
					+ "  from pub_workflownote"
					+ " where billid = '"+pk+"'"
					+ "   and checkman = '"+ClientEnvironment.getInstance().getUser().getPrimaryKey()+"'"
					+ "   and (workflow_type is null or workflow_type = 1)"
					+ "   and actiontype <> 'BIZ'"
					+ "   and approvestatus in (0, 2)"
					+ " order by senddate";

				Object[] tmp=  (Object[]) NCLocator.getInstance().lookup(IUAPQueryBS.class).executeQuery(sql, new ArrayProcessor());
				if(tmp!=null&&tmp.length>0)
				{
					if((Integer)tmp[0]<1)//表示是当前登录人可以审核单据
					{
						throw new BusinessException("当前操作员没有修改单据的权限，或弃审后再进行修改");
					}
				}
		}
	}
	
	
	public void onBillRef(int intBtn) throws Exception {
		// TODO 自动生成的方法存根
		ButtonObject btn=getButtonManager().getButton(intBtn);
		btn.setTag("99Q1"+":");
//		edit by wsw at 2018-03-28 需求变更，由付款申请（按结算） 变更为 付款申请（按合同）
//		btn.setTag("99Q4"+":");
		onBoBusiTypeAdd(btn, null);
		btn.setTag(String.valueOf(intBtn));
		
		if(!PfUtilClient.isCloseOK()){
			return;
		}
		
	}
	
	private final void onBoBusiTypeAdd(ButtonObject bo,String sourceBillId) throws Exception{
		getBusiDelegator().childButtonClicked(bo, _getCorp().getPrimaryKey(),getBillUI()._getModuleCode() , _getOperator(), 
				getUIController().getBillType(), getBillUI(), getBillUI().getUserObject(), sourceBillId);//
		if(PfUtilClient.makeFlag){
			getBillUI().setCardUIState();
			getBillUI().setBillOperate(1);
		} else if(PfUtilClient.isCloseOK()){
			if(isDataChange())
				setRefData(PfUtilClient.getRetOldVos());
			else
				setRefData(PfUtilClient.getRetOldVos());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void setRefData(AggregatedValueObject[] vos) throws Exception {
		// TODO 自动生成的方法存根
		getBillUI().setCardUIState();
		
		AggProjPayVO vo=(AggProjPayVO) refVOChange(vos);
		if(vo==null)
			throw new BusinessException(nc.ui.ml.NCLangRes.getInstance().getStrByID("uifactory", "UPPuifactory-000084"));
		
		AggProjPayVO aggvo=vo;// JXToJD(vo);
		//设置为新增处理
		getBillUI().setBillOperate(IBillOperate.OP_REFADD);
		//填充界面
		getBillCardPanelWrapper().setCardData(aggvo);
		
		getBillCardPanelWrapper().getBillCardPanel().execHeadTailLoadFormulas();
		for(int i=0;i<getBillCardPanelWrapper().getBillCardPanel().getBillModel().getRowCount();i++)
		{
			getBillCardPanelWrapper().getBillCardPanel().getBillModel().execEditFormulas(i);//执行编辑公式
			//表体行合同主键
			String  pk_contract = (String) getBillCardPanelWrapper().getBillCardPanel().getBillModel().getValueAt(i, "vdef1");
			//累计付款金额
			UFDouble ljfkje= (UFDouble) getBillCardPanelWrapper().getBillCardPanel().getBillModel().getValueAt(i, "contrasubmoney");
			//累计结算金额
			UFDouble ljjsje= (UFDouble) getBillCardPanelWrapper().getBillCardPanel().getBillModel().getValueAt(i, "contraaccmoney");
			////////////////add by HMX 2019-5-24
			if(ljjsje==null && pk_contract!=null){//统计到货入库中,项目物资采购合同的价税合计
				StringBuilder sumsql = new StringBuilder("select cast(sum(vdef4) AS NUMBER (10, 2)) sum from po_arriveorder_b where corderid in(");
				sumsql.append("select distinct b.corderid from po_order_b b where ccontractid=(");
				sumsql.append(" select pk_ct_manage from ct_manage where ct_code=(");
				sumsql.append(" select vbillno from jzpm_cm_contract where pk_contract='"+pk_contract+"' and nvl(dr, 0) = 0) and nvl(dr, 0) = 0");
				sumsql.append(") and nvl(b.dr, 0) = 0  ) and nvl(dr, 0) = 0");
				try{
					IUAPQueryBS query = (IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class);
				    HashMap<String,BigDecimal> map =  (HashMap<String, BigDecimal>) query.executeQuery(sumsql.toString(), new MapProcessor());
				    if(map!=null){
				    	BigDecimal sum = map.get("sum");
				    	if(sum!=null){
				    		UFDouble summny = new UFDouble(sum);
				    		ljjsje=summny;
				    		getBillCardPanelWrapper().getBillCardPanel().getBillModel().setValueAt(summny, i, "contraaccmoney");
				    	}
				    }
				}catch (Exception e) {
					//System.out.println(e);//出现了异常，不能影响单据的正常进行
				}
			}
            //////////////// add end
			if(ljfkje==null)
				ljfkje=new UFDouble(0);
			
			if(ljjsje==null)
				ljjsje=new UFDouble(0);
			
			if(ljjsje.doubleValue()!=0)
			{
				getBillCardPanelWrapper().getBillCardPanel().getBillModel().setValueAt(ljfkje.multiply(100).div(ljjsje, 2)+"%", i, "fkbl");
			}
		}

	}
}
