/*      */ package nc.ui.jzpm.pa05;
/*      */ 
/*      */ import java.awt.Color;
/*      */ import java.util.Collection;
/*      */ import java.util.HashMap;
/*      */ import java.util.Map;
/*      */ import java.util.Observable;
/*      */ import nc.bs.framework.common.NCLocator;
/*      */ import nc.bs.logging.Logger;
/*      */ import nc.itf.jzfdc.pub.ISysInitQryService;
/*      */ import nc.itf.jzpm.pub.IJZPMPubBusi;
/*      */ import nc.itf.uap.IUAPQueryBS;
/*      */ import nc.itf.uap.bd.cust.ICustBankQry;
/*      */ import nc.jdbc.framework.processor.ColumnProcessor;
/*      */ import nc.ui.bd.ref.UFRefManage;
/*      */ import nc.ui.bd.ref.busi.BankAccCustDefaultRefModel;
/*      */ import nc.ui.jz.pub.DeptPsnRela;
/*      */ import nc.ui.jz.pub.bodymenue.BodyImgButtonClick;
/*      */ import nc.ui.jz.pub.bodymenue.BodyMenuItemClick;
/*      */ import nc.ui.jz.pub.buttonstate.BillLinkBtnVO;
/*      */ import nc.ui.jz.pub.buttonstate.LinkQueryPfBtnVO;
/*      */ import nc.ui.jzpm.cmpub.contract.ProjContRela;
/*      */ import nc.ui.jzpm.core.buttonstate.ButtonFactory;
/*      */ import nc.ui.jzpm.core.buttonstate.OthdeductAddLineBtn;
/*      */ import nc.ui.jzpm.core.buttonstate.PayApplyDeductBtnVO;
/*      */ import nc.ui.jzpm.core.buttonstate.PrePaydeductAddLineBtn;
/*      */ import nc.ui.jzpm.core.pub.ref.ContractRefTreeModel;
/*      */ import nc.ui.jzpm.papub.pay.PayCFreeCustUtil;
/*      */ import nc.ui.jzpm.pub.ref.BdFeeRefModel;
/*      */ import nc.ui.jzpm.pub.ui.BillManageUI;
/*      */ import nc.ui.pub.ButtonObject;
/*      */ import nc.ui.pub.beans.MessageDialog;
/*      */ import nc.ui.pub.beans.UIRefPane;
/*      */ import nc.ui.pub.beans.UITable;
/*      */ import nc.ui.pub.bill.BillCardBeforeEditListener;
/*      */ import nc.ui.pub.bill.BillCardPanel;
/*      */ import nc.ui.pub.bill.BillEditEvent;
/*      */ import nc.ui.pub.bill.BillItem;
/*      */ import nc.ui.pub.bill.BillItemEvent;
/*      */ import nc.ui.pub.bill.BillListPanel;
/*      */ import nc.ui.pub.bill.BillModel;
/*      */ import nc.ui.pub.bill.BillScrollPane;
/*      */ import nc.ui.pub.linkoperate.ILinkApproveData;
/*      */ import nc.ui.pub.linkoperate.ILinkMaintainData;
/*      */ import nc.ui.scm.freecust.UFRefGridUI;
/*      */ import nc.ui.trade.bill.AbstractManageController;
/*      */ import nc.ui.trade.bill.BillCardPanelWrapper;
/*      */ import nc.ui.trade.bill.BillListPanelWrapper;
/*      */ import nc.ui.trade.bsdelegate.BusinessDelegator;
/*      */ import nc.ui.trade.buffer.BillUIBuffer;
/*      */ import nc.ui.trade.button.ButtonManager;
/*      */ import nc.ui.trade.button.ButtonVOFactory;
/*      */ import nc.ui.trade.manage.ManageEventHandler;
/*      */ import nc.vo.bd.CorpVO;
/*      */ import nc.vo.jz.pub.SafeCompute;
/*      */ import nc.vo.jzpm.bdh240.PayMode;
/*      */ import nc.vo.jzpm.cmpub.contract.CmContractVO;
/*      */ import nc.vo.jzpm.pa05.PaPayapplyVO;
/*      */ import nc.vo.jzpm.papub.pay.PaSettleVO;
/*      */ import nc.vo.jzpm.pub.JZPMProxy;
/*      */ import nc.vo.jzpm.pub.cmpub.IContract;
/*      */ import nc.vo.jzpm.pub.cmpub.IPayMode;
/*      */ import nc.vo.jzpm.pub.cmpub.Ifundproperty;
/*      */ import nc.vo.jzpm.sub1005.SubSettleOtduVO;
/*      */ import nc.vo.jzpm.sub1020.SubTaxVO;
/*      */ import nc.vo.ml.AbstractNCLangRes;
/*      */ import nc.vo.ml.NCLangRes4VoTransl;
/*      */ import nc.vo.pub.AggregatedValueObject;
/*      */ import nc.vo.pub.BusinessException;
/*      */ import nc.vo.pub.SuperVO;
/*      */ import nc.vo.pub.lang.UFDate;
/*      */ import nc.vo.pub.lang.UFDouble;
/*      */ import nc.vo.trade.button.ButtonVO;
/*      */ import nc.vo.trade.field.IBillField;
/*      */ 
/*      */ public class ClientUI
/*      */   extends BillManageUI implements BillCardBeforeEditListener
/*      */ {
/*      */   private static final long serialVersionUID = 1L;
/*   80 */   private PaSettleVO[] svos = null;
/*      */   
/*   82 */   private DeptPsnRela deptPsnRela = null;
/*      */   
/*   84 */   private ProjContRela m_projContRela = null;
/*      */   
/*   86 */   private Object m_applyOriMny = null;
/*      */   
/*   88 */   protected BodyMenuItemClick mc = null;
/*   89 */   protected BodyImgButtonClick img = null;
/*      */   
/*   91 */   private CmContractVO contractVO = null;
/*      */   
/*      */   public DeptPsnRela getDeptPsnRela() {
/*   94 */     if (deptPsnRela == null) {
/*   95 */       BillItem deptItem = getBillCardPanel().getHeadItem("pk_deptdoc");
/*   96 */       BillItem psnItem = getBillCardPanel().getHeadItem("pk_psndoc");
/*   97 */       deptPsnRela = new DeptPsnRela(deptItem, psnItem);
/*      */     }
/*   99 */     return deptPsnRela;
/*      */   }
/*      */   
/*      */   protected ProjContRela getProjContRela() {
/*  103 */     if (m_projContRela == null) {
/*  104 */       BillItem pk_project = getBillCardPanel().getHeadItem("pk_project");
/*  105 */       BillItem pk_contract = getBillCardPanel().getHeadItem("pk_contract");
/*  106 */       m_projContRela = new ProjContRela(pk_project, pk_contract);
/*      */     }
/*  108 */     return m_projContRela;
/*      */   }
/*      */   
/*      */   public ClientUI()
/*      */   {
/*  113 */     init();
/*      */   }
/*      */   
/*      */   public ClientUI(Boolean useBillSource)
/*      */   {
/*  118 */     super(useBillSource);
/*  119 */     init();
/*      */   }
/*      */   
/*      */   public ClientUI(String pk_corp, String pk_billType, String pk_busitype, String operater, String billId)
/*      */   {
/*  124 */     super(pk_corp, pk_billType, pk_busitype, operater, billId);
/*      */   }
/*      */   
/*      */   private void init() {
/*  128 */     getBillCardPanel().getBillTable("jzpm_pa_payapply_b").removeSortListener();
/*      */     
/*  130 */     getBillCardPanel().getBillModel("jzpm_pa_payapply_b").setSortColumn("subvbillno");
/*      */   }
/*      */   
/*      */   protected AbstractManageController createController()
/*      */   {
/*  135 */     return new ClientCtrl();
/*      */   }
/*      */   
/*      */   protected ManageEventHandler createEventHandler()
/*      */   {
/*  140 */     return new ClientEventHandler(this, getUIControl());
/*      */   }
/*      */   
/*      */   protected void initPrivateButton()
/*      */   {
/*  145 */     OthdeductAddLineBtn othdeductAddLineBtn = new OthdeductAddLineBtn();
/*  146 */     addPrivateButton(othdeductAddLineBtn.getButtonVO());
/*      */     
/*  148 */     PrePaydeductAddLineBtn prePaydeductAddLineBtn = new PrePaydeductAddLineBtn();
/*  149 */     addPrivateButton(prePaydeductAddLineBtn.getButtonVO());
/*      */     
/*  151 */     ButtonVO lineBtnVo = ButtonVOFactory.getInstance().build(10);
/*      */     
/*  153 */     lineBtnVo.setChildAry(new int[] { 11, 12 });
/*  154 */     addPrivateButton(lineBtnVo);
/*      */     
/*  156 */     ButtonVO closedBtn = ButtonFactory.getPaCloseBtn(new int[] { 2 });
/*      */     
/*  158 */     addPrivateButton(closedBtn);
/*      */     
/*  160 */     ButtonVO unclosedBtn = ButtonFactory.getPaUnCloseBtn(new int[] { 2 });
/*      */     
/*  162 */     addPrivateButton(unclosedBtn);
/*      */     
/*  164 */     super.initPrivateButton();
/*      */   }
/*      */   
/*      */   protected ButtonVO[] initAssFuncBtnVOs()
/*      */   {
/*  169 */     PayApplyDeductBtnVO ductBtn = new PayApplyDeductBtnVO();
/*  170 */     ductBtn.getButtonVO().setOperateStatus(new int[] { 0, 1 });
/*      */     
/*  172 */     return new ButtonVO[] { ductBtn.getButtonVO() };
/*      */   }
/*      */   
/*      */   public void bodyRowChange(BillEditEvent e)
/*      */   {
/*  177 */     super.bodyRowChange(e);
/*  178 */     if ((getBillOperate() != 1) && (getBillOperate() != 0))
/*      */     {
/*  180 */       return;
/*      */     }
/*      */     
/*  183 */     int row = e.getRow();
/*  184 */     if (row >= 0) {
/*  185 */       setBodyLineDelState(row);
/*      */     }
/*      */   }
/*      */   
/*      */   public void setlineState(int row) {
/*  190 */     ButtonObject delLineBtn = getButtonManager().getButton(12);
/*      */     
/*  192 */     if (checkIsContLine(row)) {
/*  193 */       delLineBtn.setEnabled(false);
/*      */     } else {
/*  195 */       delLineBtn.setEnabled(true);
/*      */     }
/*      */     try
/*      */     {
/*  199 */       updateButtonUI();
/*      */     } catch (Exception e1) {
/*  201 */       Logger.error(e1);
/*      */     }
/*      */   }
/*      */   
/*      */   public void setBodyLineDelState(int row)
/*      */   {
/*  207 */     ButtonObject delLineBtn = getButtonManager().getButton(12);
/*      */     
/*  209 */     int ifundproperty = getIFundproperty(row);
/*  210 */     Boolean bisnew = getBodyBooleanValue(row, "bisnew");
/*  211 */     if (ifundproperty == 3) {
/*  212 */       delLineBtn.setEnabled(false);
/*  213 */     } else if ((ifundproperty == 4) && (bisnew != null) && (!bisnew.booleanValue())) {
/*  214 */       delLineBtn.setEnabled(false);
/*      */     } else {
/*  216 */       delLineBtn.setEnabled(true);
/*      */     }
/*      */     try {
/*  219 */       updateButtonUI();
/*      */     } catch (Exception e1) {
/*  221 */       Logger.error(e1);
/*      */     }
/*      */   }
/*      */   
/*      */   protected void initSelfData()
/*      */   {
/*  227 */     initContRef();
/*  228 */     initComBox();
/*  229 */     initFreeCustRef();
/*      */   }
/*      */   
/*      */   private void initFreeCustRef()
/*      */   {
/*  234 */     ((UIRefPane)getBillCardPanel().getHeadItem("cfreecustid").getComponent()).getRef().setRefUI(new UFRefGridUI(this));
/*      */   }
/*      */   
/*      */   public void setDefaultData()
/*      */     throws Exception
/*      */   {
/*  240 */     super.setDefaultData();
/*  241 */     getDeptPsnRela().setOnAdd();
/*      */     
/*  243 */     initBistogl();
/*      */   }
/*      */   
/*      */   public boolean isLoadBodyImgBtns()
/*      */   {
/*  248 */     return true;
/*      */   }
/*      */   
/*      */   public boolean isLoadBodyPopMenu()
/*      */   {
/*  253 */     return true;
/*      */   }
/*      */   
/*      */   private void initComBox()
/*      */   {
/*  258 */     getBillCardWrapper().initHeadComboBox("ipaymode", IPayMode.IPAYMODE, true);
/*      */     
/*  260 */     getBillListWrapper().initHeadComboBox("ipaymode", IPayMode.IPAYMODE, true);
/*      */     
/*  262 */     getBillCardWrapper().initHeadComboBox("icontstage", IContract.ICONTSTAGE, true);
/*      */     
/*  264 */     getBillListWrapper().initHeadComboBox("icontstage", IContract.ICONTSTAGE, true);
/*      */     
/*  266 */     getBillCardWrapper().initBodyComboBox("ifundproperty", Ifundproperty.IFUNDPROPERTY, true);
/*      */     
/*  268 */     getBillListWrapper().initBodyComboBox("ifundproperty", Ifundproperty.IFUNDPROPERTY, true);
/*      */     
/*  270 */     getBillCardWrapper().initBodyComboBox("ipaymode", PayMode.getNames(), true);
/*      */     
/*  272 */     getBillListWrapper().initBodyComboBox("ipaymode", PayMode.getNames(), true);
/*      */   }
/*      */   
/*      */   private void initContRef()
/*      */   {
/*  277 */     int[] conttype = { 1, 3, 4, 6 };
/*      */     
/*  279 */     UIRefPane refPanel = (UIRefPane)getBillCardPanel().getHeadItem("pk_contract").getComponent();
/*      */     
/*  281 */     ContractRefTreeModel contractRefTreeModel = new ContractRefTreeModel(conttype);
/*      */     
/*  283 */     contractRefTreeModel.clearCacheData();
/*  284 */     refPanel.setRefModel(contractRefTreeModel);
/*      */     
/*  286 */     UIRefPane projectrefPanel = (UIRefPane)getBillCardPanel().getHeadItem("pk_project").getComponent();
/*      */   }
/*      */   
/*      */   public void initBistogl()
/*      */   {
/*  291 */     String funcode = "H51705";
/*  292 */     ISysInitQryService service = (ISysInitQryService)NCLocator.getInstance().lookup(ISysInitQryService.class.getName());
/*      */     try
/*      */     {
/*  295 */       boolean[] nodeFlags = service.queryJZFinanceSysInitInfo(funcode, _getCorp().getPk_corp());
/*      */       
/*  297 */       if ((nodeFlags != null) && (nodeFlags.length > 0)) {
/*  298 */         getBillCardPanel().setHeadItem("bisupload", Boolean.valueOf(nodeFlags[0]));
/*      */       }
/*      */     } catch (BusinessException e) {
/*  301 */       Logger.error("查询是否传会记平台出错：", e);
/*      */     }
/*      */   }
/*      */   
/*      */   public void afterEdit(BillEditEvent e)
/*      */   {
/*  307 */     String key = e.getKey();
/*  308 */     if (e.getPos() == 0) {
/*  309 */       afterHeadEdit(key);
/*      */     } else {
/*  311 */       afterBodyEdit(key);
/*      */     }
/*      */   }
/*      */   
/*      */   private void afterHeadEdit(String key) {
/*  316 */     if (key.equalsIgnoreCase("pk_psndoc")) {
/*  317 */       getDeptPsnRela().setDeptByPsn();
/*  318 */     } else if (key.equalsIgnoreCase("pk_deptdoc")) {
/*  319 */       getDeptPsnRela().setPsnByDept();
/*      */     }
/*  321 */     else if (key.equalsIgnoreCase("pk_cusmandoc")) {
/*  322 */       getBillCardPanel().getHeadItem("cfreecustid").setValue(null);
/*  323 */       PayCFreeCustUtil.setVHeadseconedAccidAttri(getBillCardPanel(), "pk_cusbasdoc", "cfreecustid", "pk_secondaccid", "vsecondaccid");
/*      */       
/*  325 */       execHeadCFreeCustFormulas();
/*      */     }
/*  327 */     else if ("ncontpayscale".equalsIgnoreCase(key)) {
/*  328 */       calcNcontshouldmny();
/*  329 */     } else if ("ipaymode".equalsIgnoreCase(key)) {
/*  330 */       calcNcontshouldmny();
/*  331 */     } else if ("napplyoriginmny".equalsIgnoreCase(key)) {
/*  332 */       afterEditNApplyOriginMny();
/*  333 */     } else if ("icontstage".equalsIgnoreCase(key)) {
/*  334 */       afterEditIContStage();
/*  335 */       calcNcontshouldmny();
/*  336 */     } else if ("cfreecustid".equalsIgnoreCase(key)) {
/*  337 */       execHeadCFreeCustFormulas();
/*  338 */     } else if ("pk_secondaccid".equalsIgnoreCase(key)) {
/*  339 */       execHeadSecondAccidFormulas();
/*      */     }
/*  341 */     getBillCardPanel().execHeadEditFormulas();
/*      */   }
/*      */   
/*      */   public void afterEditContract(String pk_contract) {
/*  345 */     cleanInfoWhileChangeContract();
/*  346 */     pk_contractAfterEdit(pk_contract);
/*  347 */     checkIsExistHZ(pk_contract);
/*  348 */     setMaxDigitBeforeUpdate();
/*  349 */     int row = getBillCardPanel().getBillTable().getSelectedRow();
/*  350 */     setlineState(row);
/*  351 */     updateBillShowDigits();
/*  352 */     PayCFreeCustUtil.setVHeadseconedAccidAttri(getBillCardPanel(), "pk_cusbasdoc", "cfreecustid", "pk_secondaccid", "vsecondaccid");
/*      */     
/*  354 */     getBillCardPanel().execHeadEditFormulas();
/*      */   }
/*      */   
/*      */   private void afterEditIContStage()
/*      */   {
/*  359 */     Object o = getBillCardPanel().getHeadItem("icontstage").getValueObject();
/*  360 */     if (!isNull(o)) {
/*  361 */       switch (new Integer(o.toString()).intValue()) {
/*      */       case 0: 
/*  363 */         getBillCardPanel().setHeadItem("nprecontpayscale", contractVO.getNdisfshpaidscale());
/*  364 */         getBillCardPanel().setHeadItem("ncontpayscale", contractVO.getNdisfshpaidscale());
/*  365 */         break;
/*      */       case 1: 
/*  367 */         getBillCardPanel().setHeadItem("nprecontpayscale", contractVO.getNfshedpaidscale());
/*  368 */         getBillCardPanel().setHeadItem("ncontpayscale", contractVO.getNfshedpaidscale());
/*  369 */         break;
/*      */       case 2: 
/*  371 */         getBillCardPanel().setHeadItem("nprecontpayscale", contractVO.getNrepairdpaidscale());
/*  372 */         getBillCardPanel().setHeadItem("ncontpayscale", contractVO.getNrepairdpaidscale());
/*  373 */         break;
/*      */       default: 
/*  375 */         getBillCardPanel().setHeadItem("nprecontpayscale", null);
/*  376 */         getBillCardPanel().setHeadItem("ncontpayscale", null);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void checkIsExistHZ(String pk_contract) {
/*  382 */     String condition = " isnull(dr,0)=0 and pk_billtype='99Q1' and pk_contract='" + pk_contract + "'";
/*      */     try {
/*  384 */       Collection vos = JZPMProxy.getIJZPMPubBusi().queryByCondition(PaPayapplyVO.class, condition, new String[] { "pk_payapply" });
/*      */       
/*  386 */       if ((vos == null) || (vos.size() == 0)) {
/*  387 */         MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000002"), " 您确认对该合同做汇算清理吗？\n提醒：一旦做过汇算清理就不再支持对单期进行付款了！");
/*      */       }
/*      */     }
/*      */     catch (BusinessException e) {
/*  391 */       Logger.error("查询合同是否存在付款申请单（汇总）时出错：", e);
/*      */     }
/*      */   }
/*      */   
/*      */   private void afterEditNApplyOriginMny()
/*      */   {
/*  397 */     setHeadBasemny("napplybasemny", getHeadDoubleValue("napplyoriginmny"));
/*      */   }
/*      */   
/*      */   protected void setApplyOriMny(Object obj) {
/*  401 */     m_applyOriMny = obj;
/*      */   }
/*      */   
/*      */   protected boolean checkNapplyoriginmny() {
/*  405 */     UFDouble napplyoriginmny = getHeadDoubleValue("napplyoriginmny");
/*  406 */     UFDouble ncanapplyoriginmny = getHeadDoubleValue("ncanapplyoriginmny");
/*  407 */     if (SafeCompute.compare(ncanapplyoriginmny, napplyoriginmny) < 0) {
/*  408 */       MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000002"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000016"));
/*      */       
/*  410 */       return false;
/*      */     }
/*  412 */     return true;
/*      */   }
/*      */   
/*      */   protected boolean checkNapplyoriginmny(UFDouble temp)
/*      */   {
/*  417 */     UFDouble ncanapplyoriginmny = getHeadDoubleValue("ncanapplyoriginmny");
/*  418 */     if (SafeCompute.compare(ncanapplyoriginmny, temp) < 0) {
/*  419 */       MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000002"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000016"));
/*      */       
/*  421 */       return false;
/*      */     }
/*  423 */     return true;
/*      */   }
/*      */   
/*      */   protected boolean checkNapplyoriginmny2()
/*      */   {
/*  428 */     UFDouble napplyoriginmny = getHeadDoubleValue("napplyoriginmny");
/*      */     
/*  430 */     UFDouble ncanapplyoriginmny = getHeadDoubleValue("ncanapplyoriginmny");
/*  431 */     if (SafeCompute.compare(ncanapplyoriginmny, napplyoriginmny) < 0)
/*      */     {
/*  433 */       return MessageDialog.showYesNoDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000002"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000017")) == 4;
/*      */     }
/*      */     
/*  436 */     return true;
/*      */   }
/*      */   
/*      */   private boolean checkThisApplyDate()
/*      */   {
/*  441 */     UFDate dthisapplydate = getHeadDateValue("dthisapplydate");
/*  442 */     UFDate dlastapplydate = getHeadDateValue("dlastapplydate");
/*  443 */     if ((dthisapplydate != null) && (dlastapplydate != null) && (dthisapplydate.compareTo(dlastapplydate) < 0))
/*      */     {
/*  445 */       MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000002"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000018"));
/*      */       
/*  447 */       getBillCardPanel().setHeadItem("dthisapplydate", null);
/*  448 */       return false;
/*      */     }
/*  450 */     return true;
/*      */   }
/*      */   
/*      */   private void setHeadBasemny(String field, UFDouble mny)
/*      */   {
/*  455 */     getBillCardPanel().setHeadItem(field, getBaseMny(mny));
/*      */   }
/*      */   
/*      */   protected void calcBodyContLineNapplymny()
/*      */   {
/*  460 */     UFDouble nthisapplyoriginmny = getHeadDoubleValue("napplyoriginmny");
/*  461 */     UFDouble nthisapplybasemny = getHeadDoubleValue("napplybasemny");
/*      */     
/*  463 */     UFDouble nthismatedeductoriginmny = getHeadDoubleValue("nthismatedeductoriginmny");
/*  464 */     UFDouble nthismatedeductbasemny = getHeadDoubleValue("nthismatedeductbasemny");
/*      */     
/*  466 */     UFDouble nthisothdeductoriginmny = getHeadDoubleValue("nthisothdeductoriginmny");
/*  467 */     UFDouble nthisothdeductbasemny = getHeadDoubleValue("nthisothdeductbasemny");
/*      */     
/*  469 */     UFDouble napplyoriginmny = SafeCompute.add(SafeCompute.add(nthisapplyoriginmny, nthismatedeductoriginmny), nthisothdeductoriginmny);
/*      */     
/*  471 */     UFDouble napplybasemny = SafeCompute.add(SafeCompute.add(nthisapplybasemny, nthismatedeductbasemny), nthisothdeductbasemny);
/*      */     
/*  473 */     int rowCount = getBillCardPanel().getRowCount();
/*  474 */     for (int i = 0; i < rowCount; i++) {
/*  475 */       if (checkIsContLine(i)) {
/*  476 */         getBillCardPanel().setBodyValueAt(napplyoriginmny, i, "napplyoriginmny");
/*      */         
/*  478 */         getBillCardPanel().setBodyValueAt(napplybasemny, i, "napplybasemny");
/*      */         
/*  480 */         getBillCardPanel().getBillModel().setRowState(i, 2);
/*      */         
/*  482 */         break;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   protected void calcBodyContLineNapplymny2() {}
/*      */   
/*      */   protected void setBodyItemsEditable(int row)
/*      */   {
/*  492 */     if (row < 0) {
/*  493 */       return;
/*      */     }
/*  495 */     getBillCardPanel().getBillModel().setCellEditable(row, "fyxx", true);
/*      */     
/*  497 */     getBillCardPanel().getBillModel().setCellEditable(row, "vmemo", true);
/*      */     
/*  499 */     if (!checkIsContLine(row)) {
/*  500 */       getBillCardPanel().getBillModel().setCellEditable(row, "napplyoriginmny", true);
/*      */     }
/*      */     else
/*      */     {
/*  504 */       getBillCardPanel().getBillModel().setCellEditable(row, "napplyoriginmny", true);
/*      */     }
/*      */   }
/*      */   
/*      */   protected void setBodyCellEditable(int row, int iproperty, boolean bisnew)
/*      */   {
/*  510 */     if (row < 0) {
/*  511 */       return;
/*      */     }
/*  513 */     getBillCardPanel().getBillModel().setCellEditable(row, "fyxx", true);
/*      */     
/*  515 */     getBillCardPanel().getBillModel().setCellEditable(row, "vmemo", true);
/*      */     
/*  517 */     if ((iproperty == 4) && (!bisnew)) {
/*  518 */       getBillCardPanel().getBillModel().setCellEditable(row, "napplyoriginmny", false);
/*      */     } else {
/*  520 */       getBillCardPanel().getBillModel().setCellEditable(row, "napplyoriginmny", true);
/*      */     }
/*      */   }
/*      */   
/*      */   protected UFDouble getHeadDoubleValue(String field) {
/*  525 */     Object o = getBillCardPanel().getHeadItem(field).getValueObject();
/*  526 */     if ((o != null) && (o.toString().trim().length() > 0)) {
/*  527 */       return new UFDouble(o.toString());
/*      */     }
/*  529 */     return new UFDouble(0);
/*      */   }
/*      */   
/*      */   protected int getHeadIntValue(String field) {
/*  533 */     String o = getHeadItemString(field);
/*  534 */     if ((o != null) && (o.trim().length() > 0)) {
/*  535 */       return Integer.parseInt(o);
/*      */     }
/*  537 */     return -1;
/*      */   }
/*      */   
/*      */   public boolean getHeadItemBoolean(String field)
/*      */   {
/*  542 */     String o = getHeadItemString(field);
/*  543 */     return (o != null) && (o.trim().length() > 0) && (("Y".equalsIgnoreCase(o)) || ("true".equalsIgnoreCase(o)));
/*      */   }
/*      */   
/*      */   protected UFDate getHeadDateValue(String field) {
/*  547 */     Object o = getBillCardPanel().getHeadItem(field).getValueObject();
/*  548 */     if ((o != null) && (o.toString().trim().length() > 0)) {
/*  549 */       return new UFDate(o.toString());
/*      */     }
/*  551 */     return null;
/*      */   }
/*      */   
/*      */   protected String getHeadStringValue(String field) {
/*  555 */     Object o = getBillCardPanel().getHeadItem(field).getValueObject();
/*  556 */     if ((o != null) && (o.toString().trim().length() > 0)) {
/*  557 */       return o.toString();
/*      */     }
/*  559 */     return null;
/*      */   }
/*      */   
/*      */   protected UFDouble getBodyDoubleValue(int row, String field) {
/*  563 */     Object o = getBillCardPanel().getBodyValueAt(row, field);
/*  564 */     if ((o != null) && (o.toString().trim().length() > 0)) {
/*  565 */       return new UFDouble(o.toString());
/*      */     }
/*  567 */     return new UFDouble(0);
/*      */   }
/*      */   
/*      */   protected String getBodyStringValue(int row, String field)
/*      */   {
/*  572 */     Object o = getBillCardPanel().getBodyValueAt(row, field);
/*  573 */     if ((o != null) && (o.toString().trim().length() > 0)) {
/*  574 */       return o.toString();
/*      */     }
/*  576 */     return null;
/*      */   }
/*      */   
/*      */   protected Boolean getBodyBooleanValue(int row, String field) {
/*  580 */     Object o = getBillCardPanel().getBodyValueAt(row, field);
/*  581 */     if (o != null) {
/*  582 */       return (Boolean)o;
/*      */     }
/*  584 */     return Boolean.valueOf(false);
/*      */   }
/*      */   
/*      */   protected void calcNcontshouldmny()
/*      */   {
/*  589 */     int ipaymode = getHeadIntValue("ipaymode");
/*      */     
/*  591 */     UFDouble nsettlesumoriginmny = getHeadDoubleValue("nsettlesumoriginmny");
/*      */     
/*  593 */     UFDouble nsettlesumbasemny = getHeadDoubleValue("nsettlesumbasemny");
/*      */     
/*  595 */     UFDouble nothdeductsumoriginmny = SafeCompute.add(getHeadDoubleValue("nothdeductsumoriginmny"), getHeadDoubleValue("npredeductsumoriginmny"));
/*      */     
/*  597 */     UFDouble nothdeductsumbasemny = SafeCompute.add(getHeadDoubleValue("nothdeductsumbasemny"), getHeadDoubleValue("npredeductsumbasemny"));
/*      */     
/*  599 */     UFDouble nmatedeductsumoriginmny = getHeadDoubleValue("nmatedeductsumoriginmny");
/*      */     
/*  601 */     UFDouble nmatedeductsumbasemny = getHeadDoubleValue("nmatedeductsumbasemny");
/*      */     
/*  603 */     UFDouble ncontpayscale = SafeCompute.div(getHeadDoubleValue("ncontpayscale"), new UFDouble(100));
/*      */     
/*  605 */     UFDouble ncontshouldoriginmny = calcNcontshouldnmny(ipaymode, nsettlesumoriginmny, nmatedeductsumoriginmny, nothdeductsumoriginmny, ncontpayscale);
/*      */     
/*  607 */     UFDouble ncontshouldbasemny = calcNcontshouldnmny(ipaymode, nsettlesumbasemny, nmatedeductsumbasemny, nothdeductsumbasemny, ncontpayscale);
/*      */     
/*  609 */     UFDouble nsumreserveoriginmny = getHeadDoubleValue("nsumreserveoriginmny");
/*  610 */     UFDouble nsumreservebasemny = getHeadDoubleValue("nsumreservebasemny");
/*      */     
/*  612 */     ncontshouldoriginmny = SafeCompute.sub(ncontshouldoriginmny, nsumreserveoriginmny);
/*  613 */     ncontshouldbasemny = SafeCompute.sub(ncontshouldbasemny, nsumreservebasemny);
/*      */     
/*  615 */     getBillCardPanel().setHeadItem("ncontshouldoriginmny", ncontshouldoriginmny);
/*      */     
/*  617 */     getBillCardPanel().setHeadItem("ncontshouldbasemny", ncontshouldbasemny);
/*      */   }
/*      */   
/*      */   private UFDouble calcNcontshouldnmny(int ipaymode, UFDouble nsettlesummny, UFDouble nmatedeductsummny, UFDouble nothdeductsummny, UFDouble ncontpayscale)
/*      */   {
/*  622 */     UFDouble ncontshouldmny = new UFDouble(0);
/*  623 */     switch (ipaymode) {
/*      */     case 0: 
/*      */       break;
/*      */     case 1: 
/*  627 */       ncontshouldmny = nsettlesummny;
/*  628 */       break;
/*      */     case 2: 
/*  630 */       ncontshouldmny = SafeCompute.sub(SafeCompute.sub(SafeCompute.multiply(nsettlesummny, ncontpayscale), nmatedeductsummny), nothdeductsummny);
/*      */       
/*  632 */       break;
/*      */     case 3: 
/*  634 */       ncontshouldmny = SafeCompute.sub(SafeCompute.multiply(SafeCompute.sub(nsettlesummny, nmatedeductsummny), ncontpayscale), nothdeductsummny);
/*      */       
/*  636 */       break;
/*      */     case 4: 
/*  638 */       ncontshouldmny = SafeCompute.multiply(SafeCompute.sub(SafeCompute.sub(nsettlesummny, nmatedeductsummny), nothdeductsummny), ncontpayscale);
/*      */     }
/*      */     
/*  641 */     return ncontshouldmny;
/*      */   }
/*      */   
/*      */   private void afterThisApplyDateEdit2()
/*      */   {
/*      */     try
/*      */     {
/*  648 */       setHeadSettleData2();
/*      */       
/*  650 */       createBodyLineByContract();
/*      */     } catch (BusinessException e) {
/*  652 */       Logger.error(e.getMessage(), e);
/*  653 */       showErrorMessage(NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000019") + e.getMessage());
/*      */     }
/*      */   }
/*      */   
/*      */   private void afterBodyEdit(String key)
/*      */   {
/*  659 */     if (key.equalsIgnoreCase("napplyoriginmny")) {
/*  660 */       int rownum = getBillCardPanel().getBillTable().getSelectedRow();
/*  661 */       int ifundproperty = getIFundproperty(rownum);
/*      */       
/*  663 */       if (ifundproperty == 3)
/*      */       {
/*  665 */         UFDouble napplyoriginmny = getBodyDoubleValue(rownum, "napplyoriginmny");
/*      */         
/*  667 */         UFDouble undudectmny = getUnDeductMatSettleMny();
/*  668 */         if (SafeCompute.sub(napplyoriginmny, undudectmny).doubleValue() > 0.0D) {
/*  669 */           getBillCardPanel().setBodyValueAt(undudectmny, rownum, "napplyoriginmny");
/*  670 */           getBillCardPanel().setBodyValueAt(getBaseMny(undudectmny), rownum, "napplybasemny");
/*  671 */           MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000002"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000020") + undudectmny.doubleValue() + NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000021"));
/*      */         }
/*      */         else
/*      */         {
/*  675 */           getBillCardPanel().setBodyValueAt(getBaseMny(napplyoriginmny), rownum, "napplybasemny");
/*      */         }
/*  677 */       } else if (ifundproperty == 6) {
/*  678 */         checkPreDeductMny(rownum);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  683 */     if (key.equalsIgnoreCase("napplyoriginmny"))
/*      */     {
/*  685 */       int rownum = getBillCardPanel().getBillTable().getSelectedRow();
/*      */       
/*  687 */       setBodybasemny(rownum, "napplybasemny", getBodyDoubleValue(rownum, "napplyoriginmny"));
/*      */       
/*  689 */       calcHeadDeductmny();
/*      */       
/*  691 */       calcBodyContLineNapplymny2();
/*      */       
/*  693 */       getBillCardPanel().execHeadEditFormulas();
/*      */       
/*  695 */       calcNcontshouldmny();
/*      */       
/*  697 */       getBillCardPanel().execHeadEditFormulas();
/*      */     }
/*      */   }
/*      */   
/*      */   private void checkPreDeductMny(int rownum)
/*      */   {
/*  703 */     UFDouble napplyoriginmny = getBodyDoubleValue(rownum, "napplyoriginmny");
/*      */     
/*  705 */     UFDouble prepaymny = getHeadDoubleValue("npredeductedoriginmny");
/*  706 */     for (int i = 0; i < getBillCardPanel().getBillTable().getRowCount(); i++) {
/*  707 */       if ((getIFundproperty(i) == 6) && (i != rownum)) {
/*  708 */         prepaymny = SafeCompute.add(prepaymny, getBodyDoubleValue(i, "napplyoriginmny"));
/*      */       }
/*      */     }
/*  711 */     UFDouble undudectmny = SafeCompute.sub(getHeadDoubleValue("nprepaysumoriginmny"), prepaymny);
/*  712 */     if (SafeCompute.sub(napplyoriginmny, undudectmny).doubleValue() > 0.0D) {
/*  713 */       getBillCardPanel().setBodyValueAt(undudectmny, rownum, "napplyoriginmny");
/*  714 */       getBillCardPanel().setBodyValueAt(getBaseMny(undudectmny), rownum, "napplybasemny");
/*  715 */       MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000002"), "预付款扣回中本期扣款金额不能大于剩余未扣款金额" + undudectmny.doubleValue() + NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000021"));
/*      */     }
/*      */     else
/*      */     {
/*  719 */       getBillCardPanel().setBodyValueAt(getBaseMny(napplyoriginmny), rownum, "napplybasemny");
/*      */     }
/*      */   }
/*      */   
/*      */   protected void calHeadApplyMny()
/*      */   {
/*  725 */     UFDouble napplyoriginmny_h = new UFDouble(0);
/*      */     
/*  727 */     UFDouble napplybasemny_h = new UFDouble(0);
/*      */     
/*  729 */     UFDouble napplyoriginmny_b = new UFDouble(0);
/*  730 */     UFDouble napplybasemny_b = new UFDouble(0);
/*  731 */     int ifundproperty = -1;
/*      */     
/*  733 */     int rowCount = getBillCardPanel().getRowCount();
/*  734 */     for (int i = 0; i < rowCount; i++)
/*      */     {
/*  736 */       ifundproperty = getIFundproperty(i);
/*      */       
/*  738 */       napplyoriginmny_b = getBodyDoubleValue(i, "napplyoriginmny");
/*      */       
/*  740 */       napplybasemny_b = getBodyDoubleValue(i, "napplybasemny");
/*      */       
/*  742 */       switch (ifundproperty) {
/*      */       case 0: 
/*      */       case 1: 
/*      */       case 2: 
/*      */       case 5: 
/*  747 */         napplyoriginmny_h = SafeCompute.add(napplyoriginmny_h, napplyoriginmny_b);
/*      */         
/*  749 */         napplybasemny_h = SafeCompute.add(napplybasemny_h, napplybasemny_b);
/*      */         
/*  751 */         break;
/*      */       case 3: 
/*  753 */         napplyoriginmny_h = SafeCompute.sub(napplyoriginmny_h, napplyoriginmny_b);
/*      */         
/*  755 */         napplybasemny_h = SafeCompute.sub(napplybasemny_h, napplybasemny_b);
/*      */         
/*  757 */         break;
/*      */       case 4: 
/*  759 */         napplyoriginmny_h = SafeCompute.sub(napplyoriginmny_h, napplyoriginmny_b);
/*      */         
/*  761 */         napplybasemny_h = SafeCompute.sub(napplybasemny_h, napplybasemny_b);
/*      */       }
/*      */       
/*      */     }
/*      */     
/*  766 */     getBillCardPanel().setHeadItem("napplyoriginmny", napplyoriginmny_h);
/*  767 */     setApplyOriMny(getHeadDoubleValue("napplyoriginmny"));
/*  768 */     setHeadBasemny("napplybasemny", (UFDouble)m_applyOriMny);
/*      */   }
/*      */   
/*      */   protected void checkNmatedeductsumoriginmny() throws BusinessException
/*      */   {
/*  773 */     UFDouble nmatedeductsumoriginmny = getHeadDoubleValue("nmatedeductsumoriginmny");
/*  774 */     UFDouble nmatesettlesumoriginmny = getHeadDoubleValue("nmatesettlesumoriginmny");
/*  775 */     if (SafeCompute.compare(nmatesettlesumoriginmny, nmatedeductsumoriginmny) < 0)
/*      */     {
/*  777 */       throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000022"));
/*      */     }
/*      */   }
/*      */   
/*      */   protected void calcHeadDeductmny()
/*      */   {
/*  783 */     UFDouble nthismatedeductoriginmny = new UFDouble(0);
/*      */     
/*  785 */     UFDouble nthismatedeductbasemny = new UFDouble(0);
/*      */     
/*  787 */     UFDouble nthisothdeductoriginmny = new UFDouble(0);
/*      */     
/*  789 */     UFDouble nthisothdeductbasemny = new UFDouble(0);
/*      */     
/*  791 */     UFDouble nthispredeductoriginmny = new UFDouble(0);
/*      */     
/*  793 */     UFDouble nthispredeductbasemny = new UFDouble(0);
/*      */     
/*  795 */     UFDouble napplyoriginmny_b = new UFDouble(0);
/*  796 */     UFDouble napplybasemny_b = new UFDouble(0);
/*  797 */     int ifundproperty = -1;
/*      */     
/*  799 */     int rowCount = getBillCardPanel().getRowCount();
/*  800 */     for (int i = 0; i < rowCount; i++)
/*      */     {
/*  802 */       ifundproperty = getIFundproperty(i);
/*      */       
/*  804 */       napplyoriginmny_b = getBodyDoubleValue(i, "napplyoriginmny");
/*      */       
/*  806 */       napplybasemny_b = getBodyDoubleValue(i, "napplybasemny");
/*      */       
/*  808 */       switch (ifundproperty) {
/*      */       case 3: 
/*  810 */         nthismatedeductoriginmny = SafeCompute.add(nthismatedeductoriginmny, napplyoriginmny_b);
/*      */         
/*  812 */         nthismatedeductbasemny = SafeCompute.add(nthismatedeductbasemny, napplybasemny_b);
/*      */         
/*  814 */         break;
/*      */       case 4: 
/*  816 */         nthisothdeductoriginmny = SafeCompute.add(nthisothdeductoriginmny, napplyoriginmny_b);
/*      */         
/*  818 */         nthisothdeductbasemny = SafeCompute.add(nthisothdeductbasemny, napplybasemny_b);
/*      */         
/*  820 */         break;
/*      */       case 6: 
/*  822 */         nthispredeductoriginmny = SafeCompute.add(nthispredeductoriginmny, napplyoriginmny_b);
/*      */         
/*  824 */         nthispredeductbasemny = SafeCompute.add(nthispredeductbasemny, napplybasemny_b);
/*      */       }
/*      */       
/*      */     }
/*      */     
/*      */ 
/*  830 */     getBillCardPanel().setHeadItem("nthismatedeductoriginmny", nthismatedeductoriginmny);
/*      */     
/*  832 */     getBillCardPanel().setHeadItem("nthismatedeductbasemny", nthismatedeductbasemny);
/*      */     
/*  834 */     getBillCardPanel().setHeadItem("nthisothdeductoriginmny", nthisothdeductoriginmny);
/*      */     
/*  836 */     getBillCardPanel().setHeadItem("nthisothdeductbasemny", nthisothdeductbasemny);
/*      */     
/*  838 */     getBillCardPanel().setHeadItem("nthispredeductoriginmny", nthispredeductoriginmny);
/*      */     
/*  840 */     getBillCardPanel().setHeadItem("nthispredeductbasemny", nthispredeductbasemny);
/*      */   }
/*      */   
/*      */   private void setBodybasemny(int rownum, String field, UFDouble mny)
/*      */   {
/*  845 */     getBillCardPanel().setBodyValueAt(getBaseMny(mny), rownum, field);
/*      */   }
/*      */   
/*      */   protected void delAllLine()
/*      */   {
/*  850 */     int rowcount = getBillCardPanel().getRowCount();
/*  851 */     int[] rows = new int[rowcount];
/*  852 */     for (int i = 0; i < rowcount; i++) {
/*  853 */       rows[i] = i;
/*      */     }
/*  855 */     BillScrollPane bsp = getBillCardPanel().getBodyPanel(getBillCardPanel().getCurrentBodyTableCode());
/*      */     
/*  857 */     bsp.delLine(rows);
/*      */   }
/*      */   
/*      */   private void cleanInfoWhileChangeContract()
/*      */   {
/*  862 */     cleanHeadBillInfo();
/*  863 */     cleanHeadSettleInfo();
/*  864 */     cleanHeadApplyInfo();
/*  865 */     cleanBodyData();
/*      */   }
/*      */   
/*      */   private void cleanHeadBillInfo()
/*      */   {
/*  870 */     getBillCardPanel().getHeadItem("pk_project").setValue(null);
/*  871 */     getBillCardPanel().getHeadItem("projectcode").setValue(null);
/*  872 */     getBillCardPanel().getHeadItem("projectname").setValue(null);
/*  873 */     getBillCardPanel().getHeadItem("contNO").setValue(null);
/*  874 */     getBillCardPanel().getHeadItem("vname").setValue(null);
/*  875 */     getBillCardPanel().getHeadItem("pk_conttype").setValue(null);
/*  876 */     getBillCardPanel().getHeadItem("conttype").setValue(null);
/*  877 */     getBillCardPanel().getHeadItem("pk_second").setValue(null);
/*  878 */     getBillCardPanel().getHeadItem("second").setValue(null);
/*  879 */     getBillCardPanel().getHeadItem("pk_secondbase").setValue(null);
/*  880 */     getBillCardPanel().getHeadItem("pk_cusmandoc").setValue(null);
/*  881 */     getBillCardPanel().getHeadItem("cusmandoc").setValue(null);
/*  882 */     getBillCardPanel().getHeadItem("pk_cusbasdoc").setValue(null);
/*  883 */     getBillCardPanel().getHeadItem("dlastapplydate").setValue(null);
/*  884 */     getBillCardPanel().getHeadItem("dthisapplydate").setValue(null);
/*      */   }
/*      */   
/*      */   private void cleanHeadSettleInfo()
/*      */   {
/*  889 */     getBillCardPanel().getHeadItem("nthissettleoriginmny").setValue(null);
/*  890 */     getBillCardPanel().getHeadItem("nthissettlebasemny").setValue(null);
/*  891 */     getBillCardPanel().getHeadItem("nsettlesumoriginmny").setValue(null);
/*  892 */     getBillCardPanel().getHeadItem("nsettlesumbasemny").setValue(null);
/*  893 */     getBillCardPanel().getHeadItem("nthismatesettleoriginmny").setValue(null);
/*  894 */     getBillCardPanel().getHeadItem("nthismatesettlebasemny").setValue(null);
/*  895 */     getBillCardPanel().getHeadItem("nmatesettlesumoriginmny").setValue(null);
/*  896 */     getBillCardPanel().getHeadItem("nmatesettlesumbasemny").setValue(null);
/*  897 */     getBillCardPanel().getHeadItem("nthismatedeductoriginmny").setValue(null);
/*  898 */     getBillCardPanel().getHeadItem("nthismatedeductbasemny").setValue(null);
/*  899 */     getBillCardPanel().getHeadItem("nmatedeductedoriginmny").setValue(null);
/*  900 */     getBillCardPanel().getHeadItem("nmatedeductedbasemny").setValue(null);
/*  901 */     getBillCardPanel().getHeadItem("nmatedeductsumoriginmny").setValue(null);
/*  902 */     getBillCardPanel().getHeadItem("nmatedeductsumbasemny").setValue(null);
/*  903 */     getBillCardPanel().getHeadItem("nthisothdeductoriginmny").setValue(null);
/*  904 */     getBillCardPanel().getHeadItem("nthisothdeductbasemny").setValue(null);
/*  905 */     getBillCardPanel().getHeadItem("nothdeductedoriginmny").setValue(null);
/*  906 */     getBillCardPanel().getHeadItem("nothdeductedbasemny").setValue(null);
/*  907 */     getBillCardPanel().getHeadItem("nothdeductsumoriginmny").setValue(null);
/*  908 */     getBillCardPanel().getHeadItem("nothdeductsumbasemny").setValue(null);
/*  909 */     getBillCardPanel().getHeadItem("nshouldsumoriginmny").setValue(null);
/*      */   }
/*      */   
/*      */   private void cleanHeadApplyInfo()
/*      */   {
/*  914 */     getBillCardPanel().getHeadItem("ncontpayscale").setValue(null);
/*  915 */     getBillCardPanel().getHeadItem("ipaymode").setValue(null);
/*  916 */     getBillCardPanel().getHeadItem("ncontshouldoriginmny").setValue(null);
/*  917 */     getBillCardPanel().getHeadItem("ncontshouldbasemny").setValue(null);
/*  918 */     getBillCardPanel().getHeadItem("nappliedoriginmny").setValue(null);
/*  919 */     getBillCardPanel().getHeadItem("nappliedbasemny").setValue(null);
/*  920 */     getBillCardPanel().getHeadItem("npaidoriginmny").setValue(null);
/*  921 */     getBillCardPanel().getHeadItem("npaidbasemny").setValue(null);
/*  922 */     getBillCardPanel().getHeadItem("nnopayoriginmny").setValue(null);
/*  923 */     getBillCardPanel().getHeadItem("nnopaybasemny").setValue(null);
/*  924 */     getBillCardPanel().getHeadItem("ncanapplyoriginmny").setValue(null);
/*  925 */     getBillCardPanel().getHeadItem("ncanapplybasemny").setValue(null);
/*  926 */     getBillCardPanel().getHeadItem("napplyoriginmny").setValue(null);
/*  927 */     getBillCardPanel().getHeadItem("napplybasemny").setValue(null);
/*      */   }
/*      */   
/*      */   private void cleanBodyData()
/*      */   {
/*  932 */     delAllLine();
/*  933 */     getBillCardPanel().getBillModel().clearBodyData();
/*      */   }
/*      */   
/*      */   private boolean pk_contractAfterEdit(String pk_contract)
/*      */   {
/*      */     try
/*      */     {
/*  940 */       setContractData2payApply(pk_contract);
/*      */       
/*      */ 
/*  943 */       setNsettlesumoriginmnyforPkcontract(pk_contract);
/*      */       
/*  945 */       afterThisApplyDateEdit2();
/*      */     } catch (Exception e) {
/*  947 */       Logger.error(e.getMessage(), e);
/*  948 */       showErrorMessage(NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000024") + e.getMessage());
/*      */       
/*  950 */       return false;
/*      */     }
/*      */     
/*  953 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void setNsettlesumoriginmnyforPkcontract(String pk_contract)
/*      */   {
/*  962 */     StringBuffer sb = new StringBuffer();
/*  963 */     sb.append("select sum(nmny) as nmny                                              ");
/*  964 */     sb.append("  from (select distinct b.nmny                                        ");
/*  965 */     sb.append("          from ic_general_b b                                         ");
/*  966 */     sb.append("         inner join ic_general_h h                   ");
/*  967 */     sb.append("            on h.cgeneralhid = b.cgeneralhid                          ");
/*  968 */     sb.append("         right join po_order o                        ");
/*  969 */     sb.append("            on b.cfirstbillhid = o.corderid                           ");
/*  970 */     sb.append("         inner join po_order_b ob                        ");
/*  971 */     sb.append("            on ob.corderid = o.corderid                               ");
/*  972 */     sb.append("         right join ct_manage m                          ");
/*  973 */     sb.append("            on m.pk_ct_manage = ob.csourcebillid                      ");
/*  974 */     sb.append("          left join ct_manage_b mb                     ");
/*  975 */     sb.append("            on mb.pk_ct_manage = m.pk_ct_manage                       ");
/*  976 */     sb.append("         right join jzpm_cm_contract c                ");
/*  977 */     sb.append("            on c.pk_contract = mb.csourcebillhid                      ");
/*  978 */     sb.append("         where h.cbilltypecode = '45'              ");
/*      */     
/*  980 */     sb.append("           and c.pk_contract = '" + pk_contract + "');               ");
/*      */     try {
/*  982 */       Object nmny = ((IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class)).executeQuery(sb.toString(), new ColumnProcessor("nmny"));
/*  983 */       if ((nmny != null) && (nmny.toString().length() > 0)) {
/*  984 */         getBillCardPanel().setHeadItem("nsettlesumoriginmny", nmny.toString());
/*      */       }
/*      */     }
/*      */     catch (BusinessException e) {
/*  988 */       e.printStackTrace();
/*      */     }
/*      */   }
/*      */   
/*      */   private void setHeadDlastapplydate(String pk_contract)
/*      */     throws BusinessException
/*      */   {
/*  995 */     getBillCardPanel().setHeadItem("dlastapplydate", PayApplyQueryUtil.queryDlastapplydate(pk_contract));
/*      */   }
/*      */   
/*      */   private void setContractData2payApply(String pk_contract)
/*      */   {
/* 1000 */     SuperVO tempVO = null;
/*      */     try {
/* 1002 */       tempVO = getBusiDelegator().queryByPrimaryKey(CmContractVO.class, pk_contract);
/*      */       
/* 1004 */       this.contractVO = ((CmContractVO)tempVO);
/*      */     }
/*      */     catch (Exception e) {
/* 1007 */       Logger.error(e.getMessage(), e);
/* 1008 */       showErrorMessage(NCLangRes4VoTransl.getNCLangRes().getStrByID("H51705", "UJZH51705-8000025"));
/*      */       
/* 1010 */       this.contractVO = null;
/* 1011 */       return;
/*      */     }
/* 1013 */     if (tempVO != null) {
/* 1014 */       CmContractVO contractVO = (CmContractVO)tempVO;
/* 1015 */       getBillCardPanel().setHeadItem("pk_contract", contractVO.getPk_contract());
/*      */       
/* 1017 */       getBillCardPanel().setHeadItem("pk_project", contractVO.getPk_project());
/*      */       
/* 1019 */       getBillCardPanel().setHeadItem("pk_second", contractVO.getPk_second());
/*      */       
/* 1021 */       getBillCardPanel().setHeadItem("pk_secondbase", contractVO.getPk_secondbase());
/*      */       
/* 1023 */       getBillCardPanel().setHeadItem("vname", contractVO.getVname());
/* 1024 */       getBillCardPanel().setHeadItem("pk_conttype", contractVO.getPk_conttype());
/*      */       
/* 1026 */       getBillCardPanel().setHeadItem("pk_cusmandoc", contractVO.getPk_second());
/*      */       
/* 1028 */       getBillCardPanel().setHeadItem("pk_cusbasdoc", contractVO.getPk_secondbase());
/*      */       
/* 1030 */       getBillCardPanel().setHeadItem("pk_origintype", contractVO.getPk_origintype());
/*      */       
/* 1032 */       getBillCardPanel().setHeadItem("pk_basetype", contractVO.getPk_basetype());
/*      */       
/* 1034 */       getBillCardPanel().setHeadItem("nbaserate", contractVO.getNbaserate());
/*      */       
/* 1036 */       getBillCardPanel().setHeadItem("npaidoriginmny", contractVO.getNactualsumoriginmny());
/*      */       
/* 1038 */       getBillCardPanel().setHeadItem("npaidbasemny", contractVO.getNactualsumbasemny());
/*      */       
/* 1040 */       getBillCardPanel().setHeadItem("nappliedoriginmny", contractVO.getNapplysumoriginmny());
/*      */       
/* 1042 */       getBillCardPanel().setHeadItem("nappliedbasemny", contractVO.getNapplysumbasemny());
/*      */       
/* 1044 */       getBillCardPanel().setHeadItem("nsettlesumoriginmny", contractVO.getNsettlesumoriginmny());
/*      */       
/* 1046 */       getBillCardPanel().setHeadItem("nsettlesumbasemny", contractVO.getNsettlesumbasemny());
/*      */       
/* 1048 */       getBillCardPanel().setHeadItem("nmatesettlesumoriginmny", contractVO.getNmatesettlesumoriginmny());
/*      */       
/* 1050 */       getBillCardPanel().setHeadItem("nmatesettlesumbasemny", contractVO.getNmatesettlesumbasemny());
/*      */       
/* 1052 */       getBillCardPanel().setHeadItem("nmatedeductedoriginmny", contractVO.getNmatesettleappliedoriginmny());
/*      */       
/* 1054 */       getBillCardPanel().setHeadItem("nmatedeductedbasemny", contractVO.getNmatesettleappliedbasemny());
/*      */       
/* 1056 */       getBillCardPanel().setHeadItem("ts", contractVO.getTs());
/*      */       
/* 1058 */       getBillCardPanel().execHeadFormulas(getBillCardPanel().getHeadItem("pk_conttype").getEditFormulas());
/*      */       
/* 1060 */       getBillCardPanel().execHeadFormulas(getBillCardPanel().getHeadItem("projectname").getEditFormulas());
/*      */       
/* 1062 */       getBillCardPanel().setHeadItem("nprepaysumoriginmny", contractVO.getNprepaysumoriginmny());
/*      */       
/* 1064 */       getBillCardPanel().setHeadItem("nprepaysumbasemny", contractVO.getNprepaysumbasemny());
/*      */       
/* 1066 */       getBillCardPanel().setHeadItem("npredeductedoriginmny", contractVO.getNpredeductsumoriginmny());
/*      */       
/* 1068 */       getBillCardPanel().setHeadItem("npredeductedbasemny", contractVO.getNpredeductsumbasemny());
/*      */       
/* 1070 */       getBillCardPanel().setHeadItem("nthirdoriginmny", contractVO.getNthirdoriginmny());
/* 1071 */       getBillCardPanel().setHeadItem("nthirdbasemny", contractVO.getNthirdbasemny());
/*      */       
/* 1073 */       getBillCardPanel().setHeadItem("nselforiginmny", contractVO.getNselforiginmny());
/* 1074 */       getBillCardPanel().setHeadItem("nselfbasemny", contractVO.getNselfbasemny());
/*      */       
/* 1076 */       getBillCardPanel().setHeadItem("nothdeductedoriginmny", contractVO.getNothdeductedoriginmny());
/* 1077 */       getBillCardPanel().setHeadItem("nothdeductedbasemny", contractVO.getNothdeductedbasemny());
/*      */       
/* 1079 */       UFDouble nsettlesumoriginmny = contractVO.getNsettlesumoriginmny();
/*      */       
/* 1081 */       UFDouble nreservedeductrate = SafeCompute.div(contractVO.getNreservedeductrate(), new UFDouble(100));
/* 1082 */       UFDouble nsumreserveoriginmny = SafeCompute.multiply(nsettlesumoriginmny, nreservedeductrate);
/* 1083 */       UFDouble nsumreservebasemny = SafeCompute.multiply(contractVO.getNsettlesumbasemny(), nreservedeductrate);
/*      */       
/* 1085 */       UFDouble nreserveoriginmny = contractVO.getNreserveoriginmny();
/* 1086 */       UFDouble nreservebasemny = contractVO.getNreservebasemny();
/* 1087 */       if (SafeCompute.compare(nsumreserveoriginmny, nreserveoriginmny) > 0) {
/* 1088 */         nsumreserveoriginmny = nreserveoriginmny;
/* 1089 */         nsumreservebasemny = nreservebasemny;
/*      */       }
/*      */       
/* 1092 */       getBillCardPanel().setHeadItem("def_nreservedeductrate", contractVO.getNreservedeductrate());
/* 1093 */       getBillCardPanel().setHeadItem("nsumreserveoriginmny", nsumreserveoriginmny);
/* 1094 */       getBillCardPanel().setHeadItem("nsumreservebasemny", nsumreservebasemny);
/*      */       
/* 1096 */       getBillCardPanel().setHeadItem("nownerpayoriginmny", contractVO.getNownerpayoriginmny());
/* 1097 */       getBillCardPanel().setHeadItem("nownerpaybasemny", contractVO.getNownerpaybasemny());
/*      */     }
/*      */     
/* 1100 */     setSubTaxInfo(pk_contract);
/*      */   }
/*      */   
/*      */   private void setSubTaxInfo(String pk_contract) {
/* 1104 */     String sql = " isnull(dr,0)=0 and pk_contract='" + pk_contract + "'";
/*      */     try {
/* 1106 */       SubTaxVO[] subTaxVOs = (SubTaxVO[])getBusiDelegator().queryByCondition(SubTaxVO.class, sql);
/*      */       
/* 1108 */       if ((subTaxVOs != null) && (subTaxVOs.length > 0)) {
/* 1109 */         Integer itaxpaytype = subTaxVOs[0].getItaxpaytype();
/* 1110 */         getBillCardPanel().setHeadItem("itaxpaytype", itaxpaytype);
/* 1111 */         UFDouble nbusinesstaxoriginmny = UFDouble.ZERO_DBL;
/* 1112 */         UFDouble nconstructiontaxoriginmny = UFDouble.ZERO_DBL;
/* 1113 */         UFDouble neducationtaxoriginmny = UFDouble.ZERO_DBL;
/* 1114 */         UFDouble nothtaxoriginmny = UFDouble.ZERO_DBL;
/* 1115 */         for (SubTaxVO subTaxVO : subTaxVOs) {
/* 1116 */           nbusinesstaxoriginmny = SafeCompute.add(nbusinesstaxoriginmny, subTaxVO.getNbusinesstaxoriginmny());
/* 1117 */           nconstructiontaxoriginmny = SafeCompute.add(nconstructiontaxoriginmny, subTaxVO.getNconstructiontaxoriginmny());
/* 1118 */           neducationtaxoriginmny = SafeCompute.add(neducationtaxoriginmny, subTaxVO.getNeducationtaxoriginmny());
/* 1119 */           nothtaxoriginmny = SafeCompute.add(nothtaxoriginmny, subTaxVO.getNothtaxoriginmny());
/*      */         }
/*      */         
/* 1122 */         getBillCardPanel().setHeadItem("nbusinesstaxoriginmny", nbusinesstaxoriginmny);
/* 1123 */         getBillCardPanel().setHeadItem("nconstructiontaxoriginmny", nconstructiontaxoriginmny);
/* 1124 */         getBillCardPanel().setHeadItem("neducationtaxoriginmny", neducationtaxoriginmny);
/* 1125 */         getBillCardPanel().setHeadItem("nothtaxoriginmny", nothtaxoriginmny);
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1129 */       Logger.error(e.getMessage(), e);
/* 1130 */       return;
/*      */     }
/*      */   }
/*      */   
/*      */   private UFDouble getUnDeductMatSettleMny()
/*      */   {
/* 1136 */     UFDouble napplyoriginmny = UFDouble.ZERO_DBL;
/*      */     
/* 1138 */     UFDouble nmatesettlesumoriginmny = getHeadDoubleValue("nmatesettlesumoriginmny");
/*      */     
/* 1140 */     UFDouble nmatedeductedoriginmny = getHeadDoubleValue("nmatedeductedoriginmny");
/* 1141 */     napplyoriginmny = SafeCompute.sub(nmatesettlesumoriginmny, nmatedeductedoriginmny);
/*      */     
/* 1143 */     return napplyoriginmny;
/*      */   }
/*      */   
/*      */   private void createBodyLineByContract()
/*      */   {
/* 1148 */     delAllLine();
/*      */     
/* 1150 */     int iproperty = getHeadIntValue("iproperty");
/* 1151 */     if (iproperty == 1)
/*      */     {
/* 1153 */       addLineForMatDuData();
/*      */       
/* 1155 */       addLineForOthDuData();
/*      */     }
/*      */   }
/*      */   
/*      */   private void addLineForMatDuData()
/*      */   {
/* 1161 */     UFDouble matdeductoriginmny = getUnDeductMatSettleMny();
/*      */     
/* 1163 */     if (SafeCompute.compare(matdeductoriginmny, UFDouble.ZERO_DBL) > 0) {
/* 1164 */       ClientEventHandler event = (ClientEventHandler)getManageEventHandler();
/*      */       try
/*      */       {
/* 1167 */         event.onBoLineAdd();
/*      */       } catch (Exception e) {
/* 1169 */         Logger.error("增行出现错误！");
/*      */       }
/* 1171 */       int num = getBillCardPanel().getRowCount();
/*      */       
/* 1173 */       getBillCardPanel().setBodyValueAt(Integer.valueOf(3), num - 1, "ifundproperty");
/*      */       
/* 1175 */       UFDouble matdeductbasemny = getBaseMny(getUnDeductMatSettleMny());
/*      */       
/* 1177 */       getBillCardPanel().setBodyValueAt(matdeductoriginmny, num - 1, "napplyoriginmny");
/*      */       
/* 1179 */       getBillCardPanel().setBodyValueAt(matdeductbasemny, num - 1, "napplybasemny");
/*      */       
/* 1181 */       getBillCardPanel().setBodyValueAt(Boolean.valueOf(false), num - 1, "bisnew");
/*      */       
/* 1183 */       getBillCardPanel().setHeadItem("nthismatedeductoriginmny", matdeductoriginmny);
/* 1184 */       getBillCardPanel().setHeadItem("nthismatedeductbasemny", matdeductbasemny);
/*      */     }
/*      */   }
/*      */   
/*      */   private void addLineForOthDuData()
/*      */   {
/* 1190 */     ClientEventHandler event = (ClientEventHandler)getManageEventHandler();
/* 1191 */     SubSettleOtduVO[] otduVOs = (SubSettleOtduVO[])null;
/*      */     try
/*      */     {
/* 1194 */       otduVOs = PayApplyQueryUtil.querySettleOtherDeductData(getHeadItemString("pk_contract"));
/*      */     }
/*      */     catch (BusinessException e)
/*      */     {
/* 1198 */       Logger.error("查询分包结算单的其它扣款出现错误！");
/*      */     }
/*      */     
/* 1201 */     Map<String, SubSettleOtduVO> otduVoMap = getGatherOtduVoByFee(otduVOs);
/*      */     
/* 1203 */     if ((otduVoMap != null) && (!otduVoMap.isEmpty())) {
/* 1204 */       UFDouble nthisothdeductoriginmny = new UFDouble(0);
/* 1205 */       UFDouble nthisothdeductbasemny = new UFDouble(0);
/* 1206 */       for (String key : otduVoMap.keySet()) {
/* 1207 */         SubSettleOtduVO otduVO = (SubSettleOtduVO)otduVoMap.get(key);
/*      */         try
/*      */         {
/* 1210 */           event.onBoLineAdd();
/*      */         } catch (Exception e) {
/* 1212 */           Logger.error("增行出现错误！");
/*      */         }
/* 1214 */         int num = getBillCardPanel().getRowCount();
/*      */         
/* 1216 */         getBillCardPanel().setBodyValueAt(Integer.valueOf(4), num - 1, "ifundproperty");
/*      */         
/* 1218 */         getBillCardPanel().setBodyValueAt(otduVO.getPk_feebasinfo(), num - 1, "pk_feebasinfo");
/*      */         
/* 1220 */         getBillCardPanel().execBodyFormula(num - 1, "fyxx");
/*      */         
/* 1222 */         UFDouble napplyoriginmny = otduVO.getNdeductoriginmny();
/*      */         
/* 1224 */         getBillCardPanel().setBodyValueAt(napplyoriginmny, num - 1, "napplyoriginmny");
/*      */         
/* 1226 */         getBillCardPanel().setBodyValueAt(getBaseMny(napplyoriginmny), num - 1, "napplybasemny");
/*      */         
/* 1228 */         getBillCardPanel().setBodyValueAt(Boolean.valueOf(false), num - 1, "bisnew");
/*      */         
/* 1230 */         nthisothdeductoriginmny = SafeCompute.add(nthisothdeductoriginmny, napplyoriginmny);
/* 1231 */         nthisothdeductbasemny = SafeCompute.add(nthisothdeductbasemny, getBaseMny(napplyoriginmny));
/*      */       }
/* 1233 */       getBillCardPanel().setHeadItem("nthisothdeductoriginmny", nthisothdeductoriginmny);
/* 1234 */       getBillCardPanel().setHeadItem("nthisothdeductbasemny", nthisothdeductbasemny);
/*      */     }
/*      */   }
/*      */   
/*      */   private Map<String, SubSettleOtduVO> getGatherOtduVoByFee(SubSettleOtduVO[] vos) {
/* 1239 */     if ((vos == null) || (vos.length == 0)) {
/* 1240 */       return null;
/*      */     }
/* 1242 */     Map map = new HashMap();
/* 1243 */     for (SubSettleOtduVO vo : vos) {
/* 1244 */       String pk_feebasinfo = vo.getPk_feebasinfo();
/* 1245 */       if (map.containsKey(pk_feebasinfo)) {
/* 1246 */         SubSettleOtduVO otduVo = (SubSettleOtduVO)map.get(pk_feebasinfo);
/* 1247 */         UFDouble ndeductoriginmny = otduVo.getNdeductoriginmny();
/* 1248 */         otduVo.setNdeductoriginmny(SafeCompute.add(vo.getNdeductoriginmny(), ndeductoriginmny));
/*      */       } else {
/* 1250 */         map.put(pk_feebasinfo, vo);
/*      */       }
/*      */     }
/* 1253 */     return map;
/*      */   }
/*      */   
/*      */   protected String getSettleBillType()
/*      */   {
/* 1258 */     String settlebilltype = null;
/* 1259 */     int iproperty = getHeadIntValue("iproperty");
/* 1260 */     switch (iproperty) {
/*      */     case 1: 
/* 1262 */       settlebilltype = "99N1";
/* 1263 */       break;
/*      */     case 3: 
/* 1265 */       settlebilltype = "99G6";
/* 1266 */       break;
/*      */     case 4: 
/* 1268 */       settlebilltype = "99L6";
/* 1269 */       break;
/*      */     case 5: 
/*      */     case 6: 
/*      */     case 7: 
/* 1273 */       settlebilltype = "9934";
/* 1274 */       break;
/*      */     case 9: 
/* 1276 */       settlebilltype = "99HK";
/*      */     }
/*      */     
/*      */     
/*      */ 
/* 1281 */     return settlebilltype;
/*      */   }
/*      */   
/*      */   private void setHeadSettleData2()
/*      */     throws BusinessException
/*      */   {
/* 1287 */     UFDouble nthissettleoriginmny = new UFDouble(0);
/* 1288 */     UFDouble nthissettlebasemny = new UFDouble(0);
/*      */     
/* 1290 */     UFDouble nthismatesettleoriginmny = new UFDouble(0);
/* 1291 */     UFDouble nthismatesettlebasemny = new UFDouble(0);
/*      */     
/* 1293 */     String vlastbill = "";
/* 1294 */     int index = 0;
/*      */     
/* 1296 */     svos = PayApplyQueryUtil.querySettleData(getHeadItemString("pk_contract"), getSettleBillType(), null, true);
/*      */     
/* 1298 */     if ((svos != null) && (svos.length > 0)) {
/* 1299 */       for (PaSettleVO svo : svos) {
/* 1300 */         nthissettleoriginmny = SafeCompute.add(nthissettleoriginmny, svo.getNsettleoriginmny());
/*      */         
/* 1302 */         nthissettlebasemny = SafeCompute.add(nthissettlebasemny, svo.getNsettlebasemny());
/*      */         
/* 1304 */         nthismatesettleoriginmny = SafeCompute.add(nthismatesettleoriginmny, svo.getNmatesettleoriginmny());
/*      */         
/* 1306 */         nthismatesettlebasemny = SafeCompute.add(nthismatesettlebasemny, svo.getNmatesettlebasemny());
/*      */         
/* 1308 */         String pk_settle_b = svo.getPk_setttle_b() == null ? "" : svo.getPk_setttle_b();
/* 1309 */         if (index == 0) {
/* 1310 */           vlastbill = svo.getPk_billtype() + "@" + svo.getPk_settle() + "%" + pk_settle_b + "&" + svo.getTs();
/*      */         }
/*      */         else {
/* 1313 */           vlastbill = vlastbill + ";" + svo.getPk_billtype() + "@" + svo.getPk_settle() + "%" + pk_settle_b + "&" + svo.getTs();
/*      */         }
/* 1315 */         index++;
/*      */       }
/*      */     }
/*      */     
/* 1319 */     getBillCardPanel().setHeadItem("nthissettleoriginmny", nthissettleoriginmny);
/* 1320 */     getBillCardPanel().setHeadItem("nthissettlebasemny", nthissettlebasemny);
/*      */     
/* 1322 */     getBillCardPanel().setHeadItem("nthismatesettleoriginmny", nthismatesettleoriginmny);
/* 1323 */     getBillCardPanel().setHeadItem("nthismatesettlebasemny", nthismatesettlebasemny);
/* 1324 */     getBillCardPanel().setHeadItem("vlastbill", vlastbill);
/*      */   }
/*      */   
/*      */   public void setApplidmny(int num)
/*      */   {
/* 1329 */     Object nsettleoriginmnyO = getBillCardPanel().getBodyValueAt(num - 1, "nsettleoriginmny");
/*      */     
/* 1331 */     Object nsettlebasemnyO = getBillCardPanel().getBodyValueAt(num - 1, "nsettlebasemny");
/*      */     
/* 1333 */     Object nappliedoriginmnyO = getBillCardPanel().getBodyValueAt(num - 1, "nappliedoriginmny");
/*      */     
/* 1335 */     Object nappliedbasemnyO = getBillCardPanel().getBodyValueAt(num - 1, "nappliedbasemny");
/*      */     
/* 1337 */     UFDouble nsettleoriginmny = new UFDouble(nsettleoriginmnyO == null ? null : nsettleoriginmnyO.toString());
/*      */     
/* 1339 */     UFDouble nsettlebasemny = new UFDouble(nsettlebasemnyO == null ? null : nsettlebasemnyO.toString());
/*      */     
/* 1341 */     UFDouble nappliedoriginmny = new UFDouble(nappliedoriginmnyO == null ? null : nappliedoriginmnyO.toString());
/*      */     
/* 1343 */     UFDouble nappliedbasemny = new UFDouble(nappliedbasemnyO == null ? null : nappliedbasemnyO.toString());
/*      */     
/* 1345 */     getBillCardPanel().setBodyValueAt(SafeCompute.sub(nsettleoriginmny, nappliedoriginmny), num - 1, "applidormny");
/*      */     
/* 1347 */     getBillCardPanel().setBodyValueAt(SafeCompute.sub(nsettlebasemny, nappliedbasemny), num - 1, "applidmnybas");
/*      */     
/* 1349 */     getBillCardPanel().setBodyValueAt(SafeCompute.sub(nsettleoriginmny, nappliedoriginmny), num - 1, "napplyoriginmny");
/*      */     
/* 1351 */     getBillCardPanel().setBodyValueAt(SafeCompute.sub(nsettlebasemny, nappliedbasemny), num - 1, "napplybasemny");
/*      */   }
/*      */   
/*      */   public String[] getHeadBaseItems()
/*      */   {
/* 1356 */     return new String[] { "nthissettlebasemny", "nsettlesumbasemny", "nthismatesettlebasemny", "nmatesettlesumbasemny", "nthismatedeductbasemny", "nmatedeductedbasemny", "nmatedeductsumbasemny", "nthisothdeductbasemny", "nothdeductedbasemny", "nothdeductsumbasemny", "nshouldsumbasemny", "ncontshouldbasemny", "nappliedbasemny", "npaidbasemny", "nnopaybasemny", "ncanapplybasemny", "napplybasemny", "npaybasemny", "nbefapplybasemny", "nsumreservebasemny", "nownerpaybasemny" };
/*      */   }
/*      */   
/*      */   public String[] getHeadOriginItems()
/*      */   {
/* 1361 */     return new String[] { "nthissettleoriginmny", "nsettlesumoriginmny", "nthismatesettleoriginmny", "nmatesettlesumoriginmny", "nthismatedeductoriginmny", "nmatedeductedoriginmny", "nmatedeductsumoriginmny", "nthisothdeductoriginmny", "nothdeductedoriginmny", "nothdeductsumoriginmny", "nshouldsumoriginmny", "ncontshouldoriginmny", "nappliedoriginmny", "npaidoriginmny", "nnopayoriginmny", "ncanapplyoriginmny", "napplyoriginmny", "npayoriginmny", "nbefapplyoriginmny", "nsumreservebasemny", "nownerpayoriginmny" };
/*      */   }
/*      */   
/*      */   public String[][] getBodyBaseItems()
/*      */   {
/* 1366 */     return new String[][] { { "napplybasemny", "npaybasemny" } };
/*      */   }
/*      */   
/*      */   public String[][] getBodyOriginItems()
/*      */   {
/* 1371 */     return new String[][] { { "napplyoriginmny", "npayoriginmny" } };
/*      */   }
/*      */   
/*      */   protected int getIFundproperty(int row)
/*      */   {
/* 1376 */     Object o = getBillCardPanel().getBodyValueAt(row, "ifundproperty");
/* 1377 */     if ((o != null) && (o.toString().trim().length() > 0)) {
/* 1378 */       if (Ifundproperty.IFUNDPROPERTY[3].equalsIgnoreCase(o.toString()))
/* 1379 */         return 3;
/* 1380 */       if (Ifundproperty.IFUNDPROPERTY[4].equalsIgnoreCase(o.toString()))
/*      */       {
/* 1382 */         return 4; }
/* 1383 */       if (Ifundproperty.IFUNDPROPERTY[2].equalsIgnoreCase(o.toString()))
/*      */       {
/* 1385 */         return 2; }
/* 1386 */       if (Ifundproperty.IFUNDPROPERTY[1].equalsIgnoreCase(o.toString()))
/*      */       {
/* 1388 */         return 1; }
/* 1389 */       if (Ifundproperty.IFUNDPROPERTY[0].equalsIgnoreCase(o.toString()))
/*      */       {
/* 1391 */         return 0; }
/* 1392 */       if (Ifundproperty.IFUNDPROPERTY[5].equalsIgnoreCase(o.toString()))
/*      */       {
/* 1394 */         return 5; }
/* 1395 */       if (Ifundproperty.IFUNDPROPERTY[6].equalsIgnoreCase(o.toString()))
/*      */       {
/* 1397 */         return 6;
/*      */       }
/*      */     }
/*      */     
/* 1401 */     return -1;
/*      */   }
/*      */   
/*      */   protected boolean checkIsNewDeduct(int rowIndex)
/*      */   {
/* 1406 */     boolean isnew = false;
/*      */     
/* 1408 */     if (isListPanelSelected()) {
/* 1409 */       isnew = getBillListPanel().getBodyBillModel().getValueAt(rowIndex, "bisnew") == null ? false : ((Boolean)getBillListPanel().getBodyBillModel().getValueAt(rowIndex, "bisnew")).booleanValue();
/*      */     }
/*      */     else {
/* 1412 */       isnew = getBodyBooleanValue(rowIndex, "bisnew").booleanValue();
/*      */     }
/* 1414 */     return isnew;
/*      */   }
/*      */   
/*      */   protected boolean checkIsContLine(int rowIndex)
/*      */   {
/* 1419 */     int ifundproperty = getIFundproperty(rowIndex);
/*      */     
/* 1421 */     switch (ifundproperty) {
/*      */     case 3: 
/*      */     case 4: 
/* 1424 */       return false;
/*      */     case 0: 
/*      */     case 1: 
/*      */     case 2: 
/*      */     case 5: 
/* 1429 */       return true;
/*      */     }
/* 1431 */     return false;
/*      */   }
/*      */   
/*      */   public PaSettleVO[] getSvos()
/*      */   {
/* 1436 */     return svos;
/*      */   }
/*      */   
/*      */   public void update(Observable o, Object arg)
/*      */   {
/* 1441 */     super.update(o, arg);
/* 1442 */     adjustShowColor();
/*      */     
/* 1444 */     updateEditenble();
/*      */     
/* 1446 */     updateLineenble();
/*      */     
/* 1448 */     updateCloseBtn();
/*      */     
/* 1450 */     updateUnCloseBtn();
/*      */   }
/*      */   
/*      */   public void updateLineenble()
/*      */   {
/* 1455 */     ButtonObject lineBtn = getButtonManager().getButton(10);
/* 1456 */     int ibillStatus = getHeadIntValue(getBillField().getField_BillStatus());
/* 1457 */     if ((ibillStatus == 2) || (ibillStatus == 3))
/*      */     {
/* 1459 */       lineBtn.setEnabled(false);
/*      */       try {
/* 1461 */         updateButtonUI();
/*      */       } catch (Exception e) {
/* 1463 */         Logger.error(e);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void updateEditenble()
/*      */   {
/* 1470 */     ButtonObject editBtn = getButtonManager().getButton(3);
/* 1471 */     int ibillStatus = getHeadIntValue(getBillField().getField_BillStatus());
/* 1472 */     if (ibillStatus == 2) {
/* 1473 */       editBtn.setEnabled(true);
/*      */       try {
/* 1475 */         updateButtonUI();
/*      */       } catch (Exception e) {
/* 1477 */         Logger.error(e);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void updateCloseBtn()
/*      */   {
/* 1484 */     ButtonObject closeBtn = getButtonManager().getButton(703);
/* 1485 */     Boolean bisclosed = Boolean.valueOf(getHeadItemBoolean("bisclosed"));
/* 1486 */     int ibillStatus = getHeadIntValue(getBillField().getField_BillStatus());
/* 1487 */     if ((ibillStatus == 1) && (!bisclosed.booleanValue())) {
/* 1488 */       closeBtn.setEnabled(true);
/*      */     } else {
/* 1490 */       closeBtn.setEnabled(false);
/*      */     }
/*      */     try {
/* 1493 */       updateButtonUI();
/*      */     } catch (Exception e) {
/* 1495 */       Logger.error("更新关闭按钮状态出错！");
/*      */     }
/*      */   }
/*      */   
/*      */   public void updateUnCloseBtn()
/*      */   {
/* 1501 */     ButtonObject unCloseBtn = getButtonManager().getButton(704);
/* 1502 */     Boolean bisclosed = Boolean.valueOf(getHeadItemBoolean("bisclosed"));
/* 1503 */     if ((bisclosed != null) && (bisclosed.booleanValue())) {
/* 1504 */       unCloseBtn.setEnabled(true);
/*      */     } else {
/* 1506 */       unCloseBtn.setEnabled(false);
/*      */     }
/*      */     try {
/* 1509 */       updateButtonUI();
/*      */     } catch (Exception e) {
/* 1511 */       Logger.error("更新反关闭按钮状态出错！");
/*      */     }
/*      */   }
/*      */   
/*      */   public void doApproveAction(ILinkApproveData approvedata)
/*      */   {
/* 1517 */     setCurrentPanel("CARDPANEL");
/*      */     try
/*      */     {
/* 1520 */       getBufferData().addVOToBuffer(loadHeadData(approvedata.getBillID()));
/*      */       
/* 1522 */       setListHeadData(getBufferData().getAllHeadVOsFromBuffer());
/* 1523 */       getBufferData().setCurrentRow(getBufferData().getCurrentRow());
/* 1524 */       ButtonObject[] btns = getButtons();
/* 1525 */       for (ButtonObject btn : btns) {
/* 1526 */         if (("25".equals(btn.getTag())) || ("29".equals(btn.getTag())) || ("30".equals(btn.getTag())) || ("31".equals(btn.getTag())) || ("8".equals(btn.getTag())) || ("511".equals(btn.getTag())) || ("16".equals(btn.getTag())) || ("6".equals(btn.getTag())))
/*      */         {
/* 1528 */           btn.setEnabled(true);
/* 1529 */           btn.setVisible(true);
/*      */           
/* 1531 */           if ("16".equals(btn.getTag())) {
/* 1532 */             ButtonObject[] childbtns = btn.getChildButtonGroup();
/* 1533 */             if ((childbtns != null) && (childbtns.length > 0)) {
/* 1534 */               for (ButtonObject childbtn : childbtns) {
/* 1535 */                 if (childbtn.getTag().equals("19"))
/*      */                 {
/* 1537 */                   childbtn.setEnabled(true);
/* 1538 */                   childbtn.setVisible(true);
/*      */                 }
/*      */               }
/*      */             }
/*      */           }
/*      */           
/*      */ 
/* 1545 */           if ("511".equals(btn.getTag())) {
/* 1546 */             ButtonObject[] childbtns = btn.getChildButtonGroup();
/* 1547 */             if ((childbtns != null) && (childbtns.length > 0)) {
/* 1548 */               for (ButtonObject childbtn : childbtns)
/*      */               {
/* 1550 */                 childbtn.setEnabled(true);
/* 1551 */                 childbtn.setVisible(true);
/*      */               }
/*      */             }
/*      */           }
/*      */         }
/*      */         else {
/* 1557 */           btn.setEnabled(false);
/* 1558 */           btn.setVisible(false);
/*      */         }
/*      */         
/* 1561 */         if ("3".equals(btn.getTag())) {
/* 1562 */           btn.setEnabled(true);
/* 1563 */           btn.setVisible(true);
/*      */         }
/* 1565 */         if (("0".equals(btn.getTag())) || ("7".equals(btn.getTag())))
/*      */         {
/*      */ 
/* 1568 */           btn.setVisible(true);
/*      */         }
/*      */       }
/* 1571 */       updateButtons();
/*      */     } catch (Exception ex) {
/* 1573 */       Logger.error(ex.getMessage(), ex);
/*      */     }
/*      */   }
/*      */   
/*      */   public void doMaintainAction(ILinkMaintainData maintaindata)
/*      */   {
/* 1579 */     setCurrentPanel("CARDPANEL");
/*      */     try
/*      */     {
/* 1582 */       getBufferData().addVOToBuffer(loadHeadData(maintaindata.getBillID()));
/*      */       
/* 1584 */       setListHeadData(getBufferData().getAllHeadVOsFromBuffer());
/* 1585 */       getBufferData().setCurrentRow(getBufferData().getCurrentRow());
/* 1586 */       ButtonObject[] btns = getButtons();
/* 1587 */       for (ButtonObject btn : btns) {
/* 1588 */         if (("25".equals(btn.getTag())) || ("29".equals(btn.getTag())) || ("30".equals(btn.getTag())) || ("31".equals(btn.getTag())) || ("8".equals(btn.getTag())) || ("511".equals(btn.getTag())) || ("16".equals(btn.getTag())) || ("6".equals(btn.getTag())))
/*      */         {
/* 1590 */           btn.setEnabled(true);
/* 1591 */           btn.setVisible(true);
/*      */           
/* 1593 */           if ("16".equals(btn.getTag())) {
/* 1594 */             ButtonObject[] childbtns = btn.getChildButtonGroup();
/* 1595 */             if ((childbtns != null) && (childbtns.length > 0)) {
/* 1596 */               for (ButtonObject childbtn : childbtns) {
/* 1597 */                 if (childbtn.getTag().equals("19"))
/*      */                 {
/* 1599 */                   childbtn.setEnabled(true);
/* 1600 */                   childbtn.setVisible(true);
/*      */                 }
/*      */               }
/*      */             }
/*      */           }
/*      */           
/*      */ 
/* 1607 */           if ("511".equals(btn.getTag())) {
/* 1608 */             ButtonObject[] childbtns = btn.getChildButtonGroup();
/* 1609 */             if ((childbtns != null) && (childbtns.length > 0)) {
/* 1610 */               for (ButtonObject childbtn : childbtns)
/*      */               {
/* 1612 */                 childbtn.setEnabled(true);
/* 1613 */                 childbtn.setVisible(true);
/*      */               }
/*      */             }
/*      */           }
/*      */         }
/*      */         else {
/* 1619 */           btn.setEnabled(false);
/* 1620 */           btn.setVisible(false);
/*      */         }
/*      */         
/* 1623 */         if ("3".equals(btn.getTag())) {
/* 1624 */           btn.setEnabled(true);
/* 1625 */           btn.setVisible(true);
/*      */         }
/* 1627 */         if (("0".equals(btn.getTag())) || ("7".equals(btn.getTag())))
/*      */         {
/*      */ 
/* 1630 */           btn.setVisible(true);
/*      */         }
/*      */       }
/* 1633 */       updateButtons();
/*      */     } catch (Exception ex) {
/* 1635 */       Logger.error(ex.getMessage(), ex);
/*      */     }
/*      */   }
/*      */   
/*      */   private String[] getHeadShowRedFields()
/*      */   {
/* 1641 */     return new String[] { "nthismatedeductoriginmny", "nthismatedeductbasemny", "nthisothdeductoriginmny", "nthisothdeductbasemny", "nmatedeductsumoriginmny", "nmatedeductsumbasemny", "nothdeductsumoriginmny", "nothdeductsumbasemny", "nothdeductedoriginmny", "nothdeductedbasemny", "nmatedeductedoriginmny", "nmatedeductedbasemny", "nthismatesettleoriginmny", "nthismatesettlebasemny", "nmatesettlesumoriginmny", "nmatesettlesumbasemny" };
/*      */   }
/*      */   
/*      */   private String[] getBodyShowRedFields()
/*      */   {
/* 1646 */     return new String[] { "napplyoriginmny", "npayoriginmny", "napplybasemny", "npaybasemny" };
/*      */   }
/*      */   
/*      */   public void adjustShowColor()
/*      */   {
/* 1651 */     if (isListPanelSelected())
/*      */     {
/* 1653 */       int hrowcount = getBillListPanel().getHeadTable().getRowCount();
/* 1654 */       for (int i = 0; i < hrowcount; i++) {
/* 1655 */         setHeadForeGround(i, getHeadShowRedFields(), Color.RED);
/*      */       }
/*      */       
/* 1658 */       int browcount = getBillListPanel().getBodyTable().getRowCount();
/* 1659 */       for (int i = 0; i < browcount; i++) {
/* 1660 */         if (checkIsNewDeduct(i)) {
/* 1661 */           setBodyForeGround(i, getBodyShowRedFields(), Color.RED);
/*      */         } else {
/* 1663 */           setBodyForeGround(i, getBodyShowRedFields(), Color.BLACK);
/*      */         }
/*      */       }
/*      */     } else {
/* 1667 */       setHeadForeGround(0, getHeadShowRedFields(), Color.RED);
/*      */       
/* 1669 */       int browcount = getBillCardPanel().getBillTable().getRowCount();
/* 1670 */       for (int i = 0; i < browcount; i++) {
/* 1671 */         if (checkIsNewDeduct(i)) {
/* 1672 */           setBodyForeGround(i, getBodyShowRedFields(), Color.RED);
/*      */         } else
/* 1674 */           setBodyForeGround(i, getBodyShowRedFields(), Color.BLACK);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public boolean beforeEdit(BillEditEvent e) {
/* 1680 */     if ((e.getKey().equalsIgnoreCase("napplyoriginmny")) || (e.getKey().equalsIgnoreCase("fyxx")))
/*      */     {
/* 1682 */       setUnEditable(e.getKey());
/*      */     }
/* 1684 */     if (e.getKey().equalsIgnoreCase("fyxx")) {
/* 1685 */       filterFyxxRef();
/*      */     }
/*      */     
/* 1688 */     return super.beforeEdit(e);
/*      */   }
/*      */   
/*      */   private void filterFyxxRef() {
/* 1692 */     int row = getBillCardPanel().getBillTable().getSelectedRow();
/* 1693 */     Boolean bool = (Boolean)getBillCardPanel().getBillModel().getValueAt(row, "bisnew");
/* 1694 */     String ipaymode = String.valueOf(getBillCardPanel().getBillModel().getValueAt(row, "ipaymode"));
/* 1695 */     int paymode = PayMode.getIndex(ipaymode);
/* 1696 */     if ((!bool.booleanValue()) && (paymode != -1)) {
/* 1697 */       UIRefPane refPanelfyxx = (UIRefPane)getBillCardPanel().getBodyItem("fyxx").getComponent();
/*      */       
/* 1699 */       BdFeeRefModel bdfrm = (BdFeeRefModel)refPanelfyxx.getRefModel();
/* 1700 */       StringBuffer sb = new StringBuffer();
/* 1701 */       sb.append(" and ipaymode=");
/* 1702 */       sb.append(paymode);
/* 1703 */       bdfrm.addWherePart(sb.toString(), true);
/* 1704 */       refPanelfyxx.setNotLeafSelectedEnabled(false);
/*      */     } else {
/* 1706 */       UIRefPane refPanelfyxx = (UIRefPane)getBillCardPanel().getBodyItem("fyxx").getComponent();
/*      */       
/* 1708 */       BdFeeRefModel bdfrm = (BdFeeRefModel)refPanelfyxx.getRefModel();
/* 1709 */       StringBuffer sb = new StringBuffer();
/* 1710 */       sb.append(" and ipaymode in(");
/* 1711 */       sb.append(PayMode.prepay.getIndex());
/* 1712 */       sb.append(",");
/* 1713 */       sb.append(PayMode.pay_arrival.getIndex());
/* 1714 */       sb.append(",");
/* 1715 */       sb.append(PayMode.busi_deduction.getIndex());
/* 1716 */       sb.append(",");
/* 1717 */       sb.append(PayMode.other_arrival.getIndex());
/* 1718 */       sb.append(",");
/* 1719 */       sb.append(PayMode.other_rece_arrival.getIndex());
/* 1720 */       sb.append(")");
/* 1721 */       bdfrm.addWherePart(sb.toString(), true);
/* 1722 */       refPanelfyxx.setRefModel(bdfrm);
/* 1723 */       refPanelfyxx.setNotLeafSelectedEnabled(false);
/*      */     }
/*      */   }
/*      */   
/*      */   private void filterBank()
/*      */   {
/* 1729 */     String pk_cubasdoc = getHeadItemString("pk_cusbasdoc");
/* 1730 */     ICustBankQry psnqry = (ICustBankQry)NCLocator.getInstance().lookup(ICustBankQry.class);
/* 1731 */     String[] cubasdocs = (String[])null;
/* 1732 */     if (pk_cubasdoc != null) {
/*      */       try {
/* 1734 */         cubasdocs = psnqry.queryPKsByCuBasPK(pk_cubasdoc);
/*      */       } catch (BusinessException e) {
/* 1736 */         Logger.debug(e.getMessage());
/*      */       }
/*      */     }
/* 1739 */     UIRefPane ref = (UIRefPane)getBillCardPanel().getHeadItem("pk_secondaccid").getComponent();
/* 1740 */     BankAccCustDefaultRefModel refmd = (BankAccCustDefaultRefModel)ref.getRefModel();
/* 1741 */     refmd.setFilterPks(cubasdocs);
/*      */   }
/*      */   
/*      */   private void setUnEditable(String itemKey)
/*      */   {
/* 1746 */     int row = getBillCardPanel().getBillTable().getSelectedRow();
/* 1747 */     if (row >= 0) {
/* 1748 */       String ifundproperty = getBodyStringValue(row, "ifundproperty");
/* 1749 */       Boolean bisnew = getBodyBooleanValue(row, "bisnew");
/*      */       
/* 1751 */       if ((ifundproperty != null) && (ifundproperty.equalsIgnoreCase(Ifundproperty.IFUNDPROPERTY[4])) && (bisnew != null) && (!bisnew.booleanValue()))
/*      */       {
/* 1753 */         getBillCardPanel().getBillModel().setCellEditable(row, itemKey, false);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   protected ButtonVO[] initAssQryBtnVOs() {
/* 1759 */     return new ButtonVO[] { new LinkQueryPfBtnVO().getButtonVO(), new BillLinkBtnVO().getButtonVO() };
/*      */   }
/*      */   
/*      */   public void setCardUIData(AggregatedValueObject vo)
/*      */     throws Exception
/*      */   {
/* 1765 */     super.setCardUIData(vo);
/* 1766 */     PayCFreeCustUtil.setVHeadseconedAccidShow(getBillCardPanel(), "pk_cusbasdoc", "cfreecustid", "pk_secondaccid", "vsecondaccid");
/*      */   }
/*      */   
/*      */   public boolean beforeEdit(BillItemEvent e)
/*      */   {
/* 1771 */     if ("pk_secondaccid".equalsIgnoreCase(e.getItem().getKey())) {
/* 1772 */       filterBank();
/*      */     }
/* 1774 */     return false;
/*      */   }
/*      */   
/*      */   protected void initEventListener()
/*      */   {
/* 1779 */     super.initEventListener();
/* 1780 */     getBillCardPanel().setBillBeforeEditListenerHeadTail(this);
/*      */   }
/*      */   
/*      */   private void execHeadCFreeCustFormulas() {
/* 1784 */     String[] formulas = { "cfreecustname->getColValue(bd_freecust, vcustname, cfreecustid, cfreecustid);", "vsecondaccid->getColValue(bd_freecust, vaccount, cfreecustid, cfreecustid);", "vsecondaccidname->getColValue(bd_freecust, vaccname, cfreecustid, cfreecustid);" };
/*      */     
/* 1786 */     getBillCardPanel().execHeadFormulas(formulas);
/*      */   }
/*      */   
/*      */   private void execHeadSecondAccidFormulas() {
/* 1790 */     String[] formulas = { "vsecondaccid->getColValue(bd_bankaccbas,account,pk_bankaccbas,pk_secondaccid);", "pk_bankdoc->getColValue(bd_bankaccbas,pk_bankdoc,pk_bankaccbas,pk_secondaccid);", "vsecondaccidname->getColValue(bd_bankdoc,bankdocname,pk_bankdoc,pk_bankdoc);" };
/*      */     
/* 1792 */     getBillCardPanel().execHeadFormulas(formulas);
/*      */   }
/*      */ }

/* Location:           E:\hh\nchome\modules\jzpm\client\classes
 * Qualified Name:     nc.ui.jzpm.pa05.ClientUI
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */