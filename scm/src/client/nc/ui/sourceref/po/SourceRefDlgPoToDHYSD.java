package nc.ui.sourceref.po;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import nc.ui.po.pub.PoChangeUI;
import nc.ui.po.pub.PoPublicUIClass;
import nc.ui.pu.pub.POPubSetUI;
import nc.ui.pu.pub.POPubSetUI2;
import nc.ui.pu.pub.PuTool;
import nc.ui.pub.ClientEnvironment;
import nc.ui.pub.beans.MessageDialog;
import nc.ui.pub.beans.UIComboBox;
import nc.ui.pub.beans.UIDialog;
import nc.ui.pub.bill.BillListPanel;
import nc.ui.pub.bill.DefaultCurrTypeBizDecimalListener;
import nc.ui.pub.change.PfChangeBO_Client;
import nc.ui.pub.para.SysInitBO_Client;
import nc.ui.pub.query.QueryConditionClient;
import nc.ui.rc.receive.ArriveorderHelper;
import nc.ui.rc.receive.MethodDlg;
import nc.ui.scm.pub.report.BillRowNo;
import nc.ui.scm.pub.sourceref.BillRefListPanel;
import nc.ui.scm.pub.sourceref.SimpBillRefListPanel;
import nc.ui.scm.pub.sourceref.SourceRefDlg;
import nc.ui.scm.pub.sourceref.BillToBillRefPanel.ShowState;
import nc.vo.po.OrderHeaderVO;
import nc.vo.po.OrderItemVO;
import nc.vo.po.OrderVO;
import nc.vo.po.pub.OrderPubVO;
import nc.vo.pp.ask.IBillType;
import nc.vo.pub.AggregatedValueObject;
import nc.vo.pub.BusinessException;
import nc.vo.pub.CircularlyAccessibleValueObject;
import nc.vo.pub.lang.UFBoolean;
import nc.vo.pub.lang.UFDate;
import nc.vo.pub.lang.UFDouble;
import nc.vo.pub.para.SysInitVO;
import nc.vo.rc.receive.ArriveorderHeaderVO;
import nc.vo.rc.receive.ArriveorderItemVO;
import nc.vo.rc.receive.ArriveorderVO;
import nc.vo.rc.receive.IArriveorderStatus;
import nc.vo.scm.constant.ScmConst;
import nc.vo.scm.pu.PuPubVO;
import nc.vo.scm.pu.RelationsCalVO;
import nc.vo.scm.pu.VariableConst;
import nc.vo.scm.pub.SCMEnv;
import nc.vo.scm.pub.vosplit.SplitBillVOs;
/**
 * 
 */
public class SourceRefDlgPoToDHYSD extends SourceRefDlg {

	private static final long serialVersionUID = 1L;

String sCorp = null;

  private MethodDlg m_dlgMethodDlg = null;

  // 分单参数
  public SysInitVO m_initMethodVO = null;
  
//  订单行ID = 本次默认到货数量
  Hashtable h_NumThis = new Hashtable();
  //订单行ID = 订单累计到货数量
  Hashtable h_accNum = new Hashtable();
  //订单行ID = 订货数量
  Hashtable h_ordNum = new Hashtable();
  //订单行ID = 模板中位置（取模板中的编辑结果）
  Hashtable h_ordSelectedRow = new Hashtable();
  /*退货标志*/
  private boolean m_bBack = false;
  private ArriveorderVO[] m_arriveVOs = null;
  String sOperator = null;
  POPubSetUI2 m_cardPoPubSetUI2 = new POPubSetUI2();
  public SourceRefDlgPoToDHYSD(String pkField, String pkCorp,
      String operator, String funNode, String queryWhere,
      String billType, String businessType, String templateId,
      String currentBillType, Container parent) {
    super(pkField, pkCorp, operator, funNode, queryWhere, billType,businessType, templateId, currentBillType, parent);
    sCorp = pkCorp;
    sOperator = operator;
  }

  protected BillRefListPanel createDoubleTableListPanel() {
    return new BillRefListPanelPoToArrive(getBusinessType(), getBillType(),
        getCurrentBillType(), getPkCorp());
  }

  /**
   * 返回 SimpBillRefListPanel 特性值。
   * 
   * @return SimpBillRefListPanel
   */
  /* 警告：此方法将重新生成。 */
  protected SimpBillRefListPanel createOneTableListPanel() {
    return new SimpBillRefListPanelPoToArrive(getBusinessType(),
        getBillType(), getCurrentBillType(), getPkCorp());
  }

  class SimpBillRefListPanelPoToArrive extends SimpBillRefListPanel {
	private static final long serialVersionUID = 1L;
	public SimpBillRefListPanelPoToArrive(String biztype,
        String sourcetype, String targettype, String pk_corp) {
      super(biztype, sourcetype, targettype, pk_corp);

    }

    private void iniBusiDecimal(String strCurrtype) {

      int nDigit = new Integer(POPubSetUI.getMoneyDigitByCurr_Busi(strCurrtype));
      int nPriceDigit = new POPubSetUI(sCorp).getPriceDecimal();
      int[] iaDigits = PoPublicUIClass.getShowDigits(sCorp);
      getSourceBodyItem("noriginalcurmny").setDecimalDigits(nDigit);
      getSourceBodyItem("noriginaltaxmny").setDecimalDigits(nDigit);
      getSourceBodyItem("noriginaltaxpricemny").setDecimalDigits(nDigit);

      getSourceBodyItem("noriginalcurprice")
          .setDecimalDigits(nPriceDigit);
      getSourceBodyItem("norgtaxprice").setDecimalDigits(nPriceDigit);
      getSourceBodyItem("noriginalnetprice")
          .setDecimalDigits(nPriceDigit);
      getSourceBodyItem("norgnettaxprice").setDecimalDigits(nPriceDigit);

      getSourceBodyItem("naccumarrvnum").setDecimalDigits(iaDigits[0]);
      getSourceBodyItem("naccumstorenum").setDecimalDigits(iaDigits[0]);
      getSourceBodyItem("naccuminvoicenum").setDecimalDigits(iaDigits[0]);
      getSourceBodyItem("naccumwastnum").setDecimalDigits(iaDigits[0]);

      getSourceBodyItem("nordernum").setDecimalDigits(iaDigits[0]);
      getSourceBodyItem("nnotarrvnum").setDecimalDigits(iaDigits[0]);
      getSourceBodyItem("nassistnum").setDecimalDigits(iaDigits[1]);
      getSourceBodyItem("nconvertrate").setDecimalDigits(iaDigits[3]);
    }

    public void setSourceVOToUI(CircularlyAccessibleValueObject[] srcHeadVOs,CircularlyAccessibleValueObject[] srcBodyVOs) throws BusinessException {
      iniBusiDecimal(((OrderItemVO)srcBodyVOs[0]).getCcurrencytypeid());
      new DefaultCurrTypeBizDecimalListener(getHeadBillModel(), "chd_ccurrencytypeid", new String[] {"chd_noriginalcurmny", "chd_noriginaltaxmny", "chd_noriginaltaxpricemny"});
      super.setSourceVOToUI(srcHeadVOs, srcBodyVOs);
      PuTool.setBillModelConvertRateForConvertBill(getHeadBillModel(),
          new String[] { "cbaseid", "cassistunit", "nordernum", "nassistnum", "nconvertrate" });
      int rowCount = getHeadBillModel().getRowCount();
          //设置扣税类别
          setCountTaxType(rowCount);
      // 设置自由项
      PoPublicUIClass.setFreeColValueForConvertBill(getHeadBillModel(),"vfree");

      // 处理来源单据类型、来源单据号
      nc.ui.scm.sourcebill.SourceBillTool.loadSourceInfoAllForRedun(getHeadBillModel(), "21");
    }
    public String getRefNodeCode() {
      return "H5H280XX02";
    }
  }

  class BillRefListPanelPoToArrive extends BillRefListPanel {
	private static final long serialVersionUID = 1L;
	public BillRefListPanelPoToArrive(String biztype, String sourcetype,String targettype, String pk_corp) {
      super(biztype, sourcetype, targettype, pk_corp);
    }

    private void iniBusiDecimal(String strCurrtype) {
      int nDigit = new Integer(POPubSetUI .getMoneyDigitByCurr_Busi(strCurrtype));
      int nPriceDigit = new POPubSetUI(sCorp).getPriceDecimal();
      int[] iaDigits = PoPublicUIClass.getShowDigits(sCorp);
      getBodyItem("noriginalcurmny").setDecimalDigits(nDigit);
      getBodyItem("noriginaltaxmny").setDecimalDigits(nDigit);
      getBodyItem("noriginaltaxpricemny").setDecimalDigits(nDigit);

      getBodyItem("noriginalcurprice").setDecimalDigits(nPriceDigit);
      getBodyItem("norgtaxprice").setDecimalDigits(nPriceDigit);
      getBodyItem("noriginalnetprice").setDecimalDigits(nPriceDigit);
      getBodyItem("norgnettaxprice").setDecimalDigits(nPriceDigit);

      getBodyItem("naccumarrvnum").setDecimalDigits(iaDigits[0]);
      getBodyItem("naccumstorenum").setDecimalDigits(iaDigits[0]);
      getBodyItem("naccuminvoicenum").setDecimalDigits(iaDigits[0]);
      getBodyItem("naccumwastnum").setDecimalDigits(iaDigits[0]);

      getBodyItem("nordernum").setDecimalDigits(iaDigits[0]);
      getBodyItem("nnotarrvnum").setDecimalDigits(iaDigits[0]);
      getBodyItem("nassistnum").setDecimalDigits(iaDigits[1]);
        
      int[] daRate = m_cardPoPubSetUI2.getBothExchRateDigit(sCorp, strCurrtype);
      getBodyItem("nexchangeotobrate").setDecimalDigits(daRate[0]);
      getBodyItem("nconvertrate").setDecimalDigits(iaDigits[3]);
      
    }
    private void initBillItem(){
      UIComboBox cbbBType = (UIComboBox) getBodyItem("idiscounttaxtype").getComponent();

  cbbBType.addItem(VariableConst.IDISCOUNTTAXTYPE_NAME_INNER);
  cbbBType.addItem(VariableConst.IDISCOUNTTAXTYPE_NAME_OUTTER);
  cbbBType.addItem(VariableConst.IDISCOUNTTAXTYPE_NAME_NOCOUNT);
  cbbBType.setSelectedIndex(1);
  cbbBType.setTranslate(true);
  getBodyItem("idiscounttaxtype").setWithIndex(true);}
    public void setSourceVOToUI(
        CircularlyAccessibleValueObject[] srcHeadVOs,
        CircularlyAccessibleValueObject[] srcBodyVOs)
        throws BusinessException {
      initBillItem();
      new DefaultCurrTypeBizDecimalListener(getBodyBillModel(), "ccurrencytypeid", new String[] {"noriginalcurmny", "noriginaltaxmny", "noriginaltaxpricemny"});
      iniBusiDecimal(((OrderItemVO)srcBodyVOs[0]).getCcurrencytypeid());
      
      super.setSourceVOToUI(srcHeadVOs, srcBodyVOs);
      PuTool.setBillModelConvertRate(getBodyBillModel(),
          new String[] { "cbaseid", "cassistunit", "nordernum",
              "nassistnum", "nconvertrate" });
//      int rowCount = getBodyBillModel().getRowCount();
//          //设置扣税类别
//          setCountTaxType(rowCount);
      // 设置自由项
      PoPublicUIClass.setFreeColValue(getBodyBillModel(),
          "vfree");

      // 处理来源单据类型、来源单据号
      // 最高限价
      // PoPublicUIClass.loadListMaxPrice(getbillListPanel(),m_listPoPubSetUI2)
      // ;
      nc.ui.scm.sourcebill.SourceBillTool.loadSourceInfoAll(
          getHeadBillModel(), "21");
    }
    public synchronized void headRowChange(int iNewRow) {
      super.headRowChange(iNewRow);
       // 设置换算率
       PuTool.setBillModelConvertRate(getBodyBillModel(),new  String[]{"cbaseid","cassistunit","nordernum","nassistnum","nconvertrate"}) ;
      // 设置自由项
      PoPublicUIClass.setFreeColValue(getBodyBillModel(),"vfree");
//       int rowCount = getBodyBillModel().getRowCount();
//          //设置扣税类别
//          setCountTaxType(rowCount);
    }
     public String getRefNodeCode() {
        return "400402020123";
      }
  }

  protected boolean isShowInvHandPane() {
    return false;
  }

  protected nc.ui.pub.beans.UIPanel getPanlCmd() {
    if (panlCmd == null) {
      try {
        panlCmd = new nc.ui.pub.beans.UIPanel();
        panlCmd.setName("PanlCmd");
        panlCmd.setPreferredSize(new java.awt.Dimension(0, 40));
        panlCmd.setLayout(new java.awt.FlowLayout());
        panlCmd.add(getbtnOk(), getbtnOk().getName());
        panlCmd.add(getbtnCancel(), getbtnCancel().getName());
        panlCmd.add(getbtnQuery(), getbtnQuery().getName());
        panlCmd.add(getbtnRefQry(), getbtnRefQry().getName());

        panlCmd.add(getbtnSplitMode(), getbtnSplitMode().getName());
        setSplitMethod();
      } catch (java.lang.Throwable ex) {
        handleException(ex);
      }
    }
    return panlCmd;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == getbtnSplitMode()) {
      onMethod();
      return;
    }
    super.actionPerformed(e);
  }

  private void onMethod() {
    int rettype = getMethodDlg().showModal();
    int rsltIndex = getMethodDlg().getMethodIndex();
    String rsltString = getMethodDlg().getM_strMethod();
    getMethodDlg().setM_strMethod(rsltString);
    // 重新设置分单参数
    if (rettype == UIDialog.ID_OK) {
      try {
        SysInitVO[] m_temps = new SysInitVO[1];
        if (m_initMethodVO == null) {
          m_initMethodVO = new SysInitVO();
          m_initMethodVO.setPk_corpid(sCorp);
        }
        m_temps[0] = m_initMethodVO;
        switch (rsltIndex) {
        case 0: {
          m_temps[0].setValue("供应商"); /*-=notranslate=-*/
          break;
        }
        case 1: {
          m_temps[0].setValue("存货+供应商");/*-=notranslate=-*/
          break;
        }
        case 2: {
          m_temps[0].setValue("订单"); /*-=notranslate=-*/
          break;
        }
        case 3: {
          m_temps[0].setValue("收货仓库+供应商"); /*-=notranslate=-*/
          break;
        }
        case 4: {
          m_temps[0].setValue("订单+收货仓库"); /*-=notranslate=-*/
          break;
        }
        }
        SysInitBO_Client.saveSysInitVOs(m_temps);
      } catch (Exception ex) {
        reportException(ex);
        MessageDialog
            .showErrorDlg(
                this,
                nc.ui.ml.NCLangRes.getInstance().getStrByID(
                    "SCMCOMMON", "UPPSCMCommon-000059")/*
                                       * @res
                                       * "错误"
                                       */,
                nc.ui.ml.NCLangRes.getInstance().getStrByID(
                    "40040301", "UPP40040301-000098")/*
                                       * @res
                                       * "回写分单方式时出错，请检查采购分单方式参数(PO17)是否设置正确"
                                       */);
      }
    }
  }

  public MethodDlg getMethodDlg() {
    if (m_dlgMethodDlg == null) {
      m_dlgMethodDlg = new MethodDlg(this, sCorp);
    }
    return m_dlgMethodDlg;
  }

  /**
   * 
   * 分单方式
   * <p>
   * <b>examples:</b>
   * <p>
   * 使用示例
   * <p>
   * <b>参数说明</b>
   * <p>
   * 
   * @author liyc
   * @time 2008-2-26 上午09:28:06
   */
  public void setSplitMethod() {
    try {
      if (m_initMethodVO == null)
        m_initMethodVO = SysInitBO_Client
            .queryByParaCode(sCorp, "PO17");
      if (m_initMethodVO != null) {
        m_initMethodVO.setPk_corpid(sCorp);
        String method = m_initMethodVO.getValue().trim();
        if (method.equals(nc.ui.ml.NCLangRes.getInstance().getStrByID(
            "common", "UC000-0000275")/* @res "供应商" */)) {
          getMethodDlg().getm_rdoCust().setSelected(true);
          getMethodDlg()
              .setM_strMethod(
                  nc.ui.ml.NCLangRes.getInstance()
                      .getStrByID("40040301",
                          "UPP40040301-000225")/*
                                       * @res
                                       * "按供应商"
                                       */);
        } else if (method
            .equals(nc.ui.ml.NCLangRes.getInstance().getStrByID(
                "40040301", "UPP40040301-000211")/*
                                   * @res
                                   * "存货+供应商"
                                   */)) {
          getMethodDlg().getm_rdoInv().setSelected(true);
          getMethodDlg()
              .setM_strMethod(
                  nc.ui.ml.NCLangRes.getInstance()
                      .getStrByID("40040301",
                          "UPP40040301-000226")/*
                                       * @res
                                       * "按存货+供应商"
                                       */);
        } else if (method
            .equals(nc.ui.ml.NCLangRes.getInstance().getStrByID(
                "40040301", "UPP40040301-000212")/* @res "订单" */)) {
          getMethodDlg().getm_rdoOrder().setSelected(true);
          getMethodDlg()
              .setM_strMethod(
                  nc.ui.ml.NCLangRes.getInstance()
                      .getStrByID("40040301",
                          "UPP40040301-000227")/*
                                       * @res
                                       * "按订单"
                                       */);
        } else if (method
            .equals(nc.ui.ml.NCLangRes.getInstance().getStrByID(
                "40040301", "UPP40040301-000213")/*
                                   * @res
                                   * "收货仓库+供应商"
                                   */)) {
          getMethodDlg().getm_rdoStor().setSelected(true);
          getMethodDlg()
              .setM_strMethod(
                  nc.ui.ml.NCLangRes.getInstance()
                      .getStrByID("40040301",
                          "UPP40040301-000228")/*
                                       * @res
                                       * "按收货仓库+供应商"
                                       */);
        } else if (method
            .equals(nc.ui.ml.NCLangRes.getInstance().getStrByID(
                "40040301", "UPP40040301-000214")/*
                                   * @res
                                   * "订单+收货仓库"
                                   */)) {
          getMethodDlg().getm_rdoOrdInv().setSelected(true);
          getMethodDlg()
              .setM_strMethod(
                  nc.ui.ml.NCLangRes.getInstance()
                      .getStrByID("40040301",
                          "UPP40040301-000229")/*
                                       * @res
                                       * "按订单+收货仓库"
                                       */);
        }
      }
    } catch (Exception ex) {
      if (ex.getMessage().indexOf("@czp@") >= 0) {
        MessageDialog
            .showErrorDlg(
                this,
                nc.ui.ml.NCLangRes.getInstance().getStrByID(
                    "SCMCOMMON", "UPPSCMCommon-000059")/*
                                       * @res
                                       * "错误"
                                       */,
                nc.ui.ml.NCLangRes.getInstance().getStrByID(
                    "40040301", "UPP40040301-000095")/*
                                       * @res
                                       * "分单方式为空，请检查采购分单方式参数(PO17)是否设置正确！[nc.ui.rc.receive.ArrFrmOrdUI.initialize()]->SysInitBO_Client.queryByParaCode(getCorpId(),“PO17”)"
                                       */);
      } else {
        MessageDialog.showErrorDlg(this, nc.ui.ml.NCLangRes.getInstance().getStrByID("SCMCOMMON", "UPPSCMCommon-000059")/* * @res
                                       * "错误"
                                       */,
                nc.ui.ml.NCLangRes.getInstance().getStrByID(
                    "40040301", "UPP40040301-000096")/*
                                       * @res
                                       * "获取分单方式时出错，请检查采购分单方式参数(PO17)是否设置正确！[nc.ui.rc.receive.ArrFrmOrdUI.initialize()]->SysInitBO_Client.queryByParaCode(getCorpId(),“PO17”)"
                                       */);
      }
      reportException(ex);
    }
  }

  public AggregatedValueObject[] getDistributedVOs(AggregatedValueObject[] vos) {
    if (vos == null || vos.length == 0 || vos[0] == null)
      return null;
    HashMap<String, String> hmEmployeeids = new HashMap<String, String>();
    HashMap<String, String> hmDeptids = new HashMap<String, String>();
    int iLen = vos.length;
    for (int i = 0; i < iLen; i++) {
      hmEmployeeids.put(((OrderHeaderVO) vos[i].getParentVO())
          .getCorderid(), ((OrderHeaderVO) vos[i].getParentVO())
          .getCemployeeid());
      hmDeptids.put(((OrderHeaderVO) vos[i].getParentVO()).getCorderid(),
          ((OrderHeaderVO) vos[i].getParentVO()).getCdeptid());
    }

    nc.vo.po.OrderVO[] vosRet = null;
    // 分单参数
    String sInitPara = null;
    String sVoName = "nc.vo.po.OrderVO";
    String sHeadVoName = "nc.vo.po.OrderHeaderVO";
    String sBodyVoName = "nc.vo.po.OrderItemVO";
    String[] saHeadKey = null;
    String[] saBodyKey = null;
    try {
      String sPara = getMethodDlg().getM_strMethod();

      // 获取分单参数
      sInitPara = nc.ui.pub.para.SysInitBO_Client.getParaString(
          sCorp, "PO17");
      if (sInitPara != null) {
        sPara = sInitPara.trim();
      }

      // 进行分单处理
      if (sPara.equals("供应商")) {/*-=notranslate=-*/
        saHeadKey = new String[] { "cvendormangid" };
        saBodyKey = new String[] { "pk_arrvstoorg" };
      } else if (sPara.equals("存货+供应商")) {/*-=notranslate=-*/
        saHeadKey = new String[] { "cvendormangid" };
        saBodyKey = new String[] { "cmangid", "pk_arrvstoorg" };
      } else if (sPara.equals("订单")) {/*-=notranslate=-*/
        saHeadKey = new String[] { "corderid", "cvendormangid" };
        saBodyKey = new String[] { "pk_arrvstoorg" };
      } else if (sPara.equals("收货仓库+供应商")) {/*-=notranslate=-*/
        saHeadKey = new String[] { "cvendormangid" };
        saBodyKey = new String[] { "pk_arrvstoorg", "cwarehouseid" };
      } else if (sPara.equals("订单+收货仓库")) {/*-=notranslate=-*/
        saHeadKey = new String[] { "corderid", "cvendormangid" };
        saBodyKey = new String[] { "pk_arrvstoorg", "cwarehouseid" };
      }
      vosRet = (OrderVO[]) SplitBillVOs.getSplitVOs(sVoName, sHeadVoName,
          sBodyVoName, vos, saHeadKey, saBodyKey);
      // ***
      nc.vo.po.OrderHeaderVO headVO = null;
      nc.vo.po.OrderItemVO[] bodyvos = null;
      for (int i = 0; i < vosRet.length; i++) {
        headVO = vosRet[i].getHeadVO();
        bodyvos = vosRet[i].getBodyVO();

        // 判断分单后的采购员是否与订单表头采购员相同，如果不同，则清空采购员
        for (int j = 0; j < bodyvos.length; j++) {
          if (PuPubVO.getString_TrimZeroLenAsNull(hmEmployeeids.get(bodyvos[j].getCorderid())) != null && !hmEmployeeids.get(bodyvos[j].getCorderid()).equals(
              hmEmployeeids.get(headVO.getCorderid()))) {
            vosRet[i].getHeadVO().setCemployeeid(null);
            break;
          }
        }
        // 判断分单后的部门是否与订单表头采购员相同，如果不同，则清空部门
        // 处理分单方式中部门处理错误的问题
        for (int j = 0; j < bodyvos.length; j++) {
          if (!hmDeptids.get(bodyvos[j].getCorderid()).equals(
              hmDeptids.get(headVO.getCorderid()))) {
            vosRet[i].getHeadVO().setCdeptid(null);
            break;
          }
        }
      }

    } catch (Exception e) {
      reportException(e);
    }
    return vosRet;
  }

  protected void onOk() {
    try {
      retBillVos = getbillListPanel().getSelectedSourceVOs();

    } catch (BusinessException e) {
      showErroMessage(e.getMessage());
      return;
    }
    if (retBillVos == null || retBillVos.length <= 0) {
      showErroMessage(nc.ui.ml.NCLangRes.getInstance().getStrByID(
          "SCMCOMMON", "UPPSCMCommon-000199")/* @res "请选择单据" */);
      return;
    }
    HashMap<String, String> hmOrderHids = new HashMap<String, String>();
	for (int i = 0; i < retBillVos.length; i++) {
		nc.vo.po.OrderVO object = (OrderVO) retBillVos[i];
		if (object.getHeadVO() != null && object.getHeadVO().getCorderid() != null) {
			hmOrderHids.put(object.getHeadVO().getCorderid(), object.getHeadVO().getTs());
		}

	}
    retBillVos = getDistributedVOs(retBillVos);
    retBillVos = generateArriveorderVOsPo(retBillVos);
    if (hmOrderHids != null && hmOrderHids.size() > 0) {
  	  for (int i = 0; i < retBillVos.length; i++) {
  		  ArriveorderVO arrbills = (ArriveorderVO) retBillVos[i];
  		  if (arrbills.getBodyLen() > 0) {
  			  ArriveorderItemVO[] arritems = arrbills.getBodyVo();
  			  for (int j = 0; j < arritems.length; j++) {
  				  ArriveorderItemVO itemVO = arritems[j];
  				  if (itemVO != null) {
  					  if(itemVO.getCupsourcebillid() != null){
  						  itemVO.setTshup(hmOrderHids.get(itemVO.getCupsourcebillid()));
  					  }
  				  }
  			  }
  		  }
  	  }
    }

    this.getAlignmentX();
    this.closeOK();
  }

  @SuppressWarnings("unchecked")
public void loadHeadData() {
    Object[] result = new Object[3];
    Vector headVOs = new Vector();
    OrderHeaderVO[] headVOsForPo = null;
    OrderItemVO[] itemVOsTForPo = null;
    nc.vo.sc.order.OrderHeaderVO[] headVOsForSC = null;
    nc.vo.sc.order.OrderItemVO[] itemVOsTForSC = null;
    Vector itemVOs = new Vector();
    nc.vo.po.OrderVO[] poOrdVOs = null;

    poOrdVOs = ((nc.ui.po.pub.PoToRcQueDLG) (QueryConditionClient())).getResultVOs();

    if (poOrdVOs != null && poOrdVOs.length > 0) {
      for (int i = 0; i < poOrdVOs.length; i++) {
        Vector priorPricesV = new Vector();
        int priorPrice = 0;
        Integer[] priorPrices = null;
        for (int j = 0; j < poOrdVOs[i].getBodyVO().length; j++) {
          priorPrice = PuTool.getPricePriorPolicy(getPkCorp());
          priorPricesV.add(priorPrice);
        }
        if (priorPricesV.size() > 0) {
          priorPrices = new Integer[priorPricesV.size()];
          priorPricesV.copyInto(priorPrices);
        }
        // 参照有到货计划的采购订单生成到货单时，转单界面金额应根据订单单价和到货计划行数量计算，目前都取的订单行金额。
        calRelationBantchPriorPrice(poOrdVOs[i], priorPrices);

      }
      for (int i = 0; i < poOrdVOs.length; i++) {
        headVOs.add(poOrdVOs[i].getHeadVO());
        for (int j = 0; j < poOrdVOs[i].getBodyVO().length; j++) {
          itemVOs.add(poOrdVOs[i].getBodyVO()[j]);
        }
      }
    }
    if (headVOs.size() > 0) {
      headVOsForPo = new OrderHeaderVO[headVOs.size()];
      headVOs.copyInto(headVOsForPo);
    }
    if (itemVOs.size() > 0) {
      itemVOsTForPo = new OrderItemVO[itemVOs.size()];
      itemVOs.copyInto(itemVOsTForPo);
    }
    result[0] = headVOsForPo;
    result[1] = itemVOsTForPo;
    try{
    if (result != null && result[0] != null
        && ((Object[]) result[0]).length > 0) {

      getbillListPanel().setSourceVOToUI(
          (CircularlyAccessibleValueObject[]) result[0],
          (CircularlyAccessibleValueObject[]) result[1]);
      if (result.length >= 3 && result[2] != null
          && result[2].toString().trim().length() > 0)
        showHintMessage(result[3].toString());
    } else {
      getbillListPanel().clearUIData();
    }
    isUpdateUIDataWhenSwitch = true;
    }catch (Exception e) {
      nc.vo.scm.pub.SCMEnv.out("数据加载失败！");
      nc.vo.scm.pub.SCMEnv.out(e);
      showErroMessage(nc.ui.ml.NCLangRes.getInstance().getStrByID(
          "SCMCOMMON", "UPPSCMCommon-000255")/* @res "数据加载失败" */);
      return;
    }
  }

  public static void calRelationBantchPriorPrice(nc.vo.po.OrderVO voOrder,
      Integer[] iPriorPrice) {
    // nc.vo.po.OrderHeaderVO voHead = voOrder.getHeadVO();
    nc.vo.po.OrderItemVO[] voaItem = voOrder.getBodyVO();

    int iLen = voaItem.length;
    for (int i = 0; i < iLen; i++) {
      nc.vo.po.OrderItemVO voCurItem = voaItem[i];
      if (voCurItem.getNaccumarrvnum() == voCurItem.getNordernum())
        if (iPriorPrice[i] == RelationsCalVO.PRICE_PRIOR_TO_TAXPRICE
            || iPriorPrice[i] == RelationsCalVO.TAXPRICE_PRIOR_TO_PRICE) {
          OrderPubVO.calRelation(voCurItem, iPriorPrice[i]);
        }
    }
  }

  public QueryConditionClient QueryConditionClient() {
    return ((BillReferQueryProxyPoToDHYSD) super.getQueyDlg()).createOldQryDlg();
  }
  @SuppressWarnings("unchecked")
private AggregatedValueObject[] generateArriveorderVOsPo(AggregatedValueObject[] ordbills) {
    ArriveorderVO[] arrbills = new ArriveorderVO[ordbills.length];
    try {
      nc.vo.po.OrderHeaderVO ordhead = new nc.vo.po.OrderHeaderVO();
      nc.vo.po.OrderItemVO[] orditems = null;
      ArriveorderItemVO[] arritems = null;
      
  for (int i = 0; i < ordbills.length; i++) {
        orditems = (nc.vo.po.OrderItemVO[]) ordbills[i].getChildrenVO();
        ordhead = (nc.vo.po.OrderHeaderVO) ordbills[i].getParentVO();

        //转换前的处理
        arrbills[i] = new ArriveorderVO();
        UFDouble ordnum = null, ordacc = null, arrnum = null, nbackarrvnum = null, nbackstorenum = null;
        String pk_ord_b = null;
        String pk_ord_bb1id = null;
        h_NumThis = new Hashtable();
        h_ordNum = new Hashtable();
        h_accNum = new Hashtable();
        for (int j = 0; j < orditems.length; j++) {
          //订单数量哈希表
          pk_ord_b = orditems[j].getCorder_bid();
          pk_ord_bb1id = orditems[j].getCorder_bb1id();
          if (pk_ord_bb1id == null || pk_ord_bb1id.trim().length() == 0)
            pk_ord_bb1id = "NULL";
          ordnum = orditems[j].getNordernum();
          if (ordnum == null || ordnum.toString().trim().equals("")) {
            ordnum = new UFDouble(0);
          }
          //累计到货数量哈希表
          ordacc = orditems[j].getNaccumarrvnum();
          if (ordacc == null || ordacc.toString().trim().equals("")) {
            ordacc = new UFDouble(0);
          }
          //本次可到货数量（订单提供）
          arrnum = (UFDouble) orditems[j].getNnotarrvnum();
          //本次可到货数量(退货)
          if (isBack()) {
            nbackarrvnum = orditems[j].getNbackarrvnum();
            nbackstorenum = orditems[j].getNbackstorenum();
            if (nbackarrvnum == null)
              nbackarrvnum = new UFDouble(0.0);
            if (nbackstorenum == null)
              nbackstorenum = new UFDouble(0.0);
            if (ordnum.doubleValue() <= 0) {
              arrnum = ordnum.add(nbackarrvnum).add(nbackstorenum);
            } else {
              arrnum = ordacc.sub(nbackarrvnum).sub(nbackstorenum).multiply(-1.0);
            }
          }
          //本次默认到货数量
          h_NumThis.put(pk_ord_b + pk_ord_bb1id, PuPubVO.getUFDouble_NullAsZero(arrnum));
          //订单数量
          h_ordNum.put(pk_ord_b + pk_ord_bb1id, PuPubVO.getUFDouble_NullAsZero(ordnum));
          //订单累计到货数量
          h_accNum.put(pk_ord_b + pk_ord_bb1id, PuPubVO.getUFDouble_NullAsZero(ordacc));
          
          h_ordSelectedRow.put(pk_ord_b + pk_ord_bb1id, new Integer(j));
        }

        //VO转换
        try {
          arrbills[i] = (ArriveorderVO) PfChangeBO_Client.pfChangeBillToBill(ordbills[i], ScmConst.PO_Order, ScmConst.PO_Arrive);

          ////////////////////////////////////////////////////////////////////////////////////////////
          //转换后的处理开始

          // 设置到货单的上层时间戳 TS：
          arritems = (ArriveorderItemVO[]) arrbills[i].getChildrenVO();
                  //设置行号
          BillRowNo.setVORowNoByRule(arritems,IBillType.ARRIVEBILL,"crowno");
          //since v502, VO对照中处理
//          }
          //单据属性
          //订单数量
          arrbills[i].setH_ordNum(h_ordNum);
          //累计到货
          arrbills[i].setH_accNum(h_accNum);
          //上层单据类型
          arrbills[i].setUpBillType(ScmConst.PO_Order);
          //表头
          //状态为自由
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setIbillstatus(new Integer(IArriveorderStatus.FREE));
          //会计年度
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setCaccountyear(getAccYear());
          //单据类型
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setCbilltype(ScmConst.PO_Arrive);
          //到货日期
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setDreceivedate(getSysDate());
          //收货人(默认操作员关联人员档案，参照人员档案编辑，可空)
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setCreceivepsn(getPsnIdByOperID(sOperator));
          //制单人
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setCoperator(sOperator);
          //供应商管理档案ID
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setCvendormangid(ordhead.getCvendormangid());
          //供应商档案ID
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setCvendorbaseid(ordhead.getCvendorbaseid());
          //是否退货
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setBisback(new UFBoolean(isBack()));
          //审批日期
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setDauditdate(null);
          //审批人
           ((ArriveorderHeaderVO) arrbills[i].getParentVO()).setCauditpsn(null);

          //表体
          //表行对应的模板中位置
          int iOrdSelectedRow = 0;
          String corder_bid = null;
          String corder_bb1id = null;
          Object onarrvnum = null;
          Object onpresentnum = null;
          Object onwastnum = null;
          Object ocassist = null;
          Object oconvert = null;
          Object oarrvass = null;
          for (int k = 0; k < arrbills[i].getChildrenVO().length; k++) {
            ArriveorderItemVO vo = ((ArriveorderItemVO[]) arrbills[i].getChildrenVO())[k];
            //上层来源ID
            corder_bid = vo.getCupsourcebillrowid();
            vo.setCorder_bid(corder_bid);
            vo.setCorderid(vo.getCupsourcebillid());
            corder_bb1id = vo.getCorder_bb1id();
            if (corder_bb1id == null || corder_bb1id.trim().length() == 0)
              corder_bb1id = "NULL";
            //本次到货数量
            UFDouble anum = (UFDouble) h_NumThis.get(corder_bid + corder_bb1id);
            vo.setNarrvnum(anum);

            //最大到货数量 = 本次到货数量
            vo.setMaxarrnum(anum);
            //表体公司ID
            vo.setPk_corp(sCorp);

            //得到编辑的内容在模板中的位置
            iOrdSelectedRow = ((Integer) h_ordSelectedRow.get(corder_bid + corder_bb1id)).intValue();
            //本次收货(如果为空则用“本次到货数量”)
            onarrvnum = getbillListPanel().getSourceBodyValueAt(iOrdSelectedRow, "narrvnum");
            vo.setNarrvnum(
              (onarrvnum == null || onarrvnum.toString().trim().equals("")) ? anum : new UFDouble(onarrvnum.toString().trim()));
            //赠品数量
            if (PuPubVO.getString_TrimZeroLenAsNull(getbillListPanel().getSourceBodyValueAt(iOrdSelectedRow,"blargess")) != null) {
              Boolean bLargess = (Boolean) getbillListPanel().getSourceBodyValueAt(iOrdSelectedRow,"blargess");
              if (bLargess.booleanValue()) {
                vo.setNpresentnum(vo.getNarrvnum());
                vo.setNpresentassistnum(vo.getNassistnum());
              } else {
                vo.setNpresentnum(null);
                vo.setNpresentassistnum(null);
              }
            }
            
            //途耗数量
            onwastnum = getbillListPanel().getSourceBodyValueAt(iOrdSelectedRow, "nwastnum");
            vo.setNwastnum(
              (onwastnum == null || onwastnum.toString().trim().equals("")) ? null : new UFDouble(onwastnum.toString().trim()));
//            //辅数量
            oarrvass = vo.getNassistnum();
            vo.setNassistnum(
              (oarrvass == null || (oarrvass.toString().trim().equals("") || (oarrvass.toString().trim().equals("0.00") || oarrvass.toString().trim().equals("0")))) ? null : new UFDouble(oarrvass.toString().trim()));
//            //换算率
//            oconvert = getbillListPanel().getSourceBodyValueAt(iOrdSelectedRow, "convertrate");
//            vo.setConvertrate(
//              (oconvert == null || oconvert.toString().trim().equals("")) ? null : new UFDouble(oconvert.toString().trim()));
            //手工转换自由项
            nc.vo.po.OrderItemVO ordItem = (nc.vo.po.OrderItemVO) ordbills[i].getChildrenVO()[k];
            vo.setVfree0(ordItem.getVfree());
            //处理源头信息
            if (vo.getCupsourcebillid() != null
              && vo.getCupsourcebillid().length() > 0
              && (vo.getCsourcebillid() == null || vo.getCsourcebillid().length() == 0)) {
              vo.setCsourcebillid(vo.getCupsourcebillid());
              vo.setCsourcebillrowid(vo.getCupsourcebillrowid());
              vo.setCsourcebilltype(vo.getCupsourcebilltype());
            }
            
          }
                  
          //转换后的处理结束
          ////////////////////////////////////////////////////////////////////////////////////////////
        } catch (Exception e) {
          SCMEnv.out(e);
        }
      }
      //设置返回参照生成的到货单
      setM_arriveVOs(arrbills);
      if (arrbills != null && arrbills.length > 0) {
        //处理自由项
        nc.ui.scm.pub.FreeVOParse freeParse = new nc.ui.scm.pub.FreeVOParse();
        for (int i = 0; i < arrbills.length; i++) {
          freeParse.setFreeVO(arrbills[i].getChildrenVO(), "vfree0", "vfree", "cbaseid", "cmangid", true);
        }
      }
      
    } catch (Exception e) {
      reportException(e);
    }
    return arrbills;
  }
  public boolean isBack() {
    return m_bBack;
  }
  private void setM_arriveVOs(nc.vo.rc.receive.ArriveorderVO[] newM_arriveVOs) {
    m_arriveVOs = newM_arriveVOs;
  }
  private String getPsnIdByOperID(String userid) {
    if (userid == null) return null;
    if (userid.trim().equals("")) return null;
    userid = userid.trim();
    String psnid = null;
    try {
      psnid = ArriveorderHelper.getPkPsnByPkOper(userid);
    } catch (Exception e) {
      //reportException(e);
      SCMEnv.out("根据操作员关联人员档案时出错");
      psnid = null;
    }
    return psnid;
  }
  private String getAccYear() {
    return ClientEnvironment.getInstance().getAccountYear();
  }
  private UFDate getSysDate() {
    return ClientEnvironment.getInstance().getDate();
  }
  
  
   /**
     * 设置扣税类别。
     */
  private void setCountTaxType(int rowCount) {
    int index = 0 ;
        String strDisType = null;
        for(int i = 0 ; i < rowCount; i ++){
          if (PuPubVO.getString_TrimZeroLenAsNull(getbillListPanel().getSourceBodyValueAt(i, "idiscounttaxtype")) == null) {
            index = 0;
          }else{
            index = new Integer(getbillListPanel().getSourceBodyValueAt(i,"idiscounttaxtype").toString()).intValue();
            if(index == 0){
              strDisType = VariableConst.IDISCOUNTTAXTYPE_NAME_INNER_NO_TRANS;//应税内含
            }else if (index == 1){
              strDisType = VariableConst.IDISCOUNTTAXTYPE_NAME_OUTTER_NO_TRANS;//应税外加
            }else if (index == 2){
              strDisType = VariableConst.IDISCOUNTTAXTYPE_NAME_NOCOUNT_NO_TRANS;//不计税
            }
          }
        if (strDisType != null && strDisType.trim().length() > 0) {
          if(getbillListPanel() instanceof SimpBillRefListPanel){
            getbillPanel().getHeadBillModel().setValueAt(strDisType, i, "chd_idiscounttaxtype");
          }else{
            getbillPanel().getBodyBillModel().setValueAt(strDisType, i, "idiscounttaxtype");
          }
        } else {
          if(getbillListPanel() instanceof SimpBillRefListPanel){
            getbillPanel().getHeadBillModel().setValueAt(VariableConst.IDISCOUNTTAXTYPE_NAME_OUTTER, i, "chd_idiscounttaxtype");
          }else{
            getbillPanel().getBodyBillModel().setValueAt(VariableConst.IDISCOUNTTAXTYPE_NAME_OUTTER, i, "idiscounttaxtype");
          }
        }
        }
  }
  public BillListPanel getbillPanel() {
    if (getBilltobillrefpanel().getShowstate() == ShowState.DoubleTable)
      return getBilltobillrefpanel().getDoubleBillListPanel();
    else
      return getBilltobillrefpanel().getOneBilListPanel();
  }
    public String getRefTempletBilltypecode(){
        return "2123";
  }
}
