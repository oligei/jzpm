package nc.ui.sourceref.po;

import java.awt.Container;

import nc.ui.pub.query.QueryConditionClient;
import nc.ui.querytemplate.QueryConditionDLG;
import nc.ui.scm.pub.query.SCMQueryConditionDlg;
import nc.ui.scm.pub.sourceref.IBillReferQueryProxy;
import nc.vo.querytemplate.TemplateInfo;

public class BillReferQueryProxyPoToDHYSD extends IBillReferQueryProxy{
	nc.ui.po.pub.PoToRcQueDLG dlg = null;
	public BillReferQueryProxyPoToDHYSD() {
	}

	public BillReferQueryProxyPoToDHYSD(Container parent, TemplateInfo ti) {
		super(parent, ti);
	}

	public BillReferQueryProxyPoToDHYSD(Container parent) {
		super(parent);
	}
	@Override
	public QueryConditionDLG createNewQryDlg() {
		return null;
	}

	@Override
	public QueryConditionClient createOldQryDlg() {
		if (dlg == null) {
			dlg = new nc.ui.po.pub.PoToRcQueDLG(getContainer(), getPkCorp(),getOperator(), getBusinessType());
			dlg.setBillRefModeSelPanel(true);
		}
		return dlg;
	}

	@Override
	public boolean isNewQryDlg() {
		return false;
	}

	public boolean isShowDoubleTableRef() {
		return ((SCMQueryConditionDlg) createOldQryDlg()).isShowDoubleTableRef();
	}

	@Override
	public void setUserRefShowMode(boolean isShowDoubleTableRef) {
		((SCMQueryConditionDlg) createOldQryDlg()).setBillRefShowMode(isShowDoubleTableRef);
	}

}
