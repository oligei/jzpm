package nc.bs.pf.changedir;

/**
 * 
 * create by hmx 2019-6-3
 * */
public class CHG21TO990898 extends nc.bs.pf.change.VOConversion{
	/**
	 * CHG21TO23 构造子注解。
	 */
	public CHG21TO990898() {
		super();
	}

	/**
	* 获得字段对应。
	* @return java.lang.String[]
	*/
	public String[] getField() {
		return new String[] {"H_vdef1->H_vdef1"};
	}
}
