//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.mt100505;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import com.sun.xml.xsom.impl.scd.Iterators.Map;
import com.ufsoft.script.base.UfoDouble;

import nc.bs.framework.common.NCLocator;
import nc.bs.logging.Logger;
import nc.itf.jzpm.mtpub.IJZPMMTStatelessService;
import nc.itf.uap.IUAPQueryBS;
import nc.itf.uap.IVOPersistence;
import nc.jdbc.framework.processor.BeanListProcessor;
import nc.jdbc.framework.processor.MapProcessor;
import nc.ui.jz.pub.excel2.importer.NCExcelImportAdapter;
import nc.ui.jz.pub.ref.JZMultiCorpAllObjProjRefTreeModel;
import nc.ui.jz.pub.ref.NoFinishObjProjRefTreeModel;
import nc.ui.jz.pub.subtotal.BillLevelSubTotalAction;
import nc.ui.jz.report.MultiCorpDefaultQueryDlg;
import nc.ui.jzpm.mtpub.AppendPlanCheck;
import nc.ui.jzpm.mtpub.LinkDLg;
import nc.ui.jzpm.mtpub.turnmaterial.JZPMTMProxy;
import nc.ui.ml.NCLangRes;
import nc.ui.pub.ButtonObject;
import nc.ui.pub.beans.MessageDialog;
import nc.ui.pub.beans.UIDialog;
import nc.ui.pub.beans.UIRefPane;
import nc.ui.pub.beans.UITable;
import nc.ui.pub.bill.BillCardPanel;
import nc.ui.pub.bill.BillListPanel;
import nc.ui.pub.bill.BillModel;
import nc.ui.pub.pf.PfUtilClient;
import nc.ui.trade.bill.BillListPanelWrapper;
import nc.ui.trade.controller.IControllerBase;
import nc.ui.trade.manage.BillManageUI;
import nc.ui.trade.query.INormalQuery;
import nc.vo.jz.pub.excel2.itf.IExcelImportCfg;
import nc.vo.jzpm.cmpub.contract.CmContractVO;
import nc.vo.jzpm.cmpub.contract.CmStocklistVO;
import nc.vo.jzpm.mt100505.AggVO;
import nc.vo.jzpm.mt100505.MtCtplanBSumVO;
import nc.vo.jzpm.mt100505.MtCtplanBVO;
import nc.vo.jzpm.mt100505.MtCtplanVO;
import nc.vo.jzpm.mtpub.AggregatedVO;
import nc.vo.jzpm.mtpub.CommonUtil;
import nc.vo.jzpm.mtpub.exception.CtrlPlanNumException;
import nc.vo.ml.NCLangRes4VoTransl;
import nc.vo.pub.AggregatedValueObject;
import nc.vo.pub.BusinessException;
import nc.vo.pub.CircularlyAccessibleValueObject;
import nc.vo.pub.SuperVO;
import nc.vo.pub.lang.UFBoolean;
import nc.vo.pub.lang.UFDateTime;
import nc.vo.pub.lang.UFDouble;
import nc.vo.querytemplate.TemplateInfo;
import net.sf.antcontrib.logic.Throw;

public class ClientEventHandler extends nc.ui.jzpm.mtpub.consumetplan.ClientEventHandler {
    private static final String m_split_down = "jzpm_mt_ctplan_b2";
    private boolean isAppend;
    private List<Integer> buttonList = null;
    public static final int AssQuery = 511;
    ClientUI billui;

    public ClientEventHandler(BillManageUI billUI, IControllerBase control) {
        super(billUI, control);
        this.billui = (ClientUI)billUI;
        this.initButtonList();
    }

    protected void onBoEdit() throws Exception {
        super.onBoEdit();
        this.setNappendnumState();
        int tableIndex = this.getCurTabIndex();
        if (!this.getBillManageUI().isListPanelSelected() && tableIndex == 1) {
            this.removeSubTotalFlag();
        }

        AggVO aggvo = (AggVO)this.getBufferData().getCurrentVOClone();
        SuperVO[] bvo = (SuperVO[])aggvo.getTableVO("jzpm_mt_ctplan_b");
        if (bvo != null && bvo.length > 0) {
            SuperVO[] var7 = bvo;
            int var5 = 0;

            for(int var6 = bvo.length; var5 < var6; ++var5) {
                SuperVO vo = var7[var5];
                String pk_mandoc = vo.getAttributeValue("pk_invmandoc").toString();
                UFDouble price = new UFDouble(vo.getAttributeValue("price").toString());
                if (!this.billui.getPriceMap().containsKey(pk_mandoc)) {
                    this.billui.getPriceMap().put(pk_mandoc, price);
                }
            }
        }

    }

    private void removeSubTotalFlag() throws Exception {
        this.onBoRefresh();
    }

    @SuppressWarnings("unchecked")
	public void onBoAudit() throws Exception {
        AggregatedValueObject modelVo = this.getBufferData().getCurrentVOClone();
        this.setCheckManAndDate(modelVo);
        
        HashMap<String, UFDouble> invbasdoc_usedNumMap=null;//map--- pk_invbasdoc:usednum 已使用数量传递
        List<CmStocklistVO> oldCmStocklistVOs = null; //采购合同，表体数据
       
        if (this.checkVOStatus(modelVo, new int[]{1})) {
            System.out.println("无效的鼠标处理机制");
        } else {
            this.beforeOnBoAction(26, modelVo);
            AggregatedValueObject retVo = this.getBusinessAction().approve(modelVo, this.getUIController().getBillType(), this.getBillUI()._getDate().toString(), this.getBillUI().getUserObject());
            if (PfUtilClient.isSuccess()) {
                String pk_head = retVo.getParentVO().getPrimaryKey();
                IUAPQueryBS query = (IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class);
                this.afterOnBoAction(26, modelVo);//该方法啥都没干
                this.updateHeadFixFields(modelVo, retVo, this.getFixFields(26, retVo.getParentVO()));
                UFBoolean bisappend = ((MtCtplanVO)modelVo.getParentVO()).getBisappend();//追加
                if (bisappend.equals(new UFBoolean(true))) {
                    pk_head = ((MtCtplanVO)modelVo.getParentVO()).getPk_mt_normalplan();
                    
                    String  jzpm_mt_ctplan_b_sum_sql = "select pk_invbasdoc,usednum,pk_mt_ctplan_b_sum from jzpm_mt_ctplan_b_sum where pk_mt_ctplan = '"+pk_head+"' and nvl(dr, 0) = 0  ";
                    List<MtCtplanBSumVO> oldMtCtplanBSumVOs = (List<MtCtplanBSumVO>) query.executeQuery(jzpm_mt_ctplan_b_sum_sql, new BeanListProcessor(MtCtplanBSumVO.class));
                    if (oldMtCtplanBSumVOs != null && oldMtCtplanBSumVOs.size() > 0) {
                    	invbasdoc_usedNumMap = new HashMap<String, UFDouble>();
                    	
                    	StringBuilder stocklistSql = new StringBuilder("select * from jzpm_cm_stocklist where 1=1 ");
                    	for (int i = 0; i < oldMtCtplanBSumVOs.size(); i++) {
                    		MtCtplanBSumVO mtCtplanBSumVO = oldMtCtplanBSumVOs.get(i);
                    		invbasdoc_usedNumMap.put(mtCtplanBSumVO.getPk_invbasdoc(), mtCtplanBSumVO.getUsednum());
                    		if(i==0){//SQL语句拼接   //当下根据表体来查询数据，后来发现可以根据表头来查
                    			stocklistSql.append("and vlastbillrowid in ('"+mtCtplanBSumVO.getPk_mt_ctplan_b_sum()+"'");
                    		}else{
                    			stocklistSql.append(",'"+mtCtplanBSumVO.getPk_mt_ctplan_b_sum()+"'");
                    		}
                    		if(i==oldMtCtplanBSumVOs.size()-1){
                    			stocklistSql.append(") and nvl(dr, 0) = 0 ");
                    		}
                    	}
                    	oldCmStocklistVOs = (List<CmStocklistVO>) query.executeQuery(stocklistSql.toString(), new BeanListProcessor(CmStocklistVO.class));
					}
                    //3、特别注意，我擦，这里进行物理删除操作
                    ((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).deleteByClause(MtCtplanBSumVO.class, " pk_mt_ctplan='" + pk_head + "' ");
                }
                
                StringBuffer sb = new StringBuffer(); //查询数据，统计求和，构造新的MtCtplanBSumVO
                sb.append("select b.bissupplied,b.dr,");
                sb.append("sum(b.naccumappendnum) as naccumappendnum, ");
                sb.append("sum(b.naccummonthnum) as naccummonthnum,");
                sb.append("sum(b.naccumwishnum) as naccumwishnum,");
                sb.append("sum(b.nappendnum) as nappendnum,");
                sb.append("sum(b.ndifferratio) as ndifferratio, ");
                sb.append("sum(b.nnum) as nnum, ");
                sb.append("sum(b.nmoney) as nmoney,");
                sb.append("b.pk_invbasdoc,");
                sb.append("b.price,");
                sb.append("b.pk_invmandoc, ");
                sb.append("b.pk_mt_ctplan");
                sb.append("  from jzpm_mt_ctplan_b b  ");
                sb.append(" where nvl(dr, 0) = 0  ");
                sb.append("   and b.pk_mt_ctplan = '" + pk_head + "'");
                sb.append(" group by b.pk_invbasdoc, b.pk_invmandoc, b.pk_mt_ctplan, b.bissupplied,b.dr,b.price");
                List<MtCtplanBSumVO> newMtCtplanBSumVOs = (List)query.executeQuery(sb.toString(), new BeanListProcessor(MtCtplanBSumVO.class));
                
                if (newMtCtplanBSumVOs != null && newMtCtplanBSumVOs.size() > 0) {
                	if(invbasdoc_usedNumMap!=null){//已使用数量赋值
                		for (MtCtplanBSumVO mtCtplanBSumVO : newMtCtplanBSumVOs) {
							mtCtplanBSumVO.setUsednum(invbasdoc_usedNumMap.get(mtCtplanBSumVO.getPk_invbasdoc()));
						}
                	}
                    ((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).insertVOList(newMtCtplanBSumVOs);//只返回个主键数组，用处不大
                 
                    if(oldCmStocklistVOs!=null && oldCmStocklistVOs.size()>0){//由于前面进行物理删除，此时得更新合同表体依赖的pk_mt_ctplan_b_sum
                    	HashMap<String,String> invbasdocSumpkMap = new HashMap();//获取新PK
                        String  jzpm_mt_ctplan_b_sum_sql = "select pk_invbasdoc,pk_mt_ctplan_b_sum from jzpm_mt_ctplan_b_sum where pk_mt_ctplan = '"+pk_head+"' and nvl(dr, 0) = 0 ";
                        List<MtCtplanBSumVO> savedMtCtplanBSumVOs = (List<MtCtplanBSumVO>) query.executeQuery(jzpm_mt_ctplan_b_sum_sql, new BeanListProcessor(MtCtplanBSumVO.class));
                        if(savedMtCtplanBSumVOs!=null && savedMtCtplanBSumVOs.size()>0){
                        	for (MtCtplanBSumVO mtCtplanBSumVO : savedMtCtplanBSumVOs) {
                        		invbasdocSumpkMap.put(mtCtplanBSumVO.getPk_invbasdoc(), mtCtplanBSumVO.getPk_mt_ctplan_b_sum());
    						}
                        }
                       
            			for (int i = 0; i < oldCmStocklistVOs.size(); i++) {//更新pk_mt_ctplan_b_sum
            				CmStocklistVO  vo= oldCmStocklistVOs.get(i);
            				vo.setVlastbillrowid(invbasdocSumpkMap.get(vo.getPk_invbasdoc()));
            				vo.setTs(new UFDateTime());//更新ts相当重要
						}
            			((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).updateVOList(oldCmStocklistVOs);
            			String  contractSQL = "select * from jzpm_cm_contract where pk_contract='"+oldCmStocklistVOs.get(0).getPk_contract()+"'";
            			List<CmContractVO> ctVos = (List<CmContractVO>) query.executeQuery(contractSQL, new BeanListProcessor(CmContractVO.class));
            			if(ctVos!=null && ctVos.size()>0){
           				 ctVos.get(0).setTs(oldCmStocklistVOs.get(oldCmStocklistVOs.size()-1).getTs());
           				 ((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).updateVOList(ctVos);
           				 
           			 }
            			
            			
            		}
                }
            }

        }
    }

    private void setCheckManAndDate(AggregatedValueObject vo) throws Exception {
        vo.getParentVO().setAttributeValue(this.getBillField().getField_CheckDate(), this.getBillUI()._getDate());
        vo.getParentVO().setAttributeValue(this.getBillField().getField_CheckMan(), this.getBillUI()._getOperator());
    }

    protected void onBoSave() throws Exception {
        try {
            super.onBoSave();
        } catch (CtrlPlanNumException var2) {
            if (MessageDialog.showYesNoDlg(this.getBillUI(), NCLangRes.getInstance().getStrByID("40040401", "UPPSCMCommon-000270"), var2.getMessage() + NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000003")) == 4) {
                this.getBillCardPanel().setHeadItem("userconfirmflag", new UFBoolean(true));
                super.onBoSave();
            }
        }

        this.getBillCardPanel().setHeadItem("userconfirmflag", new UFBoolean(false));
    }

    protected void onBoLineAdd() throws Exception {
        super.onBoLineAdd();
        if (this.getBillCardPanel().getRowCount() == 1) {
            this.getBillCardPanel().getHeadItem("pk_project").setEnabled(false);
        }

    }

    protected void onBoLineDel() throws Exception {
        super.onBoLineDel();
        AggregatedVO aggvo = (AggregatedVO)this.getBillUI().getVOFromUI();
        if (aggvo.getTableVO(aggvo.getTableCodes()[0]) == null || aggvo.getTableVO(aggvo.getTableCodes()[0]).length == 0) {
            this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_project").setEnabled(true);
        }

    }

    protected void onBoElse(int intBtn) throws Exception {
        try {
            super.onBoElse(intBtn);
            switch(intBtn) {
            case 586:
                this.showNormPlanRefModel();
                break;
            case 608:
                this.showLinkMtCtplanExe(intBtn);
                break;
            case 621:
                this.showQueryDlg(intBtn);
                break;
            case 726:
                this.onImportExcelData();
                break;
            case 774:
                this.subTotal(intBtn);
            }

        } catch (BusinessException var3) {
            throw var3;
        } catch (Exception var4) {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000004"));
        }
    }

    protected void onImportExcelData() throws BusinessException {
        if (this.getChildTreeUI().getBillTreeSelectNode() == null) {
            MessageDialog.showHintDlg(this.getChildTreeUI(), NCLangRes4VoTransl.getNCLangRes().getStrByID("JZBD_PUB", "UJZJZBD_PUB-8000005"), NCLangRes4VoTransl.getNCLangRes().getStrByID("JZBD_PUB", "UJZJZBD_PUB-8000024"));
        } else {
            int tabIndex = this.getBillCardPanelWrapper().getBillCardPanel().getBodyTabbedPane().getSelectedIndex();
            if (tabIndex != -1) {
                try {
                    NCExcelImportAdapter excelImporter = null;
                    if (tabIndex == 0) {
                        Object objVal = this.getBillCardPanel().getHeadItem("bisappend").getValueObject();
                        boolean isAppend = objVal == null ? false : (new UFBoolean(objVal.toString())).booleanValue();
                        IExcelImportCfg cfg = null;
                        if (isAppend) {
                            cfg = MtCtplanBVO.EXCEL_CFG_FACTROY_APPEND.getExcelImportCfg();
                        } else {
                            cfg = MtCtplanBVO.EXCEL_CFG_FACTROY_TOTAL.getExcelImportCfg();
                        }

                        excelImporter = new NCExcelImportAdapter(this.getBillUI(), cfg, MtCtplanBVO.class);
                    }

                    this.onImportExcel(excelImporter);
                } catch (Exception var6) {
                    Logger.error(var6.getMessage(), var6);
                    throw new BusinessException(var6.getMessage(), var6);
                }
            }
        }
    }

    protected void onImportExcel(NCExcelImportAdapter<? extends SuperVO> excelImporter) throws Exception {
        Object objVal = this.getBillCardPanel().getHeadItem("bisappend").getValueObject();
        this.isAppend = objVal == null ? false : (new UFBoolean(objVal.toString())).booleanValue();
        if (1 == excelImporter.showMode()) {
            List importedResVOs = null;
            if (importedResVOs != null && ((List)importedResVOs).size() != 0) {
                BillModel billModel = this.getBillCardPanelWrapper().getBillCardPanel().getBillModel("jzpm_mt_ctplan_b");
                UITable billTable = this.getBillCardPanelWrapper().getBillCardPanel().getBillTable("jzpm_mt_ctplan_b");
                int i = 0;

                for(int size = ((List)importedResVOs).size(); i < size; ++i) {
                    this.getBillCardPanelWrapper().getBillCardPanel().addLine("jzpm_mt_ctplan_b");
                    int rowIndex = billTable.getSelectedRow();
                    billModel.setBodyRowVO((CircularlyAccessibleValueObject)((List)importedResVOs).get(i), rowIndex);
                    billModel.setValueAt(((ClientUI)this.getBillUI()).getPreSelectedTreePK(), rowIndex, "pk_wbsdef");
                }

                this.execFormula(((List)importedResVOs).size());
            }
        }

    }

    public static List<MtCtplanBVO> getImportedResVO(MtCtplanBVO[] importedResVOs) {
        List result = new ArrayList();
        if (importedResVOs != null) {
            result.addAll((Collection)Arrays.asList(importedResVOs));
        }

        return result;
    }

    private List<MtCtplanBVO> processImportedResVO(List<MtCtplanBVO> importedResVOs, boolean isAppend) throws BusinessException {
        Set pkinvmancodes = new HashSet();
        Set admixcodes = new HashSet();
        String pk_corp = this.getBillUI()._getCorp().getPk_corp();
        Iterator var7 = importedResVOs.iterator();

        while(true) {
            MtCtplanBVO vo;
            do {
                if (!var7.hasNext()) {
                    vo = null;
                    IJZPMMTStatelessService mtservice = JZPMTMProxy.getMTStatelessService();
                    List vosList = mtservice.getPksByResVo4CtPlan(importedResVOs, (String[])pkinvmancodes.toArray(new String[0]), (String[])admixcodes.toArray(new String[0]), pk_corp, isAppend);
                    return vosList;
                }

                vo = (MtCtplanBVO)var7.next();
                pkinvmancodes.add(vo.getPk_invmandoc());
            } while(vo.getPk_admixtures() == null);

            String[] var11 = vo.getPk_admixtures().split(",");
            int var9 = 0;

            for(int var10 = var11.length; var9 < var10; ++var9) {
                String pk_admix = var11[var9];
                admixcodes.add(pk_admix);
            }
        }
    }

    private void execFormula(int size) throws BusinessException {
        BillModel bm = this.getBillCardPanel().getBillModel("jzpm_mt_ctplan_b");
        List formulas = new ArrayList();
        String[] keys = new String[]{"invcode", "vdispwbscode", "nnum"};
        String[] var8 = keys;
        int var6 = 0;

        int i;
        for(i = keys.length; var6 < i; ++var6) {
            String key = var8[var6];
            String[] var12 = bm.getItemByKey(key).getEditFormulas();
            int var10 = 0;

            for(int var11 = var12.length; var10 < var11; ++var10) {
                String formula = var12[var10];
                formulas.add(formula);
            }
        }

        bm.execFormulas((String[])formulas.toArray(new String[0]), bm.getRowCount() - size, bm.getRowCount());
        Object objVal = this.getBillCardPanel().getHeadItem("bisappend").getValueObject();
        boolean isAppend = objVal == null ? false : (new UFBoolean(objVal.toString())).booleanValue();
        if (isAppend) {
            for(i = bm.getRowCount() - 1; i >= bm.getRowCount() - size; --i) {
                ((ClientUI)this.getBillUI()).afterMultiSelect(i);
                ((ClientUI)this.getBillUI()).countNnum(i);
            }
        }

    }

    public void setCardData(String pk_project, String pk_mt_ctplan) throws BusinessException {
        try {
            this.onBoAdd((ButtonObject)null);
            ClientUI ui = (ClientUI)this.getBillUI();
            BillCardPanel cardPanel = ui.getBillCardPanel();
            AggregatedValueObject latestAggVO = this.getAggVO(pk_mt_ctplan);
            if (latestAggVO != null) {
                AggregatedValueObject newHeadAggVO = CommonUtil.cloneAggVOWithHead(latestAggVO);
                AggregatedValueObject newAggVO = CommonUtil.cloneAggVO(latestAggVO);
                CircularlyAccessibleValueObject headVO = newHeadAggVO.getParentVO();
                headVO.setAttributeValue("pk_mt_ctplan", (Object)null);
                headVO.setAttributeValue("pk_project", pk_project);
                headVO.setAttributeValue("pk_mt_normalplan", pk_mt_ctplan);
                headVO.setAttributeValue("bisappend", new UFBoolean(true));
                headVO.setAttributeValue("bisvalid", new UFBoolean(false));
                headVO.setAttributeValue("vbillno", (Object)null);
                headVO.setAttributeValue("iversion", (Object)null);
                headVO.setAttributeValue("vapproveid", (Object)null);
                headVO.setAttributeValue("dapprovedate", (Object)null);
                headVO.setAttributeValue("tapprovetime", (Object)null);
                headVO.setAttributeValue("vapprovenote", (Object)null);
                CircularlyAccessibleValueObject[] bodyvos = newAggVO.getChildrenVO();
                int bodyvosLen = bodyvos.length;

                for(int i = 0; i < bodyvosLen; ++i) {
                    SuperVO bodyvo = (SuperVO)bodyvos[i];
                    if (bodyvo != null) {
                        bodyvo.setAttributeValue("vlastbillid", bodyvo.getAttributeValue(bodyvo.getParentPKFieldName()));
                        bodyvo.setAttributeValue("vlastbillrowid", bodyvo.getAttributeValue(bodyvo.getPKFieldName()));
                        bodyvo.setAttributeValue("vlastbilltype", this.getUIController().getBillType());
                        bodyvo.setAttributeValue("vlastbillbts", bodyvo.getAttributeValue("ts"));
                        bodyvo.setAttributeValue("pk_mt_ctplan", (Object)null);
                        bodyvo.setAttributeValue("pk_mt_ctplan_b", (Object)null);
                        bodyvo.setAttributeValue("vsourcebillid", (Object)null);
                        bodyvo.setAttributeValue("vsourcebillrowid", (Object)null);
                        bodyvo.setAttributeValue("vsourcebilltype", (Object)null);
                        bodyvo.setAttributeValue("pk_mt_ctplan", (Object)null);
                        bodyvo.setAttributeValue("pk_mt_ctplan_b", (Object)null);
                        bodyvo.setAttributeValue("naccummonthnum", (Object)null);
                        bodyvo.setAttributeValue("naccumwishnum", (Object)null);
                        bodyvo.setStatus(0);
                    }
                }

                this.getBillUI().setCardUIData(newHeadAggVO);
                ui.setDefaultData();
                this.getChildTreeUI().generateChildTreeNoRefreshCache();
                HashMap voMap = new HashMap();
                voMap.put(this.getChildTreeUI().getDefaultTableCode(), (SuperVO[])newAggVO.getChildrenVO());
                this.getChildTreeUI().initTreeCacheData(voMap);
                cardPanel.getHeadItem("pk_project").setEnabled(false);
                cardPanel.getHeadItem("bisappend").setEnabled(false);
                cardPanel.getHeadItem("pk_mt_normalplan").setEnabled(false);
                ui.initNappendnum(true);
                ui.showCurrNodeData();
            }
        } catch (Exception var13) {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000005"));
        }
    }

    private AggregatedValueObject getAggVO(String parentId) throws BusinessException {
        if (parentId == null) {
            return null;
        } else {
            AggregatedValueObject aggVo = null;

            try {
                ClientBusinessDelegator bd = (ClientBusinessDelegator)this.getBusiDelegator();
                aggVo = bd.queryVOByPrimaryKey(this.getUIController().getBillVoName(), parentId);
                return aggVo;
            } catch (Exception var4) {
                throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000006"), var4);
            }
        }
    }

    public void onBillRef() throws Exception {
        super.onBillRef();
        ButtonObject btn = this.getButtonManager().getButton(9);
        this.getBillUI().setDefaultData();
        btn.setTag(String.valueOf(9));
    }

    private String getUIItemValueSelected(String fieldname) throws Exception {
        String val = "";
        AggregatedValueObject aggvo = this.getCurAggVO();
        if (aggvo != null && aggvo.getParentVO() != null && aggvo.getParentVO().getAttributeValue(fieldname) != null) {
            val = (String)aggvo.getParentVO().getAttributeValue(fieldname);
        }

        return val;
    }

    private String[] checkAndGetAddedData() throws BusinessException {
        String vbillstatus = "";
        boolean bisappend = false;
        boolean bisvalid = false;
        String pk_project = "";
        String pk_mt_ctplan = "";
        ClientUI ui = (ClientUI)this.getBillUI();

        try {
            if (ui.isListPanelSelected()) {
                BillListPanel listPanel = ui.getBillListPanel();
                int row = listPanel.getHeadTable().getSelectedRow();
                vbillstatus = listPanel.getHeadBillModel().getValueAt(row, "vbillstatus").toString();
                bisappend = (new UFBoolean(listPanel.getHeadBillModel().getValueAt(row, "bisappend").toString())).booleanValue();
                pk_project = listPanel.getHeadBillModel().getValueAt(row, "pk_project").toString();
                pk_mt_ctplan = listPanel.getHeadBillModel().getValueAt(row, "pk_mt_ctplan").toString();
                bisvalid = (new UFBoolean(listPanel.getHeadBillModel().getValueAt(row, "bisvalid").toString())).booleanValue();
            } else {
                BillCardPanel cardPanel = ui.getBillCardPanel();
                vbillstatus = cardPanel.getHeadItem("vbillstatus").getValueObject().toString();
                bisappend = (new UFBoolean(cardPanel.getHeadItem("bisappend").getValueObject().toString())).booleanValue();
                pk_project = cardPanel.getHeadItem("pk_project").getValueObject().toString();
                pk_mt_ctplan = cardPanel.getHeadItem("pk_mt_ctplan").getValueObject().toString();
                bisvalid = (new UFBoolean(cardPanel.getHeadItem("bisvalid").getValueObject().toString())).booleanValue();
            }
        } catch (Exception var9) {
            return null;
        }

        if (!vbillstatus.equals(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000007")) && !vbillstatus.equals("1")) {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000008"));
        } else if (bisappend) {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000009"));
        } else if (!bisvalid) {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000010"));
        } else {
            return new String[]{pk_project, pk_mt_ctplan};
        }
    }

    private void showNormPlanRefModel() throws BusinessException {
        String[] addedData = this.checkAndGetAddedData();
        if (addedData != null) {
            this.setCardData(addedData[0], addedData[1]);
        }
    }

    private void showLinkMtCtplanExe(int intBtn) throws BusinessException {
        try {
            String pk_mt_ctplan = this.getUIItemValueSelected("pk_mt_ctplan");
            HashMap userObjMap = new HashMap();
            userObjMap.put("pk_mt_ctplan", pk_mt_ctplan);
            String[] nunitrateFieldsArray = new String[]{"naccumwishnumrate", "naccummonthnumrate"};
            String[] numFieldsArray = new String[]{"nnum", "naccummonthnum", "naccumwishnum"};
            HashMap digitalFieldMap = new HashMap();
            if (digitalFieldMap != null) {
                digitalFieldMap.put("BD501", numFieldsArray);
                digitalFieldMap.put("BD503", nunitrateFieldsArray);
            }

            userObjMap.put("digitalFieldMap", digitalFieldMap);
            LinkDLg dlg = new LinkDLg(this.getBillUI(), intBtn, userObjMap);
            dlg.showModal();
        } catch (BusinessException var8) {
            throw var8;
        } catch (Exception var9) {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000011"));
        }
    }

    private void setNappendnumState() throws Exception {
        BillCardPanel billcardPanel = this.getBillCardPanelWrapper().getBillCardPanel();
        billcardPanel.getHeadItem("bisappend").setEnabled(false);
        billcardPanel.getHeadItem("pk_project").setEnabled(false);
        UFBoolean bisappend = new UFBoolean(false);
        ClientUI billui = (ClientUI)this.getBillUI();
        AggregatedValueObject aggvo = null;
        aggvo = this.getBillCardPanelWrapper().getBillVOFromUI();
        if (aggvo != null && aggvo.getParentVO() != null && aggvo.getParentVO().getAttributeValue("bisappend") != null) {
            bisappend = (UFBoolean)aggvo.getParentVO().getAttributeValue("bisappend");
        }

        billui.initNappendnum(bisappend.booleanValue());
    }

    protected BillListPanelWrapper getBillListPanelWrapper() {
        return this.getBillManageUI().getBillListWrapper();
    }

    private BillManageUI getBillManageUI() {
        return (BillManageUI)this.getBillUI();
    }

    private void showQueryDlg(int intBtn) throws Exception {
        String pk_project = this.getUIItemValueSelected("pk_project");
        String pk_corp = this.getUIItemValueSelected("pk_corp");
        if (pk_project != null && !pk_project.trim().equals("")) {
            ClientQueryDlg querydialog = new ClientQueryDlg(this.getBillUI(), pk_project, false);
            querydialog.showModal();
            if (querydialog.getOkAndCancel() == 1) {
                pk_project = querydialog.getPk_project() == null ? "" : querydialog.getPk_project();
                String pk_wbsdef = querydialog.getPk_wbsdef() == null ? "" : querydialog.getPk_wbsdef();
                String pk_invcl = querydialog.getPk_invcl() == null ? "" : querydialog.getPk_invcl();
                String pk_invmandoc = querydialog.getPk_invmandoc() == null ? "" : querydialog.getPk_invmandoc();
                String bisOverCost = querydialog.getBisOverCost() == null ? "" : querydialog.getBisOverCost();
                HashMap userObjMap = new HashMap();
                userObjMap.put("pk_project", pk_project);
                userObjMap.put("pk_corp", pk_corp);
                userObjMap.put("pk_wbsdef", pk_wbsdef);
                userObjMap.put("pk_invmandoc", pk_invmandoc);
                userObjMap.put("bisOverCost", bisOverCost);
                userObjMap.put("pk_invcl", pk_invcl);
                String[] numFieldsArray = new String[]{"ntotalnum", "nsplitnum", "ndownnum", "ntotalplannum", "nthisplannnum", "ndownplannum"};
                ClientUI ui = (ClientUI)this.getBillUI();
                HashMap digitalFieldMap = ui.getDigitalFieldMap();
                if (digitalFieldMap != null && digitalFieldMap.get("BD501") != null) {
                    digitalFieldMap.put("BD501", numFieldsArray);
                }

                userObjMap.put("digitalFieldMap", digitalFieldMap);
                LinkDLg dlg = new LinkDLg(this.getBillUI(), intBtn, userObjMap);
                dlg.showModal();
            }
        } else {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000012"));
        }
    }

    @SuppressWarnings("unchecked")
	protected void onBoCancelAudit() throws Exception {
        AggregatedValueObject aggVO = this.getCurAggVO();
        if (aggVO != null) {
        	 HashMap<String, UFDouble> invbasdoc_usedNumMap=null;//map--- pk_invbasdoc:usednum 已使用数量传递
             List<CmStocklistVO> oldCmStocklistVOs = null; //生成已使用数量的合同，表体数据
             
            AppendPlanCheck.checkIsBakBillForNormPlan(aggVO, "bisvalid", "bisappend");
            AggregatedValueObject modelVo = this.getBufferData().getCurrentVOClone();
            this.setCheckManAndDate(modelVo);
            if (this.checkVOStatus(modelVo, new int[]{8})) {
                System.out.println("无效的鼠标处理机制");
            } else {
            	IUAPQueryBS query = (IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class);
                this.beforeOnBoAction(27, modelVo);
                UFBoolean bisappend = ((MtCtplanVO)modelVo.getParentVO()).getBisappend();
                
                if (bisappend.equals(new UFBoolean(false))){//非追加计划，校验是否存在合同 
                	String pk_head = ((MtCtplanVO)modelVo.getParentVO()).getPk_mt_ctplan();
                	String countSQL="select count(1) count from jzpm_cm_stocklist where vlastbillid = '"+pk_head+"' and nvl(dr, 0) = 0";
                	HashMap<String,Integer> countMap = (HashMap<String, Integer>) query.executeQuery(countSQL, new MapProcessor());
                	if(countMap!=null){
                		Integer count= countMap.get("count");
                		if(count>0){
                			throw new BusinessException("存在下游合同，不允许弃审");
                		}
                	}
                }else{//追加计划，校验其数量是否已经被合同使用
                	//追加计划表体信息
                	String  jzpm_mt_ctplan_b_sql = "select pk_invbasdoc,nappendnum from  jzpm_mt_ctplan_b where pk_mt_ctplan= '"+modelVo.getParentVO().getPrimaryKey()+"' and nvl(dr, 0) = 0 ";
                    List<MtCtplanBVO> oldMtCtplanBVOs = (List<MtCtplanBVO>) query.executeQuery(jzpm_mt_ctplan_b_sql, new BeanListProcessor(MtCtplanBVO.class));
                	if(oldMtCtplanBVOs!=null && oldMtCtplanBVOs.size()>0){
                		HashMap<String, UFDouble> appendNumMap = new HashMap<String,UFDouble>();
                     	for (int i = 0; i < oldMtCtplanBVOs.size(); i++) {//本次追加数量
                     		appendNumMap.put(oldMtCtplanBVOs.get(i).getPk_invbasdoc(), oldMtCtplanBVOs.get(i).getNappendnum());
     					}
                     	//汇总表求差值
                     	String pk_head = ((MtCtplanVO)modelVo.getParentVO()).getPk_mt_normalplan();
                     	HashMap<String, UFDouble> haveNumMap = new HashMap<String,UFDouble>();
                     	String  jzpm_mt_ctplan_b_sum_sql = "select pk_invbasdoc,usednum,nnum from jzpm_mt_ctplan_b_sum where pk_mt_ctplan = '"+pk_head+"' and nvl(dr, 0) = 0 ";
                         List<MtCtplanBSumVO> oldMtCtplanBSumVOs = (List<MtCtplanBSumVO>) query.executeQuery(jzpm_mt_ctplan_b_sum_sql, new BeanListProcessor(MtCtplanBSumVO.class));
                         if(oldMtCtplanBSumVOs !=null && oldMtCtplanBSumVOs.size()>0) {
                         	for (MtCtplanBSumVO mtCtplanBSumVO : oldMtCtplanBSumVOs) {
                         		UFDouble usedNum = mtCtplanBSumVO.getUsednum();
                         		if(usedNum==null)usedNum=UFDouble.ZERO_DBL;
                         		haveNumMap.put(mtCtplanBSumVO.getPk_invbasdoc(), mtCtplanBSumVO.getNnum().sub(usedNum));
                         	}
                         	//比较校验
                         	Set<Entry<String, UFDouble>> ens =  appendNumMap.entrySet();
                         	for (Entry<String, UFDouble> entry : ens) {
                         		UFDouble planNum = haveNumMap.get(entry.getKey());
                         		if(planNum.compareTo(entry.getValue())<0){
                         			throw new BusinessException("该追加计划已经被下游合同引用，不允许弃审");
                         		}
                         	}
                         }
                	}
                }
                //执行弃审
                AggregatedValueObject retVo = this.getBusinessAction().unapprove(modelVo, this.getUIController().getBillType(), this.getBillUI()._getDate().toString(), this.getBillUI().getUserObject());
                
                if (retVo.getParentVO() != null) {
                    modelVo.getParentVO().setAttributeValue("ts", retVo.getParentVO().getAttributeValue("ts"));
                    modelVo.getParentVO().setAttributeValue(this.getBillManageUI().getBillField().getField_BillStatus(), retVo.getParentVO().getAttributeValue(this.getBillManageUI().getBillField().getField_BillStatus()));
                    modelVo.getParentVO().setAttributeValue(this.getBillManageUI().getBillField().getField_CheckNote(), retVo.getParentVO().getAttributeValue(this.getBillManageUI().getBillField().getField_CheckNote()));
                }

                if (PfUtilClient.isSuccess()) {
                    this.afterOnBoAction(27, modelVo);
                    Integer intState = (Integer)modelVo.getParentVO().getAttributeValue(this.getBillField().getField_BillStatus());
                    if (intState == 8) {
                        modelVo.getParentVO().setAttributeValue(this.getBillField().getField_CheckMan(), (Object)null);
                        modelVo.getParentVO().setAttributeValue(this.getBillField().getField_CheckDate(), (Object)null);
                    }

                    this.updateHeadFixFields(modelVo, retVo, this.getFixFields(27, retVo.getParentVO()));
                    String pk_head = retVo.getParentVO().getPrimaryKey();//1012AA1000000009RUQB
                    
                    
                    if (bisappend.equals(new UFBoolean(true))) {
                        pk_head = ((MtCtplanVO)modelVo.getParentVO()).getPk_mt_normalplan();
                        String  jzpm_mt_ctplan_b_sum_sql = "select pk_invbasdoc,usednum,pk_mt_ctplan_b_sum from jzpm_mt_ctplan_b_sum where pk_mt_ctplan = '"+pk_head+"' and nvl(dr, 0) = 0 ";
                        List<MtCtplanBSumVO> oldMtCtplanBSumVOs = (List<MtCtplanBSumVO>) query.executeQuery(jzpm_mt_ctplan_b_sum_sql, new BeanListProcessor(MtCtplanBSumVO.class));
                        StringBuilder stocklistSql = new StringBuilder("select * from jzpm_cm_stocklist where 1=1 ");
                        if (oldMtCtplanBSumVOs != null && oldMtCtplanBSumVOs.size() > 0) {
                        	invbasdoc_usedNumMap = new HashMap<String, UFDouble>();
                        	for (int i = 0; i < oldMtCtplanBSumVOs.size(); i++) {
                        		MtCtplanBSumVO mtCtplanBSumVO = oldMtCtplanBSumVOs.get(i);
                        		invbasdoc_usedNumMap.put(mtCtplanBSumVO.getPk_invbasdoc(), mtCtplanBSumVO.getUsednum());
                        		if(i==0){//SQL语句拼接
                        			stocklistSql.append("and vlastbillrowid in ('"+mtCtplanBSumVO.getPk_mt_ctplan_b_sum()+"'");
                        		}else{
                        			stocklistSql.append(",'"+mtCtplanBSumVO.getPk_mt_ctplan_b_sum()+"'");
                        		}
                        		if(i==oldMtCtplanBSumVOs.size()-1){
                        			stocklistSql.append(") and nvl(dr, 0) = 0 ");
                        		}
        					}
                        	oldCmStocklistVOs = (List<CmStocklistVO>) query.executeQuery(stocklistSql.toString(), new BeanListProcessor(CmStocklistVO.class));
                        }
                        
                        ((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).deleteByClause(MtCtplanBSumVO.class, " pk_mt_ctplan='" + pk_head + "' ");
                        StringBuffer sb = new StringBuffer();
                        sb.append("select b.bissupplied,b.dr,");
                        sb.append("sum(b.naccumappendnum) as naccumappendnum, ");
                        sb.append("sum(b.naccummonthnum) as naccummonthnum, ");
                        sb.append("sum(b.naccumwishnum) as naccumwishnum, ");
                        sb.append("sum(b.nappendnum) as nappendnum, ");
                        sb.append("sum(b.ndifferratio) as ndifferratio, ");
                        sb.append("sum(b.nnum) as nnum, ");
                        sb.append("sum(b.nmoney) as nmoney, ");
                        sb.append("b.pk_invbasdoc, ");
                        sb.append("b.price, ");
                        sb.append("b.pk_invmandoc, ");
                        sb.append("b.pk_mt_ctplan  ");
                        sb.append("from jzpm_mt_ctplan_b b ");
                        sb.append("where nvl(dr, 0) = 0 ");
                        sb.append("   and b.pk_mt_ctplan = '" + pk_head + "'");
                        sb.append(" group by b.pk_invbasdoc, b.pk_invmandoc, b.pk_mt_ctplan, b.bissupplied,b.dr,b.price ");
                        List<MtCtplanBSumVO> newMtCtplanBSumVOs = (List)query.executeQuery(sb.toString(), new BeanListProcessor(MtCtplanBSumVO.class));
                        
                        if (newMtCtplanBSumVOs != null && newMtCtplanBSumVOs.size() > 0) {
                        	if(invbasdoc_usedNumMap!=null){//已使用数量赋值
                        		for (MtCtplanBSumVO mtCtplanBSumVO : newMtCtplanBSumVOs) {
        							mtCtplanBSumVO.setUsednum(invbasdoc_usedNumMap.get(mtCtplanBSumVO.getPk_invbasdoc()));
        						}
                        	}
                            ((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).insertVOList(newMtCtplanBSumVOs);
                            
                            if(oldCmStocklistVOs!=null && oldCmStocklistVOs.size()>0){
                            	HashMap<String,String> invbasdocSumpkMap = new HashMap();
                                String  jzpm_mt_ctplan_b_sum_sql1 = "select pk_invbasdoc,usednum,pk_mt_ctplan_b_sum from jzpm_mt_ctplan_b_sum where pk_mt_ctplan = '"+pk_head+"' and nvl(dr, 0) = 0 ";
                                List<MtCtplanBSumVO> savedMtCtplanBSumVOs = (List<MtCtplanBSumVO>) query.executeQuery(jzpm_mt_ctplan_b_sum_sql1, new BeanListProcessor(MtCtplanBSumVO.class));
                                if(savedMtCtplanBSumVOs!=null && savedMtCtplanBSumVOs.size()>0){
                                	for (MtCtplanBSumVO mtCtplanBSumVO : savedMtCtplanBSumVOs) {
                                		invbasdocSumpkMap.put(mtCtplanBSumVO.getPk_invbasdoc(), mtCtplanBSumVO.getPk_mt_ctplan_b_sum());
            						}
                                }
                    			for (int i = 0; i < oldCmStocklistVOs.size(); i++) {
                    				CmStocklistVO  vo= oldCmStocklistVOs.get(i);
                    				vo.setVlastbillrowid(invbasdocSumpkMap.get(vo.getPk_invbasdoc()));
                    				vo.setTs(new UFDateTime());
        						}
                    			((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).updateVOList(oldCmStocklistVOs);
                    			 String  contractSQL = "select * from jzpm_cm_contract where pk_contract='"+oldCmStocklistVOs.get(0).getPk_contract()+"'";
                    			 List<CmContractVO> ctVos = (List<CmContractVO>) query.executeQuery(contractSQL, new BeanListProcessor(CmContractVO.class));
                    			 if(ctVos!=null && ctVos.size()>0){
                    				 ctVos.get(0).setTs(oldCmStocklistVOs.get(oldCmStocklistVOs.size()-1).getTs());
                    				 ((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).updateVOList(ctVos);
                    			 }
                    		}
                        }
                    } else {
                        String sqlwhere = "pk_mt_ctplan='" + pk_head + "'";
                        ((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).deleteByClause(MtCtplanBSumVO.class, sqlwhere);
                    }
                }
            }
        }
    }

    private UFBoolean getUIBisappendSelected() throws Exception {
        UFBoolean bisappend = new UFBoolean(false);
        AggregatedValueObject aggvo = this.getCurAggVO();
        if (aggvo != null && aggvo.getParentVO() != null && aggvo.getParentVO().getAttributeValue("bisappend") != null) {
            bisappend = new UFBoolean(aggvo.getParentVO().getAttributeValue("bisappend").toString());
        }

        return bisappend;
    }

    private AggregatedValueObject getCurAggVO() throws Exception {
        ClientUI ui = (ClientUI)this.getBillUI();
        AggregatedValueObject aggvo = null;
        if (ui.isListPanelSelected()) {
            if (this.getBillListPanelWrapper().getBillListPanel().getHeadTable().getSelectedRow() != -1) {
                aggvo = this.getBillListPanelWrapper().getVOFromUI();
            }
        } else {
            aggvo = this.getBillCardPanelWrapper().getBillVOFromUI();
        }

        return aggvo;
    }

    protected void setItemState() throws Exception {
        super.setItemState();
        BillCardPanel cardpanel = this.getBillCardPanelWrapper().getBillCardPanel();
        cardpanel.getHeadItem("bisappend").setEnabled(false);
    }

    public void subTotal(int intBtn) {
        try {
            this.removeSubTotalFlag();
        } catch (Exception var5) {
            Logger.error(var5.getMessage(), var5);
        }

        if (intBtn == 774) {
            BillLevelSubTotalAction levelSubTotalAction = null;
            if (!this.getBillManageUI().isListPanelSelected()) {
                levelSubTotalAction = new BillLevelSubTotalAction(this.getBillManageUI(), this.getBillManageUI().getBillCardPanel(), MtCtplanBVO.class.getName());
            }

            try {
                levelSubTotalAction.execute("jzpm_mt_ctplan_b2");
            } catch (Exception var4) {
                Logger.error(var4.getMessage(), var4);
            }
        }

    }

    protected int getCurTabIndex() {
        return !this.getUI().isListPanelSelected() ? this.getUI().getBillCardPanel().getBodyTabbedPane().getSelectedIndex() : this.getUI().getBillListPanel().getBodyTabbedPane().getSelectedIndex();
    }

    private ClientUI getUI() {
        return (ClientUI)this.getBillUI();
    }

    protected void onBillQuery() {
        super.onBillQuery();
        this.setSubTotalEnabled();
    }

    private void setSubTotalEnabled() {
        ButtonObject subTotalBtn = this.getButtonManager().getButton(773);
        ButtonObject levelSubTotalBtn = this.getButtonManager().getButton(774);
        int tableIndex = this.getCurTabIndex();
        if (tableIndex == 1) {
            subTotalBtn.setEnabled(true);
            levelSubTotalBtn.setEnabled(true);
        } else {
            subTotalBtn.setEnabled(false);
            levelSubTotalBtn.setEnabled(false);
        }

        this.getUI().updateButtons();
    }

    public void setButtonList(List<Integer> buttons) {
        this.buttonList = buttons;
    }

    public List<Integer> getButtonList() {
        return this.buttonList;
    }

    protected UIDialog createQueryUI() {
        TemplateInfo tempinfo = new TemplateInfo();
        tempinfo.setPk_Org(this._getCorp().getPrimaryKey());
        tempinfo.setCurrentCorpPk(this._getCorp().getPrimaryKey());
        tempinfo.setFunNode(this.getBillUI()._getModuleCode());
        tempinfo.setUserid(this._getOperator());
        tempinfo.setBusiType(this.getBillUI().getBusinessType());
        tempinfo.setNodekey(this.getBillUI().getNodeKey());
        return new MultiCorpDefaultQueryDlg(this.getBillUI(), tempinfo);
    }

    private void initButtonList() {
        if (this.getButtonList() == null) {
            List tempbtnList = new ArrayList();
            tempbtnList.add(1);
            tempbtnList.add(53);
            tempbtnList.add(20);
            tempbtnList.add(30);
            tempbtnList.add(19);
            tempbtnList.add(21);
            tempbtnList.add(24);
            tempbtnList.add(22);
            tempbtnList.add(23);
            tempbtnList.add(6);
            tempbtnList.add(5);
            tempbtnList.add(8);
            tempbtnList.add(31);
            tempbtnList.add(9);
            tempbtnList.add(16);
            tempbtnList.add(511);
            tempbtnList.add(773);
            this.setButtonList(tempbtnList);
        }

    }

    public void onButton(ButtonObject bo) {
        try {
            ButtonObject parentBtn = bo.getParent();
            if (this.buttonList != null) {
                if (parentBtn != null) {
                    if (!this.buttonList.contains(Integer.parseInt(parentBtn.getTag()))) {
                        this.checkIsNCurrentCorp();
                    }
                } else if (!this.buttonList.contains(Integer.parseInt(bo.getTag()))) {
                    this.checkIsNCurrentCorp();
                }
            }

            super.onButton(bo);
        } catch (Exception var3) {
            this.getBillUI().showErrorMessage(var3.getMessage());
        }

    }

    private void checkIsNCurrentCorp() throws BusinessException {
        if (this.getBillUI().getFrame().getLinkType() != 1) {
            String pk_corp = "";
            BillModel billModel = this.getBillListPanelWrapper().getBillListPanel().getHeadBillModel();
            if (((BillManageUI)this.getBillUI()).isListPanelSelected()) {
                int[] selectRows = this.getBillListPanelWrapper().getBillListPanel().getHeadTable().getSelectedRows();
                if (selectRows == null || selectRows.length <= 0) {
                    return;
                }

                if (selectRows.length > 1) {
                    throw new BusinessException("请选择一条数据操作!");
                }

                pk_corp = billModel.getValueAt(selectRows[0], "pk_corp") == null ? "" : billModel.getValueAt(selectRows[0], "pk_corp").toString();
            } else {
                pk_corp = this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_corp").getValueObject() == null ? "" : this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_corp").getValueObject().toString();
            }

            if (!pk_corp.equals(this.getBillUI().getEnvironment().getCorporation().getPk_corp())) {
                throw new BusinessException("非当前公司数据，不能进行操作!");
            }
        }
    }

    protected void onBoCard() throws Exception {
        this.chgRef(true);
        super.onBoCard();
    }

    public void onBoAdd(ButtonObject bo) throws Exception {
        this.chgRef(false);
        super.onBoAdd(bo);
    }

    private void chgRef(boolean flag) {
        UIRefPane ddPane = (UIRefPane)this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_project").getComponent();
        if (flag) {
            ddPane.setRefModel(new JZMultiCorpAllObjProjRefTreeModel());
        } else {
            ddPane.setRefModel(new NoFinishObjProjRefTreeModel());
        }

    }

    protected boolean askForQueryCondition(StringBuffer sqlWhereBuf) throws Exception {
        if (sqlWhereBuf == null) {
            throw new IllegalArgumentException("askForQueryCondition().sqlWhereBuf cann't be null");
        } else {
            UIDialog querydialog = this.getQueryUI();
            if (querydialog.showModal() != 1) {
                return false;
            } else {
                INormalQuery query = (INormalQuery)querydialog;
                String strWhere = query.getWhereSql();
                if (strWhere == null || strWhere.trim().length() == 0) {
                    strWhere = "1=1";
                }

                if (this.getButtonManager().getButton(2) != null) {
                    if (this.getBillIsUseBusiCode().booleanValue()) {
                        strWhere = "(" + strWhere + ") and " + this.getBillField().getField_BusiCode() + "='" + this.getBillUI().getBusicode() + "'";
                    } else {
                        strWhere = "(" + strWhere + ") and " + this.getBillField().getField_Busitype() + "='" + this.getBillUI().getBusinessType() + "'";
                    }
                }

                strWhere = "(" + strWhere + ") and (isnull(dr,0)=0)";
                sqlWhereBuf.append(strWhere);
                return true;
            }
        }
    }
}
