//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.mt100505;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import javax.swing.event.ChangeEvent;
import nc.bs.framework.common.NCLocator;
import nc.bs.logging.Logger;
import nc.itf.uap.IUAPQueryBS;
import nc.jdbc.framework.processor.BeanListProcessor;
import nc.ui.jz.pub.buttonstate.BillLinkBtnVO;
import nc.ui.jz.pub.buttonstate.JzCommonButtonVO;
import nc.ui.jzpm.core.buttonstate.AppendPlanBtnVO;
import nc.ui.jzpm.core.buttonstate.ButtonFactory;
import nc.ui.jzpm.core.buttonstate.LinkMtCtplanExeBtnVO;
import nc.ui.jzpm.core.pub.ref.MtNormalPlanRefModel;
import nc.ui.jzpm.mtpub.turnmaterial.JZPMTMProxy;
import nc.ui.pub.ButtonObject;
import nc.ui.pub.beans.UIRefPane;
import nc.ui.pub.bill.BillEditEvent;
import nc.ui.pub.bill.BillItemEvent;
import nc.ui.pub.bill.BillModel;
import nc.ui.trade.bill.AbstractManageController;
import nc.ui.trade.bsdelegate.BusinessDelegator;
import nc.ui.trade.button.ButtonVOFactory;
import nc.ui.trade.manage.ManageEventHandler;
import nc.ui.trade.pub.TableTreeNode;
import nc.ui.trade.pub.VOTreeNode;
import nc.vo.jz.pub.util.CommonTool;
import nc.vo.jzpm.mt100505.AggVO;
import nc.vo.jzpm.mt100505.MtCtplanBSumVO;
import nc.vo.jzpm.mt100505.MtCtplanBVO;
import nc.vo.jzpm.mt100505.MtCtplanVO;
import nc.vo.jzpm.mtpub.AggregatedVO;
import nc.vo.jzpm.pub.JZPMProxy;
import nc.vo.jzpm.pub.mtpub.MTServiceProxy;
import nc.vo.ml.NCLangRes4VoTransl;
import nc.vo.pub.AggregatedValueObject;
import nc.vo.pub.BusinessException;
import nc.vo.pub.CircularlyAccessibleValueObject;
import nc.vo.pub.SuperVO;
import nc.vo.pub.lang.UFBoolean;
import nc.vo.pub.lang.UFDouble;
import nc.vo.trade.button.ButtonVO;

public class ClientUI extends nc.ui.jzpm.mtpub.consumetplan.ClientUI {
    private static final long serialVersionUID = 1L;
    private static final String m_costsplit = "jzpm_mt_ctplan_b";
    private static final String m_split_down = "jzpm_mt_ctplan_b2";
    public static final int ENABLE = 1;
    public static final int DISABLE = 2;
    public static final int APPENABLE = 3;
    private String[] numFieldsArray = new String[]{"nsrcnum", "nnum", "nappendnum", "naccumappendnum", "naccummonthnum", "naccumwishnum"};
    private String[] nunitrateFieldsArray = new String[]{"ndifferratio"};
    private String[] noEditBodyItems = new String[]{"invcode", "nappendnum", "nnum", "ndifferratio", "bissupplied", "vmemo"};
    private HashMap<String, String[]> digFieldMap = null;
    public Map<String, UFDouble> priceMap = new HashMap();

    public ClientUI(String pk_corp, String pk_billType, String pk_busitype, String operater, String billId) {
        super(pk_corp, pk_billType, pk_busitype, operater, billId);
        this.init();
    }

    public ClientUI() {
        this.init();
    }

    public void init() {
        super.init();
        this.getBillCardPanel().setBillBeforeEditListenerHeadTail(this);
        this.initNormalplanRef();
    }

    protected BusinessDelegator createBusinessDelegator() {
        return new ClientBusinessDelegator();
    }

    protected void initNormalplanRef() {
        UIRefPane refPane = null;
        refPane = (UIRefPane)this.getBillCardPanel().getHeadItem("pk_mt_normalplan").getComponent();
        MtNormalPlanRefModel refModel = (MtNormalPlanRefModel)refPane.getRefModel();
        refModel.setBillType(this.getUIControl().getBillType());
    }

    protected AbstractManageController createController() {
        return new ClientControl();
    }

    public void setDefaultData() throws Exception {
        super.setDefaultData();
        this.initNappendnum(false);
    }

    protected ManageEventHandler createEventHandler() {
        return new ClientEventHandler(this, this.getUIControl());
    }

    public String[] getTableCodes() {
        AggVO aggvo = new AggVO();
        return aggvo.getTableCodes();
    }

    protected void afterEditHead(BillEditEvent e) throws BusinessException {
        try {
            super.afterEditHead(e);
            String key = e.getKey();
            if (key.equals("bisappend")) {
                this.afterEditProjAndIsAppend(e);
                this.afterEditBisappend(e);
            } else if (key.equals("pk_project")) {
                this.afterEditProj();
            }

        } catch (BusinessException var3) {
            throw var3;
        } catch (Exception var4) {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000030"), var4);
        }
    }

    protected void afterEditProjAndIsAppend(BillEditEvent e) throws Exception {
        this.getBillCardWrapper().getBillCardPanel().getBillModel().clearBodyData();
        if (this.getTreeCache() != null && this.getTreeCache().getTreeHash() != null) {
            this.getTreeCache().getTreeHash().clear();
        }
    }

    private void afterEditProj() throws BusinessException {
        String pk_project = (String)this.getBillCardPanel().getHeadItem("pk_project").getValueObject();
        if (pk_project != null && !pk_project.trim().equals("")) {
            String pk_corp = this._getCorp().getPrimaryKey();
            String pk_name = this.getUIControl().getPkField();
            String pk_mt_normalplan = MTServiceProxy.getMTStatelessService().getMTNormalPlanPk(MtCtplanVO.class, pk_project, pk_corp, pk_name);
            if (pk_mt_normalplan != null && !"".equals(pk_mt_normalplan)) {
                this.getBillCardPanel().setHeadItem("bisappend", new UFBoolean(true));
                this.getBillCardPanel().getHeadItem("bisappend").setEnabled(false);
                this.getBillCardPanel().getHeadItem("pk_mt_normalplan").setValue(pk_mt_normalplan);
                this.initNappendnum(true);
            } else {
                this.getBillCardPanel().setHeadItem("bisappend", new UFBoolean(false));
                this.getBillCardPanel().getHeadItem("bisappend").setEnabled(true);
                this.getBillCardPanel().getHeadItem("pk_mt_normalplan").setValue((Object)null);
                this.initNappendnum(false);
            }

            String sqlwhere = "select distinct  t.pk_invmandoc as pk_invmandoc,t.price as price from jzpm_mt_ctplan_b t where t.pk_mt_ctplan='" + pk_mt_normalplan + "' or t.pk_mt_ctplan in (select m.pk_mt_ctplan from jzpm_mt_ctplan m where nvl(m.dr,0)=0 and m.pk_mt_normalplan='" + pk_mt_normalplan + "')";
            ArrayList<MtCtplanBVO> list = (ArrayList)((IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class)).executeQuery(sqlwhere, new BeanListProcessor(MtCtplanBVO.class));
            if (list != null && list.size() > 0) {
                Iterator var8 = list.iterator();

                while(var8.hasNext()) {
                    MtCtplanBVO vo = (MtCtplanBVO)var8.next();
                    String pk_invmandoc = vo.getPk_invmandoc();
                    UFDouble price = vo.getPrice();
                    if (!this.priceMap.containsKey(pk_invmandoc)) {
                        this.priceMap.put(pk_invmandoc, price);
                    }
                }
            }
        } else {
            this.getBillCardPanel().setHeadItem("bisappend", new UFBoolean(false));
            this.getBillCardPanel().getHeadItem("pk_mt_normalplan").setValue((Object)null);
            this.initNappendnum(false);
        }

    }

    protected void afterEditBody(BillEditEvent e) throws BusinessException {
        try {
            super.afterEditBody(e);
            String key = e.getKey();
            String pk_invmandoc;
            if (key.equals("price")) {
                pk_invmandoc = this.getBillCardPanel().getBodyValueAt(e.getRow(), "pk_invmandoc").toString();
                this.priceMap.put(pk_invmandoc, new UFDouble(e.getValue().toString()));
            }

            if (key.equals("nnum")) {
                pk_invmandoc = this.getBillCardPanel().getBodyValueAt(e.getRow(), "pk_invmandoc").toString();
                if (this.priceMap.containsKey(pk_invmandoc)) {
                    this.getBillCardPanel().setBodyValueAt(this.priceMap.get(pk_invmandoc), e.getRow(), "price");
                    this.getBillCardPanel().setCellEditable(e.getRow(), "price", false);
                    Object nnum = e.getValue();
                    if (nnum != null) {
                        this.getBillCardPanel().setBodyValueAt(((UFDouble)this.priceMap.get(pk_invmandoc)).multiply(new UFDouble(nnum.toString())), e.getRow(), "nmoney");
                    }
                }
            }

            int row = e.getRow();
            if (key.equals("pk_wbsdef")) {
                this.showNormData(row);
            } else if (key.equals("nappendnum")) {
                this.countNnum(row);
                String pk_invmandoc1 = this.getBillCardPanel().getBodyValueAt(e.getRow(), "pk_invmandoc").toString();
                if (this.priceMap.containsKey(pk_invmandoc1)) {
                    this.getBillCardPanel().setBodyValueAt(this.priceMap.get(pk_invmandoc1), e.getRow(), "price");
                    this.getBillCardPanel().setCellEditable(e.getRow(), "price", false);
                    Object nnum = e.getValue();
                    if (nnum != null) {
                        this.getBillCardPanel().setBodyValueAt(((UFDouble)this.priceMap.get(pk_invmandoc1)).multiply(new UFDouble(nnum.toString())), e.getRow(), "nmoney");
                    }
                }
            } else if (e.getKey().equals("vadmixturesname")) {
                UIRefPane admixturePane = (UIRefPane)this.getBillCardPanel().getBodyItem("vadmixturesname").getComponent();
                String value = admixturePane.getRefModel().getPkValue();
                String[] values = admixturePane.getRefModel().getPkValues();
                String dispValue = (String)admixturePane.getRefModel().getValue("vname");
                Object[] dispValues = admixturePane.getRefModel().getValues("vname");
                StringBuffer sbValue = new StringBuffer();
                StringBuffer sbDispValue = new StringBuffer();
                Object val;
                if (values != null && values.length > 0) {
                    String[] var14 = values;
                    int var12 = 0;

                    int var13;
                    for(var13 = values.length; var12 < var13; ++var12) {
                        String val1 = var14[var12];
                        sbValue.append(",").append(val1);
                    }

                    Object[] var22 = dispValues;
                    var12 = 0;

                    for(var13 = dispValues.length; var12 < var13; ++var12) {
                        val = var22[var12];
                        sbDispValue.append(",").append(val == null ? "" : val.toString());
                    }
                } else {
                    sbValue.append(",").append(value == null ? "" : value.toString());
                    sbDispValue.append(",").append(dispValue == null ? "" : dispValue.toString());
                }

                this.getBillCardPanel().setBodyValueAt(sbValue.substring(1), e.getRow(), "pk_admixtures");
                this.getBillCardPanel().setBodyValueAt(sbDispValue.substring(1), e.getRow(), "vadmixturesname");
                val = e.getValue() == null ? "" : e.getValue();
                Object objOldVal = e.getOldValue() == null ? "" : e.getOldValue();
                if (!val.equals(objOldVal) && this.getBillCardPanel().getBillModel().getRowState(e.getRow()) == 0) {
                    this.getBillCardPanel().getBillModel().setRowState(e.getRow(), 2);
                }
            }

        } catch (Exception var15) {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000031"), var15);
        }
    }

    protected void initPrivateButton() {
        super.initPrivateButton();
        ButtonVO btnRefVo = ButtonVOFactory.getInstance().build(9);
        btnRefVo.setBtnName(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000032"));
        this.addPrivateButton(btnRefVo);
        this.initSubTotalBtn();
        this.initImportBtn();
    }

    private void initImportBtn() {
        ButtonVO excelBtn = ButtonFactory.createButtonVO(726, "粘贴式导入", new int[]{0, 1, 3}, (int[])null, "粘贴式导入");
        this.addPrivateButton(excelBtn);
    }

    protected ButtonVO[] initAssFuncBtnVOs() {
        AppendPlanBtnVO apbtn = new AppendPlanBtnVO();
        apbtn.getButtonVO().setOperateStatus(new int[]{2});
        apbtn.getButtonVO().setExtendStatus(new int[]{3});
        return new ButtonVO[]{apbtn.getButtonVO()};
    }

    @SuppressWarnings("restriction")
	protected ButtonVO[] initAssQryBtnVOs() {
        LinkMtCtplanExeBtnVO linkCtplanBtnVO = new LinkMtCtplanExeBtnVO();
        linkCtplanBtnVO.getButtonVO().setExtendStatus(new int[]{1, 3});
        BillLinkBtnVO billLinkBtnVo = new BillLinkBtnVO();
        return new ButtonVO[]{linkCtplanBtnVO.getButtonVO(), billLinkBtnVo.getButtonVO()};
    }

    protected int getExtendStatus(AggregatedValueObject vo) {
        int status = super.getExtendStatus(vo);
        if (vo == null) {
            return status;
        } else {
            String bisappend = vo.getParentVO().getAttributeValue("bisappend") == null ? "" : vo.getParentVO().getAttributeValue("bisappend").toString();
            String bisvalid = vo.getParentVO().getAttributeValue("bisvalid") == null ? "" : vo.getParentVO().getAttributeValue("bisvalid").toString();
            int vbillstatus = vo.getParentVO().getAttributeValue("vbillstatus") == null ? -1 : Integer.parseInt(vo.getParentVO().getAttributeValue("vbillstatus").toString());
            byte status1;
            if ("N".equals(bisappend) && "Y".equals(bisvalid)) {
                status1 = 1;
                if (1 == vbillstatus) {
                    status1 = 3;
                }
            } else {
                status1 = 2;
            }

            return status1;
        }
    }

    public boolean beforeEdit(BillItemEvent e) {
        String key = e.getItem().getKey();
        if (key.equals("nnum") || key.equals("nappendnum")) {
            UFBoolean bisappend = new UFBoolean(this.getBillCardPanel().getHeadItem("bisappend").getValueObject().toString());
            if (bisappend.booleanValue()) {
                this.getBillCardPanel().getBodyItem("nnum").setEnabled(false);
                this.getBillCardPanel().getBodyItem("nappendnum").setEnabled(true);
            } else {
                this.getBillCardPanel().getBodyItem("nnum").setEnabled(true);
                this.getBillCardPanel().getBodyItem("nappendnum").setEnabled(false);
            }
        }

        return super.beforeEdit(e);
    }

    public boolean beforeEdit(BillEditEvent e) {
        if (e.getKey().equals("vadmixturesname")) {
            UIRefPane admixturePane = (UIRefPane)this.getBillCardPanel().getBodyItem("vadmixturesname").getComponent();
            admixturePane.setPK((String)null);
        }

        if (e.getKey().equals("price")) {
            String pk_invmandoc = this.getBillCardPanel().getBodyValueAt(e.getRow(), "pk_invmandoc").toString();
            if (this.priceMap.containsKey(pk_invmandoc)) {
                this.getBillCardPanel().setBodyValueAt(this.priceMap.get(pk_invmandoc), e.getRow(), "price");
                this.getBillCardPanel().setCellEditable(e.getRow(), "price", false);
                Object nnum = this.getBillCardPanel().getBodyValueAt(e.getRow(), "nnum");
                if (nnum != null) {
                    this.getBillCardPanel().setBodyValueAt(((UFDouble)this.priceMap.get(pk_invmandoc)).multiply(new UFDouble(nnum.toString())), e.getRow(), "nmoney");
                }
            }
        }

        return super.beforeEdit(e);
    }

    public String getRefBillType() {
        return "99V2";
    }

    public SuperVO[] getSignedTabbedVO(String pk_tree, String tableCode) throws BusinessException {
        AggregatedValueObject aggvo = this.getBufferData().getCurrentVO();
        Object projectObj = null;
        if (aggvo != null) {
            projectObj = aggvo.getParentVO().getAttributeValue("pk_project");
        }

        if (projectObj == null) {
            return null;
        } else {
            String headPK = aggvo.getParentVO().getPrimaryKey();
            if (tableCode.equalsIgnoreCase("jzpm_mt_ctplan_b")) {
                return this.querySplitDataFromDB(headPK, pk_tree);
            } else {
                return tableCode.equalsIgnoreCase("jzpm_mt_ctplan_b2") ? this.generateSplitDownDataFromDB(headPK) : null;
            }
        }
    }

    protected SuperVO[] querySplitDataFromDB(String headPK, String pk_tree) throws BusinessException {
        String condtionStr = " pk_mt_ctplan = '" + headPK + "' and pk_wbsdef = '" + pk_tree + "' and isnull(dr,0)=0 ";
        MtCtplanBVO[] bodyvo = (MtCtplanBVO[])JZPMProxy.getIJZPMPubBusi().queryByClause(MtCtplanBVO.class, condtionStr, "pk_mt_ctplan_b");
        return bodyvo;
    }

    protected SuperVO[] generateSplitDownDataFromDB(String headPK) throws BusinessException {
        VOTreeNode curNode = this.getBillTreeSelectNode();
        ArrayList al_treePKs = this.getAllPKs(curNode);
        String[] wbsdefPKs = CommonTool.chkNull(al_treePKs) ? null : (String[])al_treePKs.toArray(new String[0]);
        MtCtplanBVO[] bodyvo = (new JZPMTMProxy()).getMTStatefulService().getMtCtplanBVOByWBS(headPK, wbsdefPKs);
        if (wbsdefPKs != null && wbsdefPKs.length > 1 && bodyvo != null && bodyvo.length > 0) {
            Map<String, MtCtplanBVO> map = new HashMap();
            MtCtplanBVO[] var10 = bodyvo;
            int var8 = 0;

            for(int var9 = bodyvo.length; var8 < var9; ++var8) {
                MtCtplanBVO vo = var10[var8];
                String key = vo.getPk_invbasdoc().concat(vo.getPk_invmandoc());
                if (map.containsKey(key)) {
                    MtCtplanBVO tempvo = (MtCtplanBVO)map.get(key);
                    tempvo.setNdifferratio((tempvo.getNdifferratio() == null ? new UFDouble(0) : tempvo.getNdifferratio()).add(vo.getNdifferratio() == null ? new UFDouble(0) : vo.getNdifferratio()));
                    tempvo.setNnum((tempvo.getNnum() == null ? new UFDouble(0) : tempvo.getNnum()).add(vo.getNnum() == null ? new UFDouble(0) : vo.getNnum()));
                    tempvo.setNaccumappendnum((tempvo.getNaccumappendnum() == null ? new UFDouble(0) : tempvo.getNaccumappendnum()).add(vo.getNaccumappendnum() == null ? new UFDouble(0) : vo.getNaccumappendnum()));
                    tempvo.setNappendnum((tempvo.getNappendnum() == null ? new UFDouble(0) : tempvo.getNappendnum()).add(vo.getNappendnum() == null ? new UFDouble(0) : vo.getNappendnum()));
                    tempvo.setNaccummonthnum((tempvo.getNaccummonthnum() == null ? new UFDouble(0) : tempvo.getNaccummonthnum()).add(vo.getNaccummonthnum() == null ? new UFDouble(0) : vo.getNaccummonthnum()));
                    tempvo.setNaccumwishnum((tempvo.getNaccumwishnum() == null ? new UFDouble(0) : tempvo.getNaccumwishnum()).add(vo.getNaccumwishnum() == null ? new UFDouble(0) : vo.getNaccumwishnum()));
                    tempvo.setNmoney((tempvo.getNmoney() == null ? new UFDouble(0) : tempvo.getNmoney()).add(vo.getNmoney() == null ? new UFDouble(0) : vo.getNmoney()));
                    map.put(key, tempvo);
                } else {
                    vo.setPk_wbsdef(wbsdefPKs[0]);
                    map.put(key, vo);
                }
            }

            Collection<MtCtplanBVO> collection = map.values();
            bodyvo = (MtCtplanBVO[])collection.toArray(new MtCtplanBVO[0]);
            List<MtCtplanBSumVO> list = (List)((IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class)).retrieveByClause(MtCtplanBSumVO.class, "pk_mt_ctplan='" + headPK + "'");
            if (list != null && list.size() > 0) {
                Map<String, UFDouble> usernummap = new HashMap();
                Iterator var21 = list.iterator();

                while(var21.hasNext()) {
                    MtCtplanBSumVO vo = (MtCtplanBSumVO)var21.next();
                    String key = vo.getPk_invbasdoc().concat(vo.getPk_invmandoc());
                    UFDouble usernum = vo.getUsednum();
                    usernummap.put(key, usernum);
                }

                MtCtplanBVO[] var25 = bodyvo;
                int var22 = 0;

                for(int var24 = bodyvo.length; var22 < var24; ++var22) {
                    MtCtplanBVO vo = var25[var22];
                    String key = vo.getPk_invbasdoc().concat(vo.getPk_invmandoc());
                    UFDouble usernum = (UFDouble)usernummap.get(key);
                    vo.setVdef10("" + usernum);
                }
            }
        }

        return bodyvo;
    }

    protected String results2String(ArrayList pks) {
        if (pks != null && pks.size() > 0) {
            StringBuffer pk = new StringBuffer();

            for(int j = 0; j < pks.size(); ++j) {
                pk.append("'" + pks.get(j) + "',");
            }

            return pk.toString().substring(0, pk.toString().lastIndexOf(","));
        } else {
            return null;
        }
    }

    public ArrayList<String> getTabbedList() {
        ArrayList retList = new ArrayList();
        retList.add("jzpm_mt_ctplan_b2");
        return retList;
    }

    public void initNappendnum(boolean flag) {
        this.getBillCardPanel().getBodyItem("nnum").setEnabled(!flag);
        this.getBillCardPanel().getBodyItem("nappendnum").setEnabled(flag);
        if (flag) {
            this.getBillCardPanel().getBodyItem("nnum").setForeground(3);
            this.getBillCardPanel().getBodyItem("nappendnum").setForeground(12);
        } else {
            this.getBillCardPanel().getBodyItem("nnum").setForeground(12);
            this.getBillCardPanel().getBodyItem("nappendnum").setForeground(3);
        }

    }

    private void afterEditBisappend(BillEditEvent e) throws Exception {
        boolean bisappend = Boolean.parseBoolean(e.getValue().toString());
        if (bisappend) {
            Object pk_projectObj = this.getBillCardPanel().getHeadItem("pk_project").getValueObject();
            if (pk_projectObj == null || pk_projectObj.toString().trim().equals("")) {
                this.showErrorMessage(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000033"));
                this.getBillCardPanel().setHeadItem("bisappend", new UFBoolean(false));
                return;
            }

            String pk_mt_normalplan = MTServiceProxy.getMTStatelessService().getMTNormalPlanPk(MtCtplanVO.class, pk_projectObj.toString(), this._getCorp().getPrimaryKey(), this.getUIControl().getPkField());
            if (pk_mt_normalplan == null || pk_mt_normalplan.trim().equals("")) {
                this.showErrorMessage(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000034"));
                this.getBillCardPanel().setHeadItem("bisappend", new UFBoolean(false));
                return;
            }

            this.getBillCardPanel().getHeadItem("pk_mt_normalplan").setValue(pk_mt_normalplan);
            this.initNappendnum(true);
        } else {
            this.getBillCardPanel().setHeadItem("pk_mt_normalplan", (Object)null);
            this.initNappendnum(false);
        }

    }

    public void countNnum(int row) throws BusinessException {
        BillModel billmodel = this.getBillCardWrapper().getBillCardPanel().getBillModel();
        UFDouble nappendnum = billmodel.getValueAt(row, "nappendnum") == null ? new UFDouble(0) : (UFDouble)billmodel.getValueAt(row, "nappendnum");
        UFDouble naccumappendnum = billmodel.getValueAt(row, "naccumappendnum") == null ? new UFDouble(0) : (UFDouble)billmodel.getValueAt(row, "naccumappendnum");
        UFDouble nsrcnum = billmodel.getValueAt(row, "nsrcnum") == null ? new UFDouble(0) : (UFDouble)billmodel.getValueAt(row, "nsrcnum");
        UFDouble nnum_show = nappendnum.add(naccumappendnum).add(nsrcnum);
        billmodel.setValueAt(nnum_show, row, "nnum");
    }

    public HashMap<String, String[]> getDigitalFieldMap() {
        if (this.digFieldMap == null) {
            this.digFieldMap = new HashMap();
            this.digFieldMap.put("BD501", this.numFieldsArray);
            this.digFieldMap.put("BD503", this.nunitrateFieldsArray);
        }

        return this.digFieldMap;
    }

    private SuperVO[] getResouceDownBodyVOs(String[] pk_trees) {
        ArrayList al = new ArrayList();

        for(int i = 0; i < pk_trees.length; ++i) {
            ArrayList tempAL = this.getTreeCache().getBodyDataByTreeKey(pk_trees[i], "jzpm_mt_ctplan_b");
            if (tempAL != null && tempAL.size() > 0) {
                al.addAll(tempAL);
            }
        }

        return (SuperVO[])al.toArray(new SuperVO[0]);
    }

    private void showNormData(int row) throws BusinessException {
        try {
            UFBoolean bisappend = new UFBoolean(this.getBillCardPanel().getHeadItem("bisappend").getValueObject().toString());
            String pk_mt_normalplan = this.getBillCardPanel().getHeadItem("pk_mt_normalplan").getValueObject() == null ? "" : this.getBillCardPanel().getHeadItem("pk_mt_normalplan").getValueObject().toString();
            if (bisappend.booleanValue() && !pk_mt_normalplan.trim().equals("")) {
                BillModel billmodel = this.getBillCardPanel().getBillModel();
                String pk_invmandoc = billmodel.getValueAt(row, "pk_invmandoc") == null ? "" : billmodel.getValueAt(row, "pk_invmandoc").toString();
                String pk_wbsdef = billmodel.getValueAt(row, "pk_wbsdef") == null ? "" : billmodel.getValueAt(row, "pk_wbsdef").toString();
                Object[] retData = new Object[]{new UFDouble(0), new UFDouble(0), null, null, null, null, null};
                if (!pk_invmandoc.equals("") && !pk_wbsdef.equals("")) {
                    retData = MTServiceProxy.getMTStatelessService().getMTNormalData(this.getUIControl().getBillType(), pk_mt_normalplan, pk_invmandoc, pk_wbsdef);
                    if (retData[2] != null) {
                        retData[5] = this.getUIControl().getBillType();
                    }
                }

                billmodel.setValueAt(((UFDouble)retData[0]).sub((UFDouble)retData[1]), row, "nsrcnum");
                billmodel.setValueAt(retData[1], row, "naccumappendnum");
                billmodel.setValueAt(retData[2], row, "vlastbillid");
                billmodel.setValueAt(retData[3], row, "vlastbillrowid");
                billmodel.setValueAt(retData[4], row, "vlastbillbts");
                billmodel.setValueAt(retData[5], row, "vlastbilltype");
            }
        } catch (BusinessException var8) {
            throw var8;
        } catch (Exception var9) {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000035"), var9);
        }
    }

    protected void afterMultiSelect(int row) {
        try {
            this.showNormData(row);
        } catch (BusinessException var3) {
            this.showErrorMessage(var3.getMessage());
        }

    }

    public void showCurrNodeData() {
        if (this.getBillTreeSelectNode() != null) {
            super.showData(this.getBillTreeSelectNode());
        }
    }

    public AggregatedValueObject getVOFromUI() throws Exception {
        AggregatedValueObject billVO = super.getVOFromUI();
        CircularlyAccessibleValueObject headvo = billVO.getParentVO();
        UFBoolean bisappend = new UFBoolean(headvo.getAttributeValue("bisappend") == null ? "N" : headvo.getAttributeValue("bisappend").toString());
        if (!bisappend.booleanValue()) {
            return billVO;
        } else {
            billVO = this.getFilterAggVOWhileAppend(billVO);
            return billVO;
        }
    }

    public AggregatedValueObject getChangedVOFromUI() throws Exception {
        AggregatedValueObject billVO = super.getChangedVOFromUI();
        CircularlyAccessibleValueObject headvo = billVO.getParentVO();
        UFBoolean bisappend = new UFBoolean(headvo.getAttributeValue("bisappend") == null ? "N" : headvo.getAttributeValue("bisappend").toString());
        if (!bisappend.booleanValue()) {
            return billVO;
        } else {
            billVO = this.getFilterAggVOWhileAppend(billVO);
            return billVO;
        }
    }

    protected AggregatedValueObject getFilterAggVOWhileAppend(AggregatedValueObject billVO) throws Exception {
        if (billVO != null && billVO.getParentVO() != null && billVO.getChildrenVO() != null) {
            AggregatedVO aggvo = (AggregatedVO)billVO;
            CircularlyAccessibleValueObject[] childvos = aggvo.getTableVO(aggvo.getTableCodes()[0]);
            List tempList = new ArrayList();
            int len = childvos.length;

            for(int i = 0; i < len; ++i) {
                CircularlyAccessibleValueObject childvo = childvos[i];
                UFDouble nappendnum = childvo.getAttributeValue("nappendnum") == null ? new UFDouble(0) : (UFDouble)childvo.getAttributeValue("nappendnum");
                if (nappendnum.compareTo(new UFDouble(0)) != 0) {
                    tempList.add(childvo);
                }
            }

            SuperVO[] childs = (SuperVO[])null;
            if (tempList != null && tempList.size() > 0) {
                childs = (SuperVO[])tempList.toArray(new SuperVO[tempList.size()]);
            }

            billVO.setChildrenVO(childs);
            ((AggVO)billVO).setTableVO(aggvo.getTableCodes()[0], childs);
            return billVO;
        } else {
            return billVO;
        }
    }

    protected void afterGeneTreeFieldEdited(String refPK) throws BusinessException {
        String pk_project = this.getBillCardPanel().getHeadItem("pk_project").getValueObject() == null ? null : this.getBillCardPanel().getHeadItem("pk_project").getValueObject().toString();
        if (!"".equals(pk_project.trim())) {
            String pk = MTServiceProxy.getMTStatelessService().checkMTUniqueProjWhileEdit(MtCtplanVO.class, pk_project, this._getCorp().getPrimaryKey(), this.getUIControl().getPkField());
            if (pk != null && !"".equals(pk.trim())) {
                throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H514100505", "UJZH514100505-8000036"));
            }
        }

    }

    public String[] getNoEditBodyItems() {
        return this.noEditBodyItems;
    }

    protected void onTreeSelectSetButtonState(TableTreeNode selectnode) {
        super.onTreeSelectSetButtonState(selectnode);

        try {
            if (this.getButtonManager().getButton(10) == null) {
                return;
            }

            if (this.getBillOperate() == 1 || this.getBillOperate() == 0 || this.getBillOperate() == 3) {
                if (selectnode != null && selectnode.isRoot()) {
                    this.getButtonManager().getButton(726).setEnabled(false);
                } else {
                    this.getButtonManager().getButton(726).setEnabled(true);
                }

                this.updateButtonUI();
            }
        } catch (Exception var3) {
            Logger.error("设置行操作按钮状态过程中发生异常");
            this.showErrorMessage(NCLangRes4VoTransl.getNCLangRes().getStrByID("JZBD_PUB", "UJZJZBD_PUB-8000125"));
        }

        UFBoolean bisappend = new UFBoolean(this.getBillCardPanel().getHeadItem("bisappend").getValueObject().toString());
        if (!selectnode.isRoot()) {
            if (!bisappend.booleanValue()) {
                this.initNappendnum(false);
            } else {
                this.initNappendnum(true);
            }
        }

    }

    protected void initRef() {
        super.initRef();
        if (this.getBillCardPanel().getBodyItem("vadmixturesname") != null) {
            UIRefPane refPanel = (UIRefPane)this.getBillCardPanel().getBodyItem("vadmixturesname").getComponent();
            refPanel.setMultiSelectedEnabled(true);
        }

    }

    private void initSubTotalBtn() {
        JzCommonButtonVO subTotalBVO = new JzCommonButtonVO("小计合计", "小计合计", 773);
        subTotalBVO.setOperateStatus(new int[]{2, 5});
        this.addPrivateButton(subTotalBVO);
        JzCommonButtonVO levelSubTotalBVO = new JzCommonButtonVO("分级小计", "分级小计", 774);
        levelSubTotalBVO.setOperateStatus(new int[]{2, 5});
        this.addPrivateButton(levelSubTotalBVO);
        subTotalBVO.setChildAry(new int[]{774});
    }

    protected int getCurTabIndex() {
        return !this.isListPanelSelected() ? this.getBillCardPanel().getBodyTabbedPane().getSelectedIndex() : this.getBillListPanel().getBodyTabbedPane().getSelectedIndex();
    }

    public void stateChanged(ChangeEvent e) {
        int tabIndex = this.getCurTabIndex();
        if (!this.isListPanelSelected()) {
            this.updateSubTotalBtnState(tabIndex);
        }

        super.stateChanged(e);
    }

    protected void updateSubTotalBtnState(int tableIndex) {
        ButtonObject subTotalBtn = this.getButtonManager().getButton(773);
        ButtonObject levelSubTotalBtn = this.getButtonManager().getButton(774);
        if (this.getBillOperate() != 2 && this.getBillOperate() != 5) {
            subTotalBtn.setEnabled(false);
            levelSubTotalBtn.setEnabled(false);
        } else if (tableIndex == 1) {
            subTotalBtn.setEnabled(true);
            levelSubTotalBtn.setEnabled(true);
        } else {
            subTotalBtn.setEnabled(false);
            levelSubTotalBtn.setEnabled(false);
        }

        this.updateButtons();
    }

    public void update(Observable o, Object arg) {
        super.update(o, arg);
        int tabIndex = this.getCurTabIndex();
        if (!this.isListPanelSelected()) {
            this.updateSubTotalBtnState(tabIndex);
        }

    }

    public Map<String, UFDouble> getPriceMap() {
        return this.priceMap;
    }

    public void setPriceMap(Map<String, UFDouble> priceMap) {
        this.priceMap = priceMap;
    }
}
