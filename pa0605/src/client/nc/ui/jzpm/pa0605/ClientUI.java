//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.pa0605;

import nc.bs.framework.common.NCLocator;
import nc.bs.logging.Logger;
import nc.itf.uap.bd.cust.ICustBankQry;
import nc.itf.uap.bd.psndoc.IPsnDocQueryService;
import nc.ui.jz.pub.DeptPsnRela;
import nc.ui.jz.pub.buttonstate.BillLinkBtnVO;
import nc.ui.jzpm.core.pub.ref.BankAccCustAndPsnRefModel;
import nc.ui.jzpm.pa0605.switchaction.CloseBtnVO;
import nc.ui.jzpm.pa0605.switchaction.OpenBtnVO;
import nc.ui.jzpm.pa0605.switchaction.SwitchGroupBtnVO;
import nc.ui.jzpm.papub.pay.PayCFreeCustUtil;
import nc.ui.jzpm.pub.ref.JZPMAccBankFilterCurrtypeRefModel;
import nc.ui.jzpm.pub.ui.SingleHeadBillManageUI;
import nc.ui.pub.FramePanel;
import nc.ui.pub.beans.UIRefPane;
import nc.ui.pub.bill.BillCardBeforeEditListener;
import nc.ui.pub.bill.BillEditEvent;
import nc.ui.pub.bill.BillItem;
import nc.ui.pub.bill.BillItemEvent;
import nc.ui.pub.bill.BillTabbedPane;
import nc.ui.scm.freecust.UFRefGridUI;
import nc.ui.trade.bill.AbstractManageController;
import nc.ui.trade.manage.ManageEventHandler;
import nc.vo.jz.pub.BillDateGetter;
import nc.vo.jzpm.pa07.IFeeBillType;
import nc.vo.jzpm.pa07.IPayType;
import nc.vo.jzpm.pub.JZPMProxy;
import nc.vo.pub.AggregatedValueObject;
import nc.vo.pub.BusinessException;
import nc.vo.pub.billtype.BilltypeVO;
import nc.vo.pub.lang.UFDouble;
import nc.vo.trade.button.ButtonVO;

public class ClientUI extends SingleHeadBillManageUI implements BillCardBeforeEditListener {
    private static final long serialVersionUID = 1L;
    private DeptPsnRela deptPsnRela = null;

    public ClientUI(FramePanel fp) {
        super(fp);

        try {
            this.initTransType();
            this.setBillOperate(4);
        } catch (Exception var3) {
            Logger.error(var3);
        }

    }
    /**
     * add by HMX 2019-3-29
     * */
    @Override
    protected void initPrivateButton() {
    	super.initPrivateButton();
    	initWitchGroupBtn(); //添加一个按钮组，包含打开与关闭两个按钮
    }
    
    /**
     * create by HMX 2019-3-29
     * 添加一个按钮组，包含打开与关闭两个按钮
     * */
    private void initWitchGroupBtn(){
    	addPrivateButton(new CloseBtnVO().getButtonVO());
    	
    	addPrivateButton(new OpenBtnVO().getButtonVO());
    	
    	ButtonVO switchBtnVO = new SwitchGroupBtnVO().getButtonVO();
    	switchBtnVO.setChildAry(new int[]{CloseBtnVO.CLOSE_BTN_CODE,OpenBtnVO.OPEN_BTN_CODE});
    	addPrivateButton(switchBtnVO);
    }
    
    public DeptPsnRela getDeptPsnRela() {
        if (this.deptPsnRela == null) {
            BillItem deptItem = this.getBillCardPanel().getHeadItem("pk_deptdoc");
            BillItem psnItem = this.getBillCardPanel().getHeadItem("pk_psndoc");
            this.deptPsnRela = new DeptPsnRela(deptItem, psnItem);
        }

        return this.deptPsnRela;
    }

    protected void initEventListener() {
        super.initEventListener();
        this.getBillCardPanel().setBillBeforeEditListenerHeadTail(this);
    }

    protected void initRef() {
        this.initOuraccount();
        this.initFreeCustRef();
    }

    private void initOuraccount() {
        UIRefPane ref = (UIRefPane)this.getBillCardPanel().getHeadItem("pk_ouraccount").getComponent();
        JZPMAccBankFilterCurrtypeRefModel refModel = (JZPMAccBankFilterCurrtypeRefModel)ref.getRefModel();
        refModel.addWherePart(" and isnull(isinneracc,'N') = 'N'");
    }

    private void initFreeCustRef() {
        ((UIRefPane)this.getBillCardPanel().getHeadItem("cfreecustid").getComponent()).getRef().setRefUI(new UFRefGridUI(this));
    }

    public void setDefaultData() throws Exception {
        super.setDefaultData();
        this.getDeptPsnRela().setOnAdd();
        this.getBillCardPanel().setHeadItem(this.getBaseCurrFldName(), this.getCy().getLocalCurrPK());
        this.getBillCardPanel().setHeadItem(this.getOriginCurrFldName(), this.getCy().getLocalCurrPK());
        this.getBillCardPanel().setHeadItem(this.getBaseRateFldName(), UFDouble.ONE_DBL);
        this.getBillCardPanel().setHeadItem("dhappendate", BillDateGetter.getBillDate());
        this.getBillCardPanel().setHeadItem("vtranstype", this.getvtranstype());
        this.getBillCardPanel().setHeadItem("vmodulecode", this.getModuleCode());
    }

    public void afterEdit(BillEditEvent e) {
        String key = e.getKey();
        if (e.getPos() == 0) {
            this.afterHeadEdit(key);
        }

    }

    private void afterHeadEdit(String key) {
        if (key.equalsIgnoreCase("pk_psndoc")) {
            this.getDeptPsnRela().setDeptByPsn();
        } else if (key.equalsIgnoreCase("pk_deptdoc")) {
            this.getDeptPsnRela().setPsnByDept();
        } else if (key.equalsIgnoreCase("ipaytype")) {
            this.initproperty();
        } else if (key.equalsIgnoreCase("pk_cumandoc")) {
            this.refreshBank();
            PayCFreeCustUtil.setVHeadseconedAccidAttri(this.getBillCardPanel(), "pk_cubasdoc", "cfreecustid", "pk_otheraccount", "otheraccount");
            this.getBillCardPanel().execHeadLoadFormulas();
        } else if (key.equalsIgnoreCase("pk_inuser")) {
            this.refreshBank();
        } else if ("noriginmny".equalsIgnoreCase(key)) {
            UFDouble noriginValue = this.getHeadItemDouble("noriginmny");
            UFDouble nbaseValue = this.getBaseMny(noriginValue);
            this.getBillCardPanel().getHeadItem("nbasemny").setValue(nbaseValue);
        }

    }

    private void refreshBank() {
        this.getBillCardPanel().getHeadItem("cfreecustid").setValue((Object)null);
        this.getBillCardPanel().getHeadItem("pk_otheraccount").setValue((Object)null);
        this.getBillCardPanel().getHeadItem("otheraccount").setValue((Object)null);
        this.getBillCardPanel().getHeadItem("otheraccountname").setValue((Object)null);
    }

    public void setCardUIData(AggregatedValueObject vo) throws Exception {
        super.setCardUIData(vo);
        PayCFreeCustUtil.setVHeadseconedAccidShow(this.getBillCardPanel(), "pk_cubasdoc", "cfreecustid", "pk_otheraccount", "otheraccount");
    }

    protected ButtonVO[] initAssQryBtnVOs() {
        return new ButtonVO[]{(new BillLinkBtnVO()).getButtonVO()};
    }

    public String[] getHeadBaseItems() {
        return new String[]{"nbasemny"};
    }

    public String[] getHeadOriginItems() {
        return new String[]{"noriginmny"};
    }

    public boolean beforeEdit(BillItemEvent e) {
        int billOperate = this.getBillOperate();
        if (0 == billOperate || 1 == billOperate) {
            if ("pk_otheraccount".equalsIgnoreCase(e.getItem().getKey())) {
                this.filterBank();
            } else if ("pk_inuser".equalsIgnoreCase(e.getItem().getKey())) {
                if (this.isInuser()) {
                    this.getBillCardPanel().getHeadItem("pk_inuser").setEdit(true);
                } else {
                    this.getBillCardPanel().getHeadItem("pk_inuser").setEdit(false);
                }
            } else if ("pk_cumandoc".equalsIgnoreCase(e.getItem().getKey())) {
                if (this.isCumandoc()) {
                    this.getBillCardPanel().getHeadItem("pk_cumandoc").setEdit(true);
                } else {
                    this.getBillCardPanel().getHeadItem("pk_cumandoc").setEdit(false);
                }
            } else if ("cfreecustid".equals(e.getItem().getKey()) && !this.isCfreecust()) {
                this.getBillCardPanel().getHeadItem("pk_cumandoc").setEdit(false);
            }
        }

        return false;
    }

    private boolean isInuser() {
        boolean bool = false;
        String ipayTypeValue = this.getHeadItemString("ipaytype");
        if (ipayTypeValue != null && "1".equals(ipayTypeValue.trim())) {
            bool = true;
        }

        return bool;
    }

    private boolean isCumandoc() {
        boolean bool = false;
        String ipayTypeValue = this.getHeadItemString("ipaytype");
        if (ipayTypeValue != null && "0".equals(ipayTypeValue.trim())) {
            bool = true;
        }

        return bool;
    }

    private boolean isCfreecust() {
        boolean bool = false;
        String pk_cumandoc = this.getHeadItemString("pk_cumandoc");
        if (pk_cumandoc != null && pk_cumandoc.trim().length() > 0) {
            bool = true;
        }

        return bool;
    }

    private void filterBank() {
        String pk_cubasdoc = this.getHeadItemString("pk_cubasdoc");
        String pk_inbasuser = this.getHeadItemString("pk_inbasuser");
        String resetConsult = "pk_otheraccount";
        String[] banks = null;
        if (pk_cubasdoc != null && pk_cubasdoc.trim().length() > 0) {
            ICustBankQry psnqry = (ICustBankQry)NCLocator.getInstance().lookup(ICustBankQry.class);

            try {
                banks = psnqry.queryPKsByCuBasPK(pk_cubasdoc);
            } catch (BusinessException var8) {
                Logger.error(var8.getMessage(), var8);
            }
        } else if (pk_inbasuser != null && pk_inbasuser.trim().length() > 0) {
            IPsnDocQueryService idqs = (IPsnDocQueryService)NCLocator.getInstance().lookup(IPsnDocQueryService.class);

            try {
                banks = idqs.queryBankaccbasPksByPsnbasdocPk(pk_inbasuser);
            } catch (BusinessException var7) {
                Logger.error(var7.getMessage(), var7);
            }
        }

        UIRefPane ref = (UIRefPane)this.getBillCardPanel().getHeadItem(resetConsult).getComponent();
        BankAccCustAndPsnRefModel refmd = (BankAccCustAndPsnRefModel)ref.getRefModel();
        refmd.setFilterPks(banks);
    }

    private void initproperty() {
        int str = Integer.valueOf(this.getHeadItemString("ipaytype"));
        if (0 == str) {
            this.getBillCardPanel().getHeadItem("pk_inuser").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("pk_otheraccount").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("otheraccount").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("otheraccountname").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("pk_inuser").setEdit(false);
            this.getBillCardPanel().getHeadItem("pk_cumandoc").setEdit(true);
        } else if (1 == str) {
            this.getBillCardPanel().getHeadItem("pk_cumandoc").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("pk_cubasdoc").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("cfreecustid").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("pk_otheraccount").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("otheraccount").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("otheraccountname").setValue((Object)null);
            this.getBillCardPanel().getHeadItem("pk_otheraccount").setShow(true);
            this.getBillCardPanel().getHeadItem("otheraccount").setShow(false);
            this.getBillCardPanel().getHeadItem("pk_cumandoc").setEdit(false);
            this.getBillCardPanel().getHeadItem("cfreecustid").setEdit(false);
            this.getBillCardPanel().getHeadItem("pk_inuser").setEdit(true);
            BillTabbedPane tabPanel = this.getBillCardPanel().getHeadTabbedPane();
            int index = tabPanel.getSelectedIndex();
            this.getBillCardPanel().setBillData(this.getBillCardPanel().getBillData());
            tabPanel.setSelectedIndex(index);
        }

    }

    protected void initComboBox() {
        super.initComboBox();
        this.getBillCardWrapper().initHeadComboBox("ifeebilltype", IFeeBillType.I_FEEBILL_TYPE, true);
        this.getBillListWrapper().initHeadComboBox("ifeebilltype", IFeeBillType.I_FEEBILL_TYPE, true);
        this.getBillCardWrapper().initHeadComboBox("ipaytype", IPayType.I_PAY_TYPE, true);
        this.getBillListWrapper().initHeadComboBox("ipaytype", IPayType.I_PAY_TYPE, true);
    }

    protected AbstractManageController createController() {
        return new ClientCtrl();
    }

    protected ManageEventHandler createEventHandler() {
        return new ClientEventHandler(this, this.getUIControl());
    }

    private void initTransType() throws BusinessException {
        String moudelcode = this.getModuleCode();
        if (moudelcode != null && !moudelcode.equals("H5170605")) {
            String sqlwhere = " parentbilltype='99Q6'  and nodecode='" + moudelcode + "' and isnull(dr,0)=0";

            try {
                BilltypeVO[] vos = (BilltypeVO[])((BilltypeVO[])JZPMProxy.getIJZPMPubBusi().queryByClause(BilltypeVO.class, sqlwhere, "ts"));
                if (vos != null && vos.length > 0 && vos[0].getPk_billtypecode() != null) {
                    this.setvtranstype(vos[0].getPk_billtypecode());
                }
            } catch (Exception var5) {
                Logger.error("初始化交易类型失败!", var5);
            }

        }
    }
}
