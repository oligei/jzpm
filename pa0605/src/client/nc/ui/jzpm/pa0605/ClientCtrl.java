//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.pa0605;

import nc.ui.jzpm.pa0605.switchaction.SwitchGroupBtnVO;
import nc.ui.jzpm.pub.buttonsort.JZPMPubButtonSort;
import nc.ui.trade.bill.AbstractManageController;
import nc.ui.trade.bill.ISingleController;
import nc.vo.jzpm.pa07.PaPettyfeeVO;
import nc.vo.trade.pub.HYBillVO;

/**
 * decompile by HMX 2019-3-28
 * */
public class ClientCtrl extends AbstractManageController implements ISingleController {
    public ClientCtrl() {
    }

    public String getBillType() {
        return "99Q6";
    }

    /**
     * update by HMX 2019-3-29
     * 添加一个按钮组 SwitchGroupBtnVO.SWITCH_BTN_CODE
     * */
    public int[] getCardButtonAry() {
        return (new JZPMPubButtonSort()).sort(new int[]{1, 3, 0, 7, 4, 5, 8, 25,16, 511, 6, 20, 31,SwitchGroupBtnVO.SWITCH_BTN_CODE});
    }

    /**
     * update by HMX 2019-3-29
     * 添加一个按钮组 SwitchGroupBtnVO.SWITCH_BTN_CODE
     * */
    public int[] getListButtonAry() {
        return (new JZPMPubButtonSort()).sort(new int[]{1, 3, 0, 7, 4, 5, 8, 25, 16, 511, 20, 30,SwitchGroupBtnVO.SWITCH_BTN_CODE});
    }

    public String[] getCardBodyHideCol() {
        return null;
    }

    public String[] getBillVoName() {
        return new String[]{HYBillVO.class.getName(), PaPettyfeeVO.class.getName(), PaPettyfeeVO.class.getName()};
    }

    public String getBodyCondition() {
        return null;
    }

    public String getBodyZYXKey() {
        return null;
    }

    public int getBusinessActionType() {
        return 0;
    }

    public String getChildPkField() {
        return "";
    }

    public String getHeadZYXKey() {
        return null;
    }

    public String getPkField() {
        return "pk_pettyfee";
    }

    public Boolean isEditInGoing() throws Exception {
        return new Boolean(false);
    }

    public boolean isExistBillStatus() {
        return true;
    }

    public boolean isLoadCardFormula() {
        return true;
    }

    public String[] getListBodyHideCol() {
        return null;
    }

    public String[] getListHeadHideCol() {
        return null;
    }

    public boolean isShowListRowNo() {
        return true;
    }

    public boolean isShowListTotal() {
        return true;
    }

    public boolean isShowCardRowNo() {
        return true;
    }

    public boolean isShowCardTotal() {
        return true;
    }

    public boolean isSingleDetail() {
        return false;
    }
}
