package nc.ui.jzpm.pa0605.switchaction;

import nc.ui.jzpm.pub.buttonstate.JZPMButtonVO;
import nc.ui.trade.base.IBillOperate;
import nc.vo.trade.pub.IBillStatus;

/**
 * create by HMX 2019-3-29
 * 按钮组定义
 * */
public class SwitchGroupBtnVO {
	public static final int SWITCH_BTN_CODE = 28;
	
	 JZPMButtonVO btnVO = null;
	 
	 public JZPMButtonVO getButtonVO() {
	     if (btnVO == null) {
	      btnVO = new JZPMButtonVO();
	       btnVO.setBtnNo(SWITCH_BTN_CODE);
	       btnVO.setBtnName("下游权限开关");
	       btnVO.setHintStr("下游权限开关");
	       btnVO.setBtnChinaName("下游权限开关");
	       btnVO.setOperateStatus(new int[] { IBillOperate.OP_NOTEDIT});
	       btnVO.setBusinessStatus(new int[]{IBillStatus.CHECKPASS});
	     }
	      return btnVO;
	   }

}
