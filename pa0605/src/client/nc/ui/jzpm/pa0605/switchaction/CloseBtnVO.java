package nc.ui.jzpm.pa0605.switchaction;

import nc.ui.jzpm.pub.buttonstate.JZPMButtonVO;
import nc.vo.trade.button.ButtonVO;

/**
 * create by HMX 2019-3-29
 * 关闭按钮定义
 * */
public class CloseBtnVO {
	
	public static final int CLOSE_BTN_CODE = 26;
 
	 JZPMButtonVO btnVO = null;
	 
	  public ButtonVO getButtonVO() {
	     if (btnVO == null) {
	      btnVO = new JZPMButtonVO();
	       btnVO.setBtnNo(CLOSE_BTN_CODE);
	       btnVO.setBtnName("关闭");
	       btnVO.setHintStr("关闭");
	       btnVO.setBtnChinaName("关闭");
	       btnVO.setOperateStatus(new int[] { 2, 4 });
	     }
	      return btnVO;
	   }

}
