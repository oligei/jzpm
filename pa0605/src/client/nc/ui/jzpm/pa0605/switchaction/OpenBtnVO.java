package nc.ui.jzpm.pa0605.switchaction;

import nc.ui.jzpm.pub.buttonstate.JZPMButtonVO;
import nc.vo.trade.button.ButtonVO;

/**
 * create by HMX 2019-3-29
 * 打开按钮定义
 * */
public class OpenBtnVO {
	
	public static final int OPEN_BTN_CODE = 27;

	JZPMButtonVO btnVO = null;
	 
	  public ButtonVO getButtonVO() {
	     if (btnVO == null) {
	      btnVO = new JZPMButtonVO();
	       btnVO.setBtnNo(OPEN_BTN_CODE);
	       btnVO.setBtnName("打开");
	       btnVO.setHintStr("打开");
	       btnVO.setBtnChinaName("打开");
	       btnVO.setOperateStatus(new int[] { 2, 4 });
	     }
	      return btnVO;
	   }
}
