//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.pa0605;

import java.util.ArrayList;
import java.util.List;

import nc.bs.framework.common.NCLocator;
import nc.itf.uap.IVOPersistence;
import nc.ui.jzpm.pa0605.switchaction.CloseBtnVO;
import nc.ui.jzpm.pa0605.switchaction.OpenBtnVO;
import nc.ui.jzpm.pub.eventhandler.ManageEventHandler;
import nc.ui.pi.pub.PiPqPublicUIClass;
import nc.ui.pub.ButtonObject;
import nc.ui.pub.beans.UIDialog;
import nc.ui.pub.bill.BillCardPanel;
import nc.ui.querytemplate.normalpanel.INormalQueryPanel;
import nc.ui.trade.controller.IControllerBase;
import nc.ui.trade.manage.BillManageUI;
import nc.vo.jzpm.pa07.PaPettyfeeVO;
import nc.vo.pub.AggregatedValueObject;
import nc.vo.pub.BusinessException;

@SuppressWarnings("restriction")
public class ClientEventHandler extends ManageEventHandler {
    public ClientEventHandler(BillManageUI billUI, IControllerBase control) {
        super(billUI, control);
    }

    public void onBoAdd(ButtonObject bo) throws Exception {
        super.onBoAdd(bo);
        this.setEditableWhenEdit_FreeCustAndBank();
    }

    protected UIDialog createQueryUI() {
        return new PaNoContQueryDlg(this.getBillUI(), (INormalQueryPanel)null, this.getTemplateInfo());
    }

    private void setEditableWhenEdit_FreeCustAndBank() {
        BillCardPanel billCardPanel = this.getBillManageUI().getBillCardPanel();
        if (billCardPanel.getHeadItem("cfreecustid").getValueObject() == null) {
            if (billCardPanel.getHeadItem("pk_cubasdoc").getValueObject() != null && billCardPanel.getHeadItem("pk_cubasdoc").getValueObject().toString().trim().length() >= 1) {
                String sFreeFlag = "N";
                Object oTemp = billCardPanel.getHeadItem("pk_cubasdoc").getValueObject();
                if (oTemp != null) {
                    oTemp = PiPqPublicUIClass.getAResultFromTable("bd_cubasdoc", "freecustflag", "pk_cubasdoc", oTemp.toString());
                }

                if (oTemp != null) {
                    sFreeFlag = oTemp.toString();
                }

                if (sFreeFlag != null && sFreeFlag.trim() != null && sFreeFlag.equals("Y")) {
                    billCardPanel.getHeadItem("cfreecustid").setEnabled(true);
                } else {
                    billCardPanel.getHeadItem("cfreecustid").setEnabled(false);
                }
            } else {
                billCardPanel.getHeadItem("cfreecustid").setEnabled(false);
            }
        } else {
            billCardPanel.getHeadItem("cfreecustid").setEnabled(true);
        }

    }

    protected void onBoEdit() throws Exception {
        super.onBoEdit();
        this.setEditableWhenEdit_FreeCustAndBank();
    }

    protected String getHeadCondition() {
        String sqlWhere = super.getHeadCondition();
        if (sqlWhere != null && sqlWhere.length() > 0) {
            sqlWhere = sqlWhere + " and pk_billtype='" + "99Q6" + "' and vmodulecode='" + this.getBillUI().getModuleCode() + "'";
        } else {
            sqlWhere = " pk_billtype='99Q6' and vmodulecode='" + this.getBillUI().getModuleCode() + "'";
        }
        return sqlWhere;
    }
    
    
    /**
     * create by HMX 2019-3-29
     * 控制付款单能否参照到数据
     * */
    @Override
    protected void onBoElse(int intBtn) throws Exception {
    	super.onBoElse(intBtn);
    	switch (intBtn) {
		case OpenBtnVO.OPEN_BTN_CODE:
			aferOpenBtnClick();
			break;
			
		case CloseBtnVO.CLOSE_BTN_CODE:
			aferCloseBtnClick();
			break;

		default:
			break;
		}
    }
    /**
     * create by HMX 2019-3-29
     * 点击打开，付款单能进行参照
     * @throws BusinessException 
     * */
    private void aferOpenBtnClick() throws Exception{
    	AggregatedValueObject  ob = this.getBufferData().getCurrentVO();
    	if(ob!=null){
    		PaPettyfeeVO paPettyfeeVO = (PaPettyfeeVO) ob.getParentVO();
    		paPettyfeeVO.setVreserve50(null);
    		List<PaPettyfeeVO> volist = new ArrayList<PaPettyfeeVO>(1);
    		volist.add(paPettyfeeVO);
    		((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).updateVOList(volist);
    		this.getBufferData().refresh();
    	}
    	
    }
    /**
     * create by HMX 2019-3-29
     * 点击关闭，付款单不能进行参照
     * @throws Exception 
     * */
    private void aferCloseBtnClick() throws Exception{
    	AggregatedValueObject  ob = this.getBufferData().getCurrentVO();
    	if(ob!=null){
    		PaPettyfeeVO paPettyfeeVO = (PaPettyfeeVO) ob.getParentVO();
    		paPettyfeeVO.setVreserve50("1");
    		List<PaPettyfeeVO> volist = new ArrayList<PaPettyfeeVO>(1);
    		volist.add(paPettyfeeVO);
    		((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).updateVOList(volist);
    		this.getBufferData().refresh();
    	}
    }
    /**
     * create by HMX 2019-3-29
     * 不允许弃审关闭态的数据
     * */
    @Override
    protected void onBoCancelAudit() throws Exception {
    	AggregatedValueObject  ob = this.getBufferData().getCurrentVO();
    	if(ob!=null){
    		PaPettyfeeVO paPettyfeeVO = (PaPettyfeeVO) ob.getParentVO();
    		if("1".equals(paPettyfeeVO.getVreserve50())){
    			throw new Exception("选中数据处于关闭态，请先将其打开后在弃审！");
    		}
    	}
    	super.onBoCancelAudit();
    }
}
