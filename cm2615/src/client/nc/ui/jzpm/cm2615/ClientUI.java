/*     */ package nc.ui.jzpm.cm2615;
/*     */ 
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.Vector;
/*     */ import javax.swing.event.ChangeEvent;

import nc.bs.framework.common.NCLocator;
/*     */ import nc.bs.logging.Logger;
/*     */ import nc.itf.jzpm.pub.IJZPMPubBusi;
import nc.itf.uap.IUAPQueryBS;
/*     */ import nc.ui.bd.ref.busi.InvmandocDefaultRefModel;
/*     */ import nc.ui.bd.ref.busi.MeasdocDefaultRefModel;
/*     */ import nc.ui.ic.pub.bill.InvoInfoBYFormula;
/*     */ import nc.ui.ic.pub.bill.cell.FreeItemCellRender;
/*     */ import nc.ui.jzpm.cmpub.contract.CMClientUI;
/*     */ import nc.ui.jzpm.cmpub.contract.CMContractDelegator;
/*     */ import nc.ui.jzpm.core.buttonstate.ButtonFactory;
/*     */ import nc.ui.jzpm.core.buttonstate.ConsignorBtnVO;
/*     */ import nc.ui.jzpm.core.buttonstate.CustomRelaPB;
/*     */ import nc.ui.jzpm.core.buttonstate.RequirePlanBtnVO;
/*     */ import nc.ui.jzpm.core.buttonstate.RequireTotalPlanBtnVO;
/*     */ import nc.ui.jzpm.core.pub.ref.AssistUnitRefModel;
/*     */ import nc.ui.jzpm.core.pub.ref.ContTypeRefTreeModel;
/*     */ import nc.ui.jzpm.subpub.SubLineColorCtrl;
/*     */ import nc.ui.pub.ButtonObject;
/*     */ import nc.ui.pub.beans.MessageDialog;
/*     */ import nc.ui.pub.beans.UIRefPane;
/*     */ import nc.ui.pub.bill.BillCardPanel;
/*     */ import nc.ui.pub.bill.BillEditEvent;
/*     */ import nc.ui.pub.bill.BillItem;
/*     */ import nc.ui.pub.bill.BillListPanel;
/*     */ import nc.ui.pub.bill.BillModel;
/*     */ import nc.ui.pub.bill.BillTabbedPane;
/*     */ import nc.ui.scm.ic.freeitem.FreeItemRefPane;
/*     */ import nc.ui.trade.bill.AbstractManageController;
/*     */ import nc.ui.trade.button.ButtonManager;
/*     */ import nc.ui.trade.button.ButtonVOFactory;
/*     */ import nc.vo.bd.CorpVO;
/*     */ import nc.vo.bd.service.FieldColumnVO;
/*     */ import nc.vo.jz.pub.SafeCompute;
/*     */ import nc.vo.jzpm.cm0535.CmJzscmctrefVO;
/*     */ import nc.vo.jzpm.cm2615.CMStockContractAggVO;
/*     */ import nc.vo.jzpm.cmpub.contract.CmStocklistVO;
import nc.vo.jzpm.mt100505.MtCtplanBSumVO;
/*     */ import nc.vo.jzpm.pub.JZPMProxy;
/*     */ import nc.vo.ml.AbstractNCLangRes;
/*     */ import nc.vo.ml.NCLangRes4VoTransl;
/*     */ import nc.vo.pub.BusinessException;
/*     */ import nc.vo.pub.CircularlyAccessibleValueObject;
/*     */ import nc.vo.pub.lang.UFDouble;
/*     */ import nc.vo.scm.ic.bill.FreeVO;
/*     */ import nc.vo.scm.ic.bill.InvVO;
/*     */ import nc.vo.scm.pub.SCMEnv;
import nc.vo.trade.button.ButtonVO;
/*     */ 
			@SuppressWarnings("restriction")
/*     */ public class ClientUI extends CMClientUI
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*  57 */   private String oldContmeasdoc = null;
/*  58 */   private HashMap<String, String[][]> map = null;
/*  59 */   private ContTypeRefTreeModel contTypeModel = null;
/*     */   private String billtype;
/*  61 */   private FreeItemRefPane ivjFreeItemRefPane = null;
/*     */   
/*  63 */   private FreeItemCellRender m_freeCellRender = null;
/*     */   
/*  65 */   private Map<String, InvVO> invMap = new HashMap<String, InvVO>();
/*  66 */   private InvoInfoBYFormula m_InvoInfoBYFormula = null;
/*     */   
/*     */   public String getBilltype()
/*     */   {
/*  70 */     if (billtype == null) {
/*  71 */       Object vlastbilltype = getBillCardPanel().getBillModel("jzpm_cm_stocklist").getBodyValueRowVO(0, CmStocklistVO.class.getName()).getAttributeValue("vlastbilltype");
/*     */       
/*  73 */       if (vlastbilltype != null) {
/*  74 */         billtype = vlastbilltype.toString();
/*     */       }
/*     */     }
/*  77 */     return billtype;
/*     */   }
/*     */   
/*     */   public void setBilltype(String billtype) {
/*  81 */     this.billtype = billtype;
/*     */   }
/*     */   
/*     */ 
/*     */   public ClientUI() {}
/*     */   
/*     */ 
/*     */   public ClientUI(String pk_corp, String pk_billType, String pk_busitype, String operater, String billId)
/*     */   {
/*  90 */     super(pk_corp, pk_billType, pk_busitype, operater, billId);
/*     */   }
/*     */   
/*     */   public void init()
/*     */   {
/*  95 */     super.init();
/*     */     
/*  97 */     getFreeItemRefPane().setMaxLength(getBillCardPanel().getBodyItem("vfree0").getLength());
/*     */     
/*  99 */     getBillCardPanel().getBodyItem("vfree0").setComponent(getFreeItemRefPane());
/*     */     
/* 101 */     getFreeItemCellRender().setRenderer("vfree0");
/*     */   }
/*     */   
/*     */   protected FreeItemRefPane getFreeItemRefPane() {
/* 105 */     if (ivjFreeItemRefPane == null) {
/* 106 */       if ((getBillCardPanel().getBodyItem("vfree0").getComponent() != null) && ((getBillCardPanel().getBodyItem("vfree0").getComponent() instanceof FreeItemRefPane)))
/*     */       {
/* 108 */         ivjFreeItemRefPane = ((FreeItemRefPane)getBillCardPanel().getBodyItem("vfree0").getComponent());
/* 109 */         return ivjFreeItemRefPane;
/*     */       }
/*     */       try
/*     */       {
/* 113 */         ivjFreeItemRefPane = new FreeItemRefPane();
/* 114 */         ivjFreeItemRefPane.setName("FreeItemRefPane");
/* 115 */         ivjFreeItemRefPane.setLocation(209, 4);
/*     */       } catch (Throwable ivjExc) {
/* 117 */         handleException(ivjExc);
/*     */       }
/*     */     }
/* 120 */     return ivjFreeItemRefPane;
/*     */   }
/*     */   
/*     */   protected FreeItemCellRender getFreeItemCellRender() {
/* 124 */     if (m_freeCellRender == null)
/* 125 */       m_freeCellRender = new FreeItemCellRender(getBillCardPanel()) {
/*     */         public boolean findRow(int iRow, Object oValue) {
/* 127 */           InvVO voInv = null;
/* 128 */           String key = (String)getBillCardPanel().getBodyValueAt(iRow, "pk_invmandoc");
/*     */           
/* 130 */           if (invMap.containsKey(key)) {
/* 131 */             voInv = (InvVO)invMap.get(key);
/*     */           }
/*     */           
/* 134 */           if ((voInv != null) && (voInv.getIsFreeItemMgt() != null) && (voInv.getIsFreeItemMgt().intValue() == 1))
/*     */           {
/* 136 */             return (oValue == null) || (oValue.toString().trim().length() <= 0);
/*     */           }
/*     */           
/* 139 */           return false;
/*     */         } };
/* 141 */     return m_freeCellRender;
/*     */   }
/*     */   
/*     */   public InvoInfoBYFormula getInvoInfoBYFormula()
/*     */   {
/* 146 */     if (m_InvoInfoBYFormula == null)
/* 147 */       m_InvoInfoBYFormula = new InvoInfoBYFormula(getCorpPrimaryKey());
/* 148 */     return m_InvoInfoBYFormula;
/*     */   }
/*     */   
/*     */   private void afterEditInv(BillEditEvent e) {
/* 152 */     String[] refPks = new String[1];
/* 153 */     UIRefPane refPane = (UIRefPane)getBillCardPanel().getBillModel().getItemByKey("vinvmandoccode").getComponent();
/*     */     
/* 155 */     refPks = refPane.getRefPKs();
/*     */     
/* 157 */     if ((refPks == null) || (refPks.length == 0)) {
/* 158 */       return;
/*     */     }
/*     */     
/* 161 */     InvVO[] invVOs = getInvoInfoBYFormula().getInvParseWithPlanPriceAndPackType(refPks, null, null, getCorpPrimaryKey(), true, true);
/*     */     
/* 163 */     String sVfree0 = invVOs[0].getFreeItemVO().getVfree0();
/* 164 */     if ((sVfree0 != null) && (sVfree0.trim().length() > 0)) {
/* 165 */       getBillCardPanel().setBodyValueAt(sVfree0, e.getRow(), "vfree0");
/*     */     } else {
/* 167 */       getBillCardPanel().setBodyValueAt("", e.getRow(), "vfree0");
/*     */     }
/* 169 */     for (InvVO vo : invVOs) {
/* 170 */       invMap.put(vo.getCinventoryid(), vo);
/*     */     }
/*     */     
/* 173 */     getFreeItemCellRender().setRenderer("vfree0");
/*     */   }
/*     */   
/*     */   public void initInvMap() {
/* 177 */     int rowCnt = 0;
/* 178 */     int rowCnt1 = 0;
/* 179 */     BillModel bm = null;
/* 180 */     BillModel bm1 = null;
/* 181 */     if (isListPanelSelected()) {
/* 182 */       bm = getBillListPanel().getBodyBillModel("jzpm_cm_stocklist");
/* 183 */       bm1 = getBillListPanel().getBodyBillModel("jzpm_cm_stocklist_b");
/*     */     } else {
/* 185 */       bm = getBillCardPanel().getBillModel("jzpm_cm_stocklist");
/* 186 */       bm1 = getBillCardPanel().getBillModel("jzpm_cm_stocklist_b");
/*     */     }
/*     */     
/* 189 */     String pk_invmandoc = "";
/* 190 */     rowCnt = bm.getRowCount();
/* 191 */     rowCnt1 = bm1.getRowCount();
/*     */     
/* 193 */     List invLst = new java.util.ArrayList();
/* 194 */     for (int row = 0; row < rowCnt; row++) {
/* 195 */       pk_invmandoc = (String)bm.getValueAt(row, "pk_invmandoc");
/* 196 */       invLst.add(pk_invmandoc);
/*     */     }
/*     */     
/* 199 */     for (int row = 0; row < rowCnt1; row++) {
/* 200 */       pk_invmandoc = (String)bm1.getValueAt(row, "pk_invmandoc");
/*     */       
/* 202 */       if (invLst.indexOf(pk_invmandoc) == -1) {
/* 203 */         invLst.add(pk_invmandoc);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 208 */     InvVO[] invVOs = getInvoInfoBYFormula().getInvParseWithPlanPriceAndPackType((String[])invLst.toArray(new String[0]), null, null, getCorpPrimaryKey(), true, true);
/*     */     
/* 210 */     invMap = new HashMap();
/* 211 */     for (InvVO vo : invVOs) {
/* 212 */       invMap.put(vo.getCinventoryid(), vo);
/*     */     }
/*     */     
/* 215 */     InvVO tmpInvVo = null;
/* 216 */     FreeVO voFree = null;
/* 217 */     for (int row = 0; row < rowCnt; row++) {
/* 218 */       pk_invmandoc = (String)bm.getValueAt(row, "pk_invmandoc");
/* 219 */       if (invMap.containsKey(pk_invmandoc)) {
/* 220 */         tmpInvVo = (InvVO)invMap.get(pk_invmandoc);
/* 221 */         voFree = tmpInvVo.getFreeItemVO();
/*     */         
/* 223 */         for (int idx = 1; idx <= 5; idx++) {
/* 224 */           tmpInvVo.setFreeItemValue("vfree" + idx, (String)bm.getValueAt(row, "vfree" + idx));
/*     */         }
/*     */         
/* 227 */         bm.setValueAt(tmpInvVo.getFreeItemVO().getWholeFreeItem(), row, "vfree0");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 232 */     for (int row = 0; row < rowCnt1; row++) {
/* 233 */       pk_invmandoc = (String)bm1.getValueAt(row, "pk_invmandoc");
/* 234 */       if (invMap.containsKey(pk_invmandoc)) {
/* 235 */         tmpInvVo = (InvVO)invMap.get(pk_invmandoc);
/* 236 */         voFree = tmpInvVo.getFreeItemVO();
/*     */         
/* 238 */         for (int idx = 1; idx <= 5; idx++) {
/* 239 */           tmpInvVo.setFreeItemValue("vfree" + idx, (String)bm1.getValueAt(row, "vfree" + idx));
/*     */         }
/*     */         
/* 242 */         bm1.setValueAt(tmpInvVo.getFreeItemVO().getWholeFreeItem(), row, "vfree0");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 247 */     getFreeItemCellRender().setRenderer("vfree0");
/*     */   }
/*     */   
/*     */   public void afterFreeItemEdit(BillEditEvent e) {
/*     */     try {
/* 252 */       String key = getBodyItemString("jzpm_cm_stocklist", e.getRow(), "pk_invmandoc");
/* 253 */       InvVO voInv = (InvVO)invMap.get(key);
/*     */       
/* 255 */       FreeVO voFree = getFreeItemRefPane().getFreeVO();
/* 256 */       BillModel bm = getBillCardPanel().getBillModel("jzpm_cm_stocklist");
/*     */       
/* 258 */       if ((bm.getValueAt(e.getRow(), "vfree0") == null) || ("".equals(bm.getValueAt(e.getRow(), "vfree0").toString())))
/*     */       {
/* 260 */         for (int i = 1; i <= 5; i++) {
/* 261 */           voFree.setAttributeValue("vfree" + i, null);
/*     */         }
/*     */       }
/*     */       
/* 265 */       for (int idx = 1; idx <= 5; idx++) {
/* 266 */         if (voFree != null) {
/* 267 */           bm.setValueAt(voFree.getAttributeValue("vfree" + idx), e.getRow(), "vfree" + idx);
/*     */         }
/*     */         else {
/* 270 */           bm.setValueAt(null, e.getRow(), "vfree" + idx);
/*     */         }
/*     */       }
/*     */       
/* 274 */       if (voInv != null) {
/* 275 */         voInv.setFreeItemVO(voFree);
/*     */       }
/*     */       
/* 278 */       invMap.put(key, voInv);
/*     */     } catch (Exception e2) {
/* 280 */       SCMEnv.error(e2);
/*     */     }
/*     */   }
/*     */   
/*     */   public void afterUpdate()
/*     */   {
/* 286 */     super.afterUpdate();
/*     */     
/* 288 */     initInvMap();
/*     */   }
/*     */   
/*     */   protected AbstractManageController createController()
/*     */   {
/* 293 */     return new ClientCtrl();
/*     */   }
/*     */   
/*     */   protected BusinessDelegator createBusinessDelegator()
/*     */   {
/* 298 */     return new BusinessDelegator();
/*     */   }
/*     */   
/*     */   protected nc.ui.trade.manage.ManageEventHandler createEventHandler()
/*     */   {
/* 303 */     return new EventHandler(this, getUIControl());
/*     */   }
/*     */   
/*     */   protected void initSelfData()
/*     */   {
/* 308 */     super.initSelfData();
/* 309 */     initInvclRef();
/* 310 */     initInvmandocRef();
/*     */   }
/*     */   
/*     */   protected void initContType()
/*     */   {
/* 315 */     UIRefPane refpane = (UIRefPane)getBillCardPanel().getHeadItem("pk_conttype").getComponent();
/*     */     
/* 317 */     if (contTypeModel == null) {
/* 318 */       contTypeModel = new ContTypeRefTreeModel(new int[] { 2 });
/*     */     }
/*     */     
/* 321 */     refpane.setNotLeafSelectedEnabled(false);
/* 322 */     refpane.setRefModel(contTypeModel);
/*     */   }
/*     */   
/*     */   private void initInvclRef() {
/* 326 */     BillItem invclItem = getBillCardPanel().getBodyItem("vinvclcode");
/* 327 */     if (invclItem != null) {
/* 328 */       ((UIRefPane)invclItem.getComponent()).setNotLeafSelectedEnabled(false);
/*     */       
/* 330 */       ((UIRefPane)invclItem.getComponent()).setMultiSelectedEnabled(true);
/*     */     }
/*     */   }
/*     */   
/*     */   private void initInvmandocRef()
/*     */   {
/* 336 */     BillItem invmanstockItem = getBillCardPanel().getBodyItem("jzpm_cm_stocklist", "vinvmandoccode");
/*     */     
/* 338 */     if (invmanstockItem != null) {
/* 339 */       UIRefPane refPane = (UIRefPane)invmanstockItem.getComponent();
/* 340 */       InvmandocDefaultRefModel infpanelModel = new InvmandocDefaultRefModel("存货档案");
/*     */       
/* 342 */       infpanelModel.addWherePart(" and bd_invmandoc.pk_corp = '" + getCorpPrimaryKey() + "'");
/* 343 */       refPane.setMultiSelectedEnabled(true);
/* 344 */       refPane.setTreeGridNodeMultiSelected(true);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void initPrivateButton()
/*     */   {
/* 350 */     super.initPrivateButton();
/* 351 */     initLineBtns();
/* 352 */     initImportBtn();
/*     */   }
/*     */   
/*     */   private void initLineBtns()
/*     */   {
/* 357 */     ButtonVO lineBtnVo = ButtonVOFactory.getInstance().build(10);
/*     */     
/* 359 */     lineBtnVo.setChildAry(new int[] { 11, 15, 12 });
/*     */     
/* 361 */     addPrivateButton(lineBtnVo);
/*     */   }
/*     */   
/*     */   private void initImportBtn()
/*     */   {
/* 366 */     ButtonVO firstDeductBtn = ButtonFactory.createButtonVO(726, "粘贴式导入", new int[] { 0, 1, 3 }, null, "粘贴式导入");
/*     */     
/* 368 */     addPrivateButton(firstDeductBtn);
/*     */   }
/*     */   
/*     */   protected ButtonVO[] initAssQryBtnVOs()
/*     */   {
/* 373 */     return new ButtonVO[] { ButtonFactory.getJZContLinkDoCond(null), ButtonFactory.createButtonVO(400, NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJTH5112615-000250"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJTH5112615-000250"), "单据联查"), new CustomRelaPB().getButtonVO() };
/*     */   }
/*     */   
/*     */   public void updateLineAndImportBtns(String currTableCode)
/*     */     throws Exception
/*     */   {
/* 379 */     if (isInfoSupply) {
/* 380 */       super.updateLineAndImportBtns(currTableCode);
/*     */     } else {
/* 382 */       ButtonObject lineBtn = getButtonManager().getButton(10);
/* 383 */       ButtonObject ImportBtn = getButtonManager().getButton(726);
/*     */       
/* 385 */       lineBtn.setChildButtonGroup(getBtnObj(new int[] { 11, 12, 15 }));
/*     */       
/* 387 */       if ("jzpm_cm_stocklist_b".equals(currTableCode)) {
/* 388 */         lineBtn.setEnabled(false);
/* 389 */         ImportBtn.setEnabled(false);
/*     */       } else {
/* 391 */         lineBtn.setEnabled(true);
/* 392 */         ImportBtn.setEnabled(true);
/*     */       }
/* 394 */       updateButtonUI();
/*     */     }
/*     */   }
/*     */   
/*     */   public void adjustBodyColor()
/*     */   {
/* 400 */     String tablecode = getBillCardPanel().getCurrentBodyTableCode();
/* 401 */     if ("jzpm_cm_stocklist".equals(getBillCardPanel().getCurrentBodyTableCode()))
/*     */     {
/* 403 */       StockLineData stocklinedata = new StockLineData();
/* 404 */       stocklinedata.setManageUI(this);
/* 405 */       stocklinedata.setTableCode(tablecode);
/* 406 */       SubLineColorCtrl linecolor = new SubLineColorCtrl(stocklinedata);
/* 407 */       linecolor.setAllLineColor();
/*     */     }
/*     */   }
/*     */   
/*     */   public boolean beforeEdit(BillEditEvent e)
/*     */   {
/* 413 */     String key = e.getKey();
/* 414 */     int row = e.getRow();
/* 415 */     if (key.equalsIgnoreCase("define_mea")) {
/* 416 */       String invmandoc = getBillCardPanel().getBodyValueAt(row, "pk_invmandoc") == null ? null : getBillCardPanel().getBodyValueAt(row, "pk_invmandoc").toString();
/*     */       
/* 418 */       String contmeasdoc = getBillCardPanel().getBodyValueAt(row, "pk_contmeasdoc") == null ? null : getBillCardPanel().getBodyValueAt(row, "pk_contmeasdoc").toString();
/*     */       
/* 420 */       oldContmeasdoc = contmeasdoc;
/* 421 */       beforeEditMeasdoc(invmandoc, key, null);
/* 422 */     } else if ((key.equals("vinvclcode")) || (key.equals("vinvmandoccode"))) {
/* 423 */       String pk_conttype = (String)getBillCardPanel().getHeadItem("pk_conttype").getValueObject();
/* 424 */       if ((pk_conttype == null) || (pk_conttype.length() == 0)) {
/* 425 */         MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000001"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000002"));
/*     */         
/* 427 */         return false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 432 */     InvVO voInv = null;
/* 433 */     String invKey = null;
/* 434 */     if ("vfree0".equals(key)) {
/* 435 */       invKey = getBodyItemString("jzpm_cm_stocklist", e.getRow(), "pk_invmandoc");
/* 436 */       if ((invKey != null) && (invMap.containsKey(invKey))) {
/* 437 */         voInv = (InvVO)invMap.get(invKey);
/*     */         
/* 439 */         if ((voInv.getIsFreeItemMgt() == null) || (voInv.getIsFreeItemMgt().intValue() != 1))
/*     */         {
/* 441 */           return false; }
/* 442 */         if (voInv.getFreeItemVO() != null) {
/* 443 */           FreeVO voFree = voInv.getFreeItemVO();
/* 444 */           if (voFree != null) {
/* 445 */             for (int idx = 1; idx <= 5; idx++) {
/* 446 */               voInv.setFreeItemValue("vfree" + idx, (String)getBillCardPanel().getBodyValueAt(e.getRow(), "vfree" + idx));
/*     */             }
/*     */           }
/*     */           
/*     */ 
/* 451 */           getFreeItemRefPane().setFreeItemParam(voInv);
/* 452 */           return true;
/*     */         }
/* 454 */         return false;
/*     */       }
/*     */       
/* 457 */       return false;
/*     */     }
/*     */     
/* 460 */     return super.beforeEdit(e);
/*     */   }
/*     */   
/*     */   private void beforeEditMeasdoc(String invmandoc, String key, String vmanmeasdoc)
/*     */   {
/* 465 */     UIRefPane manmeasdocRefPane = null;
/* 466 */     if ((vmanmeasdoc != null) && (!"".equals(vmanmeasdoc))) {
/* 467 */       manmeasdocRefPane = (UIRefPane)getBillCardPanel().getBillModel().getItemByKey(vmanmeasdoc).getComponent();
/*     */     }
/*     */     
/* 470 */     UIRefPane refPane = (UIRefPane)getBillCardPanel().getBillModel().getItemByKey(key).getComponent();
/*     */     
/* 472 */     if (invmandoc == null) {
/* 473 */       MeasdocDefaultRefModel measdocRef = new MeasdocDefaultRefModel(NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000003"));
/*     */       
/* 475 */       refPane.setRefModel(measdocRef);
/* 476 */       if (manmeasdocRefPane != null) {
/* 477 */         manmeasdocRefPane.setRefModel(measdocRef);
/*     */       }
/* 479 */       return;
/*     */     }
/* 481 */     AssistUnitRefModel refModel = new AssistUnitRefModel(invmandoc);
/* 482 */     refPane.setRefModel(refModel);
/* 483 */     if (manmeasdocRefPane != null) {
/* 484 */       manmeasdocRefPane.setRefModel(refModel);
/*     */     }
/*     */   }
/*     */   
/*     */   public void afterEditHead(BillEditEvent e) {
/* 489 */     String key = e.getKey();
/* 490 */     if ("pk_conttype".equals(key)) {
/*     */       try {
/* 492 */         setSCMConttype();
/* 493 */         if (getBilltype() != null) {
/* 494 */           if (!billtype.equals("99F3")) {
/* 495 */             checkbody(e);
/* 496 */             setBodyInvItemEditable();
/*     */           }
/*     */         } else {
/* 499 */           checkbody(e);
/* 500 */           setBodyInvItemEditable();
/*     */         }
/*     */       } catch (BusinessException e1) {
/* 503 */         Logger.error(e1);
/*     */       }
/*     */     }
/* 506 */     super.afterEditHead(e);
/*     */   }
/*     */   
/*     */   private void checkbody(BillEditEvent e)
/*     */     throws BusinessException
/*     */   {
/* 512 */     String pk_ct_type = (String)getBillCardPanel().getHeadItem("pk_ct_type").getValueObject();
/* 513 */     String pk_corp = (String)getBillCardPanel().getHeadItem("pk_corp").getValueObject();
/* 514 */     int invctlStyle = -1;
/* 515 */     if ((pk_ct_type == null) || (pk_ct_type.length() == 0)) {
/* 516 */       return;
/*     */     }
/* 518 */     String sql = "select pk_ct_type,ninvctlstyle from ct_type where pk_ct_type = '" + pk_ct_type + "' and pk_corp = '" + pk_corp + "' and isnull(dr,0)=0 ";
/* 519 */     Vector vec = JZPMProxy.getIJZPMPubBusi().executeQuerySQL(sql);
/* 520 */     if ((vec != null) && (vec.size() > 0)) {
/* 521 */       Vector newvec = (Vector)vec.get(0);
/* 522 */       invctlStyle = ((Integer)newvec.get(1)).intValue();
/*     */     }
/* 524 */     CmStocklistVO[] bodyvos = (CmStocklistVO[])getBillCardPanel().getBillModel("jzpm_cm_stocklist").getBodyValueVOs(CmStocklistVO.class.getName());
/* 525 */     boolean invclnotnull = false;
/* 526 */     boolean invnotnull = false;
/* 527 */     if ((bodyvos != null) && (bodyvos.length > 0)) {
/* 528 */       for (CmStocklistVO vo : bodyvos) {
/* 529 */         if ((vo.getPk_invcl() != null) && (vo.getPk_invcl().length() > 0)) {
/* 530 */           invclnotnull = true;
/*     */         }
/* 532 */         if ((vo.getPk_invbasdoc() != null) && (vo.getPk_invbasdoc().length() > 0)) {
/* 533 */           invnotnull = true;
/*     */         }
/*     */       }
/*     */     }
/* 537 */     if (invctlStyle == 0) {
/* 538 */       if (invclnotnull) {
/* 539 */         MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000001"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000016"));
/*     */         
/* 541 */         getBillCardPanel().setHeadItem("pk_conttype", null);
/*     */       }
/*     */     }
/* 544 */     else if ((invctlStyle == 1) && 
/* 545 */       (invnotnull)) {
/* 546 */       MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000001"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000017"));
/*     */       
/* 548 */       getBillCardPanel().setHeadItem("pk_conttype", null);
/*     */     }
/*     */   }
/*     */   
/*     */   public void setSCMConttype()
/*     */     throws BusinessException
/*     */   {
/* 555 */     Object conttype = getBillCardPanel().getHeadItem("pk_conttype").getValueObject();
/*     */     
/* 557 */     if ((conttype == null) || (conttype.toString().length() == 0))
/* 558 */       return;
/* 559 */     CmJzscmctrefVO[] trvo = (CmJzscmctrefVO[])nc.ui.trade.business.HYPubBO_Client.queryByCondition(CmJzscmctrefVO.class, " isnull(dr,0)=0 and pk_conttype='" + conttype + "' and " + "pk_corp='" + _getCorp().getPrimaryKey() + "' ");
/*     */     
/* 561 */     if ((trvo == null) || (trvo.length == 0)) {
/* 562 */       MessageDialog.showHintDlg(this, NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000001"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000004"));
/*     */       
/* 564 */       getBillCardPanel().setHeadItem("pk_conttype", null);
/*     */     } else {
/* 566 */       getBillCardPanel().setHeadItem("pk_ct_type", trvo[0].getPk_ct_type());
/*     */     }
/*     */   }
/*     */   
/*     */   public void afterEditBody(BillEditEvent e)
/*     */   {
/* 572 */     super.afterEditBody(e);
/* 573 */     String key = e.getKey();
/* 574 */     int row = e.getRow();
/* 575 */     if ("jzpm_cm_stocklist".equalsIgnoreCase(getCurrTabCode())) {
/* 576 */       if ("vinvclcode".equalsIgnoreCase(key)) {
/* 577 */         getBillCardPanel().setBodyValueAt(null, e.getRow(), "vinvmandoccode");
/*     */         
/* 579 */         getBillCardPanel().setBodyValueAt(null, e.getRow(), "pk_invbasdoc");
/*     */         
/* 581 */         getBillCardPanel().setBodyValueAt(null, e.getRow(), "pk_invmandoc");
/*     */         
/* 583 */         getBillCardPanel().setBodyValueAt(null, e.getRow(), "vinvmandocname");
/*     */         
/* 585 */         getBillCardPanel().setBodyValueAt(null, e.getRow(), "define_mea");
/*     */         
/* 587 */         getBillCardPanel().setBodyValueAt(null, e.getRow(), "define_shape");
/*     */         
/* 589 */         getBillCardPanel().setBodyValueAt(null, e.getRow(), "define_size");
/*     */         
/* 591 */         getBillCardPanel().setBodyValueAt(null, e.getRow(), "define_type");
/*     */         
/* 593 */         multiSelectedAddLine(e);
/* 594 */       } else if ("vinvmandoccode".equalsIgnoreCase(key)) {
/* 595 */         multiSelectedAddLine(e);
/*     */       }
/* 597 */       else if (("ncontnum".equalsIgnoreCase(key)) || ("nprice".equalsIgnoreCase(key)))
/*     */       {
/*     */ 
/* 600 */         Object vreserve9 = getBillCardPanel().getBodyValueAt(row, "vreserve9");
/* 601 */         Object ncontnum = e.getValue();
/* 602 */         if ((vreserve9 != null) && (ncontnum != null) && ("ncontnum".equalsIgnoreCase(key))) {
/* 603 */           UFDouble plan_num = new UFDouble(Double.parseDouble(vreserve9.toString()));
/* 604 */           UFDouble ncount_num = new UFDouble(Double.parseDouble(ncontnum.toString()));
/* 605 */           if (ncount_num.sub(plan_num).compareTo(UFDouble.ZERO_DBL) > 0) {
/* 606 */             MessageDialog.showHintDlg(this, "错误", "合同数量不能大于计划数量");
/* 607 */             getBillCardPanel().setBodyValueAt("", row, "ncontnum");
ncount_num = UFDouble.ZERO_DBL;
/*     */           }
Object vlastbillrowid = getBillCardPanel().getBodyValueAt(row, "vlastbillrowid");
Object bvopk = getBillCardPanel().getBodyValueAt(row, "pk_stocklist");
if(vlastbillrowid!=null && bvopk!=null){
	IUAPQueryBS query = (IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class);
	try {
		MtCtplanBSumVO vo = (MtCtplanBSumVO)query.retrieveByPK(MtCtplanBSumVO.class, vlastbillrowid.toString());
		CmStocklistVO cmvo = (CmStocklistVO)query.retrieveByPK(CmStocklistVO.class, bvopk.toString());
		UFDouble subNum = ncount_num.sub(cmvo.getNcontnum());
		UFDouble usedNum = (vo.getUsednum() == null ? UFDouble.ZERO_DBL: vo.getUsednum()).add(subNum);
		if(vo.getNnum().compareTo(usedNum)<0){
			 MessageDialog.showHintDlg(this, "错误", "消耗材料汇总计划中总控数量不足");
             getBillCardPanel().setBodyValueAt(cmvo.getNcontnum(), row, "ncontnum");
             ncount_num = cmvo.getNcontnum();
		}
	} catch (BusinessException e1) {
		e1.printStackTrace();
	}
}
/*     */         }
/* 610 */         contNtotaloriginmny(row, getCurrTabCode());
					//hmx
					afterTaxRateEdit(e);
/* 611 */       } else if (key.equalsIgnoreCase("define_mea"))
/*     */       {
/* 613 */         count(row, "pk_contmeasdoc", "pk_invmandoc", "ncontnum", "nprice", null);
/*     */       }
/* 615 */       else if (key.equals("vfree0"))
/*     */       {
/* 617 */         afterFreeItemEdit(e);
/*     */       }else if(key.equals("vdef9")){
					afterTaxRateEdit(e);
				}
//合同含税金额ncontsignoriginmny 税额vdef7 合同不含税金额 vdef8 
//不含税单价vdef7  税额vdef8    税率vdef9   
/*     */     }
/*     */   }
/*     */   
private void afterTaxRateEdit(BillEditEvent e) {
	 int row = e.getRow();
	 Object ntotaloriginmny_com = getBillCardPanel().getBodyValueAt(row, "ntotaloriginmny");//价税合计
	 Object taxRate_com = getBillCardPanel().getBodyValueAt(row, "vdef9");
	 // Object taxRate_com = e.getValue();
	 if(taxRate_com==null || "".equals(taxRate_com) || "0".equals(taxRate_com)){
		 getBillCardPanel().setBodyValueAt(null, row, "vdef7", getCurrTabCode());
		 getBillCardPanel().setBodyValueAt(null, row, "vdef8", getCurrTabCode());
		 getBillCardPanel().setBodyValueAt(null, row, "vdef9", getCurrTabCode());
	 }
	 else if(ntotaloriginmny_com!=null && taxRate_com!=null && !"".equals(taxRate_com)){
		 UFDouble ntotaloriginmny = new UFDouble(Double.parseDouble(ntotaloriginmny_com.toString()));
		 UFDouble taxRate = new UFDouble(Double.parseDouble(taxRate_com.toString()));
		 UFDouble computeTaxRate = SafeCompute.div(taxRate, new UFDouble(100));
		 UFDouble ntotaloriginmnyNoTax = SafeCompute.div(ntotaloriginmny, SafeCompute.add(new UFDouble(1.0), computeTaxRate)).setScale(2, UFDouble.ROUND_HALF_UP);
		 UFDouble tax = SafeCompute.sub(ntotaloriginmny, ntotaloriginmnyNoTax);
		 getBillCardPanel().setBodyValueAt(tax, row, "vdef8", getCurrTabCode());
	 }else{
		 
	 }
	 int rowcount = getBillCardPanel().getBillModel(getCurrTabCode()).getRowCount();
     if (rowcount > 0) {
      UFDouble tax = new UFDouble(0);
      for (int i = 0; i < rowcount; i++) {
        UFDouble TaxItem = getBodyItemDouble(getCurrTabCode(), i, "vdef8");
        tax = tax.add(TaxItem == null ?UFDouble.ZERO_DBL: TaxItem);
       }
      getBillCardPanel().setHeadItem("vdef7", tax);
      Object nctmny=getBillCardPanel().getHeadItem("ncontsignoriginmny").getValueObject();
      if(nctmny!=null){
    	  UFDouble ncontsignoriginmny = new UFDouble(Double.parseDouble(nctmny.toString()));
    	  getBillCardPanel().setHeadItem("vdef8", SafeCompute.sub(ncontsignoriginmny, tax));
      }
    }
}
/*     */   public String[] getTableCodes() {
/* 623 */     return CMStockContractAggVO.m_tableCodes;
/*     */   }
/*     */   
/*     */   private void count(int row, String pk_contmeasdoc, String pk_invmandoc, String contnum, String price, String wasteprice)
/*     */   {
/* 628 */     String contmeasdoc = getBillCardPanel().getBodyValueAt(row, pk_contmeasdoc) == null ? null : getBillCardPanel().getBodyValueAt(row, pk_contmeasdoc).toString();
/*     */     
/* 630 */     String invmandoc = getBillCardPanel().getBodyValueAt(row, pk_invmandoc) == null ? null : getBillCardPanel().getBodyValueAt(row, pk_invmandoc).toString();
/*     */     
/* 632 */     UFDouble n = null;
/*     */     try {
/* 634 */       n = nc.ui.jzpm.mtpub.ClientMeasUtil.getNRateByManPk(invmandoc, oldContmeasdoc, contmeasdoc);
/*     */     }
/*     */     catch (BusinessException e1) {
/* 637 */       Logger.error("取得计量换算率错误！", e1);
/*     */     }
/* 639 */     UFDouble ncontnum = getBillCardPanel().getBodyValueAt(row, contnum) == null ? new UFDouble(0) : new UFDouble(getBillCardPanel().getBodyValueAt(row, contnum).toString());
/*     */     
/* 641 */     UFDouble nprice = getBillCardPanel().getBodyValueAt(row, price) == null ? new UFDouble(0) : new UFDouble(getBillCardPanel().getBodyValueAt(row, price).toString());
/*     */     
/* 643 */     if (n != null) {
/* 644 */       getBillCardPanel().setBodyValueAt(SafeCompute.multiply(n, ncontnum), row, contnum);
/*     */       
/* 646 */       getBillCardPanel().setBodyValueAt(SafeCompute.div(nprice, n), row, price);
/*     */       
/* 648 */       if (wasteprice != null) {
/* 649 */         UFDouble nwasteprice = getBillCardPanel().getBodyValueAt(row, wasteprice) == null ? new UFDouble(0) : new UFDouble(getBillCardPanel().getBodyValueAt(row, wasteprice).toString());
/*     */         
/* 651 */         getBillCardPanel().setBodyValueAt(SafeCompute.div(nwasteprice, n), row, wasteprice);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void contNtotaloriginmny(int row, String tabCode)
/*     */   {
/* 658 */     UFDouble num = getBodyItemDouble(tabCode, row, "ncontnum");
/* 659 */     UFDouble price = getBodyItemDouble(tabCode, row, "nprice");
/* 660 */     getBillCardPanel().setBodyValueAt(SafeCompute.multiply(num, price), row, "ntotaloriginmny", tabCode);
/*     */     
/* 662 */     getBillCardPanel().setBodyValueAt(getBaseMny(getBodyItemDouble(tabCode, row, "ntotaloriginmny")), row, "ntotalbasemny", tabCode);
/*     */     
/*     */ 
/* 665 */     int rowcount = getBillCardPanel().getBillModel(tabCode).getRowCount();
/* 666 */     if (rowcount > 0) {
/* 667 */       UFDouble ncontsignoriginmny = new UFDouble(0);
/* 668 */       for (int i = 0; i < rowcount; i++) {
/* 669 */         UFDouble ntotaloriginmny = getBodyItemDouble(tabCode, i, "ntotaloriginmny");
/* 670 */         ncontsignoriginmny = ncontsignoriginmny.add(ntotaloriginmny == null ? new UFDouble(0) : ntotaloriginmny);
/*     */       }
/* 672 */       getBillCardPanel().setHeadItem("ncontsignoriginmny", ncontsignoriginmny);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void multiSelectedAddLine(BillEditEvent e)
/*     */   {
/* 679 */     setInvclMultiSelect(e);
/* 680 */     if ("jzpm_cm_stocklist".equalsIgnoreCase(getCurrTabCode()))
/*     */     {
/* 682 */       if (e.getKey().equalsIgnoreCase("vinvmandoccode")) {
/* 683 */         UIRefPane refPane = (UIRefPane)getBillCardPanel().getBillModel().getItemByKey(e.getKey()).getComponent();
/*     */         
/* 685 */         String[] refPks = refPane.getRefPKs();
/* 686 */         String[] refCodes = refPane.getRefCodes();
/* 687 */         if ((refPks == null) || (refCodes == null) || (refPks.length == 0) || (refCodes.length == 0))
/*     */         {
/* 689 */           return;
/*     */         }
/* 691 */         for (int i = 0; i < refPks.length; i++) {
/* 692 */           if (i > 0) {
/* 693 */             getBillCardPanel().insertLine();
/*     */           }
/* 695 */           getBillCardPanel().setBodyValueAt(refPks[i], e.getRow(), "pk_invmandoc");
/*     */           
/* 697 */           getBillCardPanel().setBodyValueAt(refCodes[i], e.getRow(), "vinvmandoccode");
/*     */           
/* 699 */           getBillCardPanel().execBodyFormula(e.getRow(), "vinvmandoccode");
/* 700 */           afterEditInv(e);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public CMContractDelegator getDelegator()
/*     */   {
/* 708 */     return (BusinessDelegator)getBusiDelegator();
/*     */   }
/*     */   
/*     */   public void stateChanged(ChangeEvent e) {
/* 712 */     String selTabCode = ((BillTabbedPane)e.getSource()).getSelectedTableCode();
/*     */     
/* 714 */     if (isAddOrEdit()) {
/*     */       try {
/* 716 */         if ((getBilltype() == null) || (!getBilltype().equals("99F3"))) {
/* 717 */           updateLineAndImportBtns(selTabCode);
/*     */         }
/*     */       } catch (Exception e1) {
/* 720 */         Logger.error(e1.getMessage(), e1);
/*     */       }
/*     */     }
/* 723 */     super.stateChanged(e);
/*     */   }
/*     */   
/*     */   public void loadBodyData() throws Exception
/*     */   {
/* 728 */     super.loadBodyData();
/*     */     
/* 730 */     initInvMap();
/*     */   }
/*     */   
/*     */   protected void initRefBtn() {
/* 734 */     int[] billOperate = { 4, 2 };
/* 735 */     ButtonVO addRefContBtnVO = ButtonFactory.getCmRefBtn(billOperate);
/* 736 */     addPrivateButton(addRefContBtnVO);
/* 737 */     ConsignorBtnVO confbtn = new ConsignorBtnVO();
/* 738 */     ButtonVO addRefconfBtnVO = confbtn.getButtonVO();
/* 739 */     addPrivateButton(addRefconfBtnVO);
/* 740 */     RequirePlanBtnVO reqplanvo = new RequirePlanBtnVO();
/* 741 */     ButtonVO addRefreqBtnVO = reqplanvo.getButtonVO();
/* 742 */     addPrivateButton(addRefreqBtnVO);
/*     */     
/*     */ 
/* 745 */     RequireTotalPlanBtnVO reqtotalplanvo = new RequireTotalPlanBtnVO();
/* 746 */     ButtonVO addRefreqtotalBtnVO = reqtotalplanvo.getButtonVO();
/* 747 */     addPrivateButton(addRefreqtotalBtnVO);
/*     */     
/*     */ 
/* 750 */     ButtonVO refBtnvo = ButtonVOFactory.getInstance().build(9);
/* 751 */     refBtnvo.setChildAry(new int[] { 723, 720, 770, 771 });
/*     */     
/* 753 */     addPrivateButton(refBtnvo);
/*     */   }
/*     */   
/*     */   protected void setBodyInvItemEditable()
/*     */     throws BusinessException
/*     */   {
/* 759 */     String ct_type = getHeadItemString("pk_ct_type");
/* 760 */     if ((ct_type == null) || (ct_type.toString().length() == 0)) {
/* 761 */       return;
/*     */     }
/*     */     
/* 764 */     Object[] o = queryColumnValue("ct_type", "pk_ct_type", "ninvctlstyle", new String[] { ct_type });
/*     */     
/* 766 */     if ((o != null) && (o.length > 0)) {
/* 767 */       int controltype = new Integer(o[0].toString()).intValue();
/* 768 */       if (controltype == 0) {
/* 769 */         getBillCardPanel().getBodyItem("jzpm_cm_stocklist", "vinvclcode").setEnabled(false);
/*     */         
/* 771 */         getBillCardPanel().getBodyItem("jzpm_cm_stocklist", "vinvmandoccode").setEnabled(true);
/*     */       }
/* 773 */       else if (controltype == 1) {
/* 774 */         getBillCardPanel().getBodyItem("jzpm_cm_stocklist", "vinvclcode").setEnabled(true);
/*     */         
/* 776 */         getBillCardPanel().getBodyItem("jzpm_cm_stocklist", "vinvmandoccode").setEnabled(false);
/*     */       }
/*     */       else
/*     */       {
/* 780 */         getBillCardPanel().getBodyItem("jzpm_cm_stocklist", "vinvclcode").setEnabled(false);
/*     */         
/* 782 */         getBillCardPanel().getBodyItem("jzpm_cm_stocklist", "vinvmandoccode").setEnabled(false);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   protected Object[] queryColumnValue(String tableName, String fieldpk, String showField, String[] pks)
/*     */   {
/* 789 */     FieldColumnVO colvo = new FieldColumnVO(tableName, fieldpk, showField, pks);
/*     */     
/* 791 */     Object[] o = nc.ui.jz.pub.CacheTool.findColumnValue(colvo);
/* 792 */     return o;
/*     */   }
/*     */   
/*     */   public String[] getHeadBaseItems()
/*     */   {
/* 797 */     return new String[] { "ncontsignbasemny", "ncurrcontbasemny", "nstandbybasemny", "nbailbasemny", "nprepaybasemny", "nsettlesumbasemny", "nshouldsumbasemny", "nactualsumbasemny", "nprepaylimitbasemny", "nsettleappliedbasemny", "nsettlepaidbasemny", "nmatesettlesumbasemny", "nmatesettleappliedbasemny", "nmatesettlepaidbasemny", "ndeductsumbasemny", "napplysumbasemny" };
/*     */   }
/*     */   
/*     */   public String[] getHeadOriginItems()
/*     */   {
/* 802 */     return new String[] { "ncontsignoriginmny", "ncurrcontoriginmny", "nstandbyoriginmny", "nbailoriginmny", "nprepayoriginmny", "nsettlesumoriginmny", "nshouldsumoriginmny", "nactualsumoriginmny", "nprepaylimitoriginmny", "nsettleappliedoriginmny", "nsettlepaidoriginmny", "nmatesettlesumoriginmny", "nmatesettleappliedoriginmny", "nmatesettlepaidoriginmny", "ndeductsumoriginmny", "napplysumoriginmny" };
/*     */   }
/*     */   
/*     */   public HashMap<String, String[][]> getBodyOthDigitItems()
/*     */   {
/* 807 */     if (map == null) {
/* 808 */       map = new HashMap();
/* 809 */       map.put("jzpm_cm_stocklist", new String[][] { { "ncontnum", "nprice" }, { getSysInitNumCode(), getSysInitPriceCode() } });
/*     */       
/* 811 */       map.put("jzpm_cm_stocklist_b", new String[][] { { "ncontnum", "nprice" }, { getSysInitNumCode(), getSysInitPriceCode() } });
/*     */     }
/*     */     
/* 814 */     return map;
/*     */   }
/*     */   
/*     */   public String[][] getBodyOriginItems()
/*     */   {
/* 819 */     return new String[][] { { "ntotaloriginmny" }, { "ntotaloriginmny" } };
/*     */   }
/*     */   
/*     */   public String[][] getBodyBaseItems()
/*     */   {
/* 824 */     return new String[][] { { "ntotaloriginmny" }, { "ntotaloriginmny" } };
/*     */   }
/*     */   
/*     */   public void updateButtonUI()
/*     */     throws Exception
/*     */   {
/* 830 */     super.updateButtonUI();
/*     */   }
/*     */   
/*     */   protected int[] getIConttype()
/*     */   {
/* 835 */     return new int[] { 2 };
/*     */   }
/*     */ }

/* Location:           E:\hh\nchome\modules\jzpm\client\classes
 * Qualified Name:     nc.ui.jzpm.cm2615.ClientUI
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */