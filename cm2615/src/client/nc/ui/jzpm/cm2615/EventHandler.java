//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.cm2615;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import nc.bs.framework.common.NCLocator;
import nc.bs.logging.Logger;
import nc.itf.uap.IUAPQueryBS;
import nc.itf.uap.IVOPersistence;
import nc.ui.jz.pub.excel2.importer.NCExcelImportAdapter;
import nc.ui.jz.pub.excelimporttool.ImportExcelDialog;
import nc.ui.jz.pub.tool.CurrencyHelper;
import nc.ui.jzpm.cm901025.LinkContCondData;
import nc.ui.jzpm.cmpub.contim.ImportExcelBase;
import nc.ui.jzpm.cmpub.contract.CMEventHandler;
import nc.ui.jzpm.core.pub.ref.ContTypeRefTreeModel;
import nc.ui.jzpm.inpub.change.ui.CodeTreeAfterEditUtil;
import nc.ui.pub.ButtonObject;
import nc.ui.pub.beans.MessageDialog;
import nc.ui.pub.beans.UIRefPane;
import nc.ui.pub.beans.UITable;
import nc.ui.pub.bill.BillItem;
import nc.ui.pub.bill.BillModel;
import nc.ui.pub.pf.PfUtilClient;
import nc.ui.trade.bill.RefBillTypeChangeEvent;
import nc.ui.trade.controller.IControllerBase;
import nc.ui.trade.manage.BillManageUI;
import nc.ui.uap.sf.SFClientUtil;
import nc.vo.bd.invdoc.InvbasdocVO;
import nc.vo.jz.pub.JzInvocationParam;
import nc.vo.jz.pub.SafeCompute;
import nc.vo.jz.pub.pp0507.TeConfirmBVO;
import nc.vo.jz.pub.pp0507.TeConfirmMateVO;
import nc.vo.jzfdc.pub.util.VOTool;
import nc.vo.jzpm.cmpub.contract.CMContractAggVO;
import nc.vo.jzpm.cmpub.contract.CmContractVO;
import nc.vo.jzpm.cmpub.contract.CmStocklistVO;
import nc.vo.jzpm.cmpub.pp.TeConfirmDatas;
import nc.vo.jzpm.mt100505.MtCtplanBSumVO;
import nc.vo.jzpm.pub.JZPMProxy;
import nc.vo.jzpm.pub.cmpub.IContract;
import nc.vo.ml.NCLangRes4VoTransl;
import nc.vo.pub.AggregatedValueObject;
import nc.vo.pub.BusinessException;
import nc.vo.pub.CircularlyAccessibleValueObject;
import nc.vo.pub.SuperVO;
import nc.vo.pub.lang.UFBoolean;
import nc.vo.pub.lang.UFDate;
import nc.vo.pub.lang.UFDateTime;
import nc.vo.pub.lang.UFDouble;
import nc.vo.trade.pub.HYBillVO;
import nc.vo.trade.pub.IExAggVO;

public class EventHandler extends CMEventHandler {
    private int scm_conttypectrlmode = 0;
    private LinkContCondData m_linkContCondData = null;

    public EventHandler(BillManageUI billUI, IControllerBase control) {
        super(billUI, control);
    }

    private ClientUI getClientUI() {
        return (ClientUI)this.getBillUI();
    }

    public void onBillRef() throws Exception {
        this.getClientUI().setCodeTreeAfterEditUtil((CodeTreeAfterEditUtil)null);
        this.getClientUI().getContBaseInfoQryDlg().showModal();
        this.getClientUI().getContBaseInfoQryDlg().refreshData();
        if (this.getClientUI().getContBaseInfoQryDlg().isCloseOK()) {
            String pk_contract = this.getClientUI().getContBaseInfoQryDlg().getPkContract();
            CmContractVO contvo = this.getContVOByPK(pk_contract);
            contvo.setDapprovedate((UFDate)null);
            contvo.setDmakedate((UFDate)null);
            contvo.setPk_billtype(this.getBillCardPanelWrapper().getBillCardPanel().getBillType());
            contvo.setBmaintaindetail(new UFBoolean(true));
            String contname = contvo.getVname();
            if (contname != null && contname.length() > 60) {
                contvo.setVname(contname.substring(0, 60));
            }

            super.onBoAdd(new ButtonObject(""));
            CMContractAggVO aggvo = (CMContractAggVO)this.getClientUI().getVOFromUI();
            aggvo.setParentVO(contvo);
            this.getClientUI().setMaxDigitBeforeUpdate();
            this.getBillCardPanelWrapper().setCardData(aggvo);
            this.getClientUI().setDefaultData();
            this.getClientUI().updateBillShowDigits();
            this.getBillCardPanelWrapper().getBillCardPanel().execHeadLoadFormulas();
            this.getClientUI().isInfoSupply = false;
            this.getClientUI().updateLineAndImportBtns(this.getClientUI().getCurrTabCode());
            this.setButtonValue(9);
            this.getClientUI().setBodyInvItemEditable();
        }
    }

    public String[] getAddEditFields() {
        return new String[]{"vbillno", "idisputesolvemode", "vdisputesolveunit", "vdutyperson", "vcontract", "pk_contpricemode", "vaddress", "vfulfilsite", "vrealcontno", "vpigeonholecode"};
    }

    public void onBoAdd(ButtonObject bo) throws Exception {
        this.getClientUI().isInfoSupply = false;
        UIRefPane refpane = (UIRefPane)this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_conttype").getComponent();
        refpane.setRefModel(new ContTypeRefTreeModel(this.getContType()));
        String[] items = new String[]{"vinvclcode", "vinvmandoccode", "define_mea", "ncontnum"};
        String[] var7 = items;
        int var5 = 0;

        for(int var6 = items.length; var5 < var6; ++var5) {
            String item = var7[var5];
            this.getBillCardPanelWrapper().getBillCardPanel().getBodyItem("jzpm_cm_stocklist", item).setEnabled(true);
        }

        super.onBoAdd(bo);
    }

    protected void onBoElse(int intBtn) throws Exception {
        super.onBoElse(intBtn);
        if (631 == intBtn) {
            this.doQueryContDoCond();
        } else if (726 == intBtn) {
            this.onImportExcelData();
        } else if (770 == intBtn) {
            this.onReqPlanRef();
        } else if (771 == intBtn) {
            this.onReqTotalPlanRef();
        }

    }

    private void onReqTotalPlanRef() throws Exception {
        ButtonObject btn = this.getButtonManager().getButton(771);
        btn.setTag("99F1:");
        this.onBoBusiTypeAdd(btn, (String)null);
        this.getClientUI().initInvMap();
        btn.setTag(String.valueOf(771));
        if (PfUtilClient.isCloseOK()) {
            ;
        }
    }

    private void onReqPlanRef() throws Exception {
        ButtonObject btn = this.getButtonManager().getButton(770);
        btn.setTag("99F3:");
        this.onBoBusiTypeAdd(btn, (String)null);
        this.getClientUI().initInvMap();
        btn.setTag(String.valueOf(770));
        if (PfUtilClient.isCloseOK()) {
            ;
        }
    }

    protected void doContInfoSupply() throws Exception {
        super.doContInfoSupply();
        this.updateStateBtn();
    }

    protected void doBillNoMaintain() throws Exception {
        super.doBillNoMaintain();
        this.updateStateBtn();
    }

    private void updateStateBtn() throws Exception {
        this.getClientUI().getButtonManager().getButton(10).setEnabled(false);
        this.getClientUI().getButtonManager().getButton(726).setEnabled(false);
        this.getClientUI().updateButtonUI();
    }

    private void onBoBusiTypeAdd(ButtonObject bo, String sourceBillId) throws Exception {
        this.getBusiDelegator().childButtonClicked(bo, this._getCorp().getPrimaryKey(), this.getBillUI()._getModuleCode(), this._getOperator(), this.getUIController().getBillType(), this.getBillUI(), this.getBillUI().getUserObject(), sourceBillId);
        if (PfUtilClient.makeFlag) {
            this.getBillUI().setCardUIState();
            this.getBillUI().setBillOperate(1);
        } else if (PfUtilClient.isCloseOK()) {
            if (this.getBillBusiListener() != null) {
                String tmpString = bo.getTag();
                int findIndex = tmpString.indexOf(":");
                String newtype = tmpString.substring(0, findIndex);
                RefBillTypeChangeEvent e = new RefBillTypeChangeEvent(this, (String)null, newtype);
                this.getBillBusiListener().refBillTypeChange(e);
            }

            this.getBillUI().setCardUIState();
            AggregatedValueObject aggvo = PfUtilClient.getRetOldVo();
            if (aggvo == null) {
                throw new BusinessException("未选择参照单据!");
            }

            this.getBillUI().setBillOperate(3);
            this.getClientUI().setMaxDigitBeforeUpdate();
            this.getBillCardPanelWrapper().setCardData(aggvo);
            this.getClientUI().setDefaultData();
            this.getClientUI().updateBillShowDigits();
            this.setReqPlanRef();
        }

    }

    private void setReqPlanRef() throws Exception {
        this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_conttype").setEdit(true);
        this.setHeadItemEnable(new String[]{"pk_conttype", "pk_first", "pk_second"});
        UIRefPane refPane = (UIRefPane)this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_conttype").getComponent();
        ContTypeRefTreeModel model = (ContTypeRefTreeModel)refPane.getRefModel();
        model.setWherePart(" pk_conttype in( SELECT DISTINCT pk_conttype FROM jzpm_cm_jzscmctref where   pk_ct_type in(SELECT distinct  pk_ct_type FROM ct_type where ninvctlstyle=0 and  isnull(dr,0)=0) and isnull(dr,0)=0 ) and iproperty=" + this.getContType()[0]);
        this.setButtonValue(9);
        this.getClientUI().getButtonManager().getButton(11).setEnabled(false);
        this.getClientUI().getButtonManager().getButton(15).setEnabled(false);
        this.getClientUI().getButtonManager().getButton(726).setEnabled(false);
        this.getClientUI().updateButtonUI();
        this.getBillCardPanelWrapper().getBillCardPanel().getBillModel("jzpm_cm_stocklist").execFormulas(new String[]{"pk_contmeasdoc->getColValue(bd_invbasdoc,pk_measdoc,pk_invbasdoc,pk_invbasdoc);", "define_mea->getColValue(bd_measdoc,measname,pk_measdoc , pk_contmeasdoc);"});
        this.setbodyItemEnable("jzpm_cm_stocklist", new String[]{"vinvclcode", "vinvmandoccode", "define_mea", "vreserve9"});
        this.getBillCardPanelWrapper().getBillCardPanel().execHeadLoadFormulas();
    }

    private void setHeadItemEnable(String[] items) throws BusinessException {
        String[] var5 = items;
        int var3 = 0;

        for(int var4 = items.length; var3 < var4; ++var3) {
            String item = var5[var3];
            this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem(item).setNull(true);
        }

    }

    private void setbodyItemEnable(String tabcode, String[] items) throws BusinessException {
        String[] var6 = items;
        int var4 = 0;

        for(int var5 = items.length; var4 < var5; ++var4) {
            String item = var6[var4];
            this.getBillCardPanelWrapper().getBillCardPanel().getBodyItem(tabcode, item).setEnabled(false);
        }

    }

    private void onImportExcelData() throws BusinessException {
        int tabIndex = this.getBillCardPanelWrapper().getBillCardPanel().getBodyTabbedPane().getSelectedIndex();
        if (tabIndex != -1) {
            try {
                NCExcelImportAdapter excelImporter = null;
                if (tabIndex == 0) {
                    excelImporter = new NCExcelImportAdapter(this.getBillUI(), CmStocklistVO.EXCEL_CFG_FACTROY.getExcelImportCfg(), CmStocklistVO.class);
                }

                this.onImportExcel(excelImporter);
            } catch (Exception var3) {
                Logger.error(var3.getMessage(), var3);
                throw new BusinessException(var3.getMessage(), var3);
            }
        }
    }

    private void execFormula() throws BusinessException {
        int rowCount = this.getBillCardPanelWrapper().getBillCardPanel().getBillModel("jzpm_cm_stocklist").getRowCount();
        if (rowCount > 0) {
            String[] formulas = new String[]{"vinvclcode->getColValue(bd_invcl,invclasscode,pk_invcl,pk_invcl)", "vinvclname->getColValue(bd_invcl,invclassname,pk_invcl,pk_invcl)", "vinvmandocname->getColValue(bd_invbasdoc,invname,pk_invbasdoc,pk_invbasdoc)", "define_size->getColValue(bd_invbasdoc, invpinpai, pk_invbasdoc,pk_invbasdoc )", "define_shape->getColValue(bd_invbasdoc,invspec , pk_invbasdoc,pk_invbasdoc)", "define_type->getColValue(bd_invbasdoc, invtype, pk_invbasdoc,pk_invbasdoc )", "pk_contmeasdoc->getColValue(bd_invbasdoc,pk_measdoc,pk_invbasdoc,pk_invbasdoc)", "define_mea->getColValue(bd_measdoc,measname,pk_measdoc , pk_contmeasdoc)", "nhistorymaxprice->getColValue(bd_invmandoc,maxprice,pk_invmandoc,pk_invmandoc)"};

            for(int i = 0; i < rowCount; ++i) {
                this.getBillCardPanelWrapper().getBillCardPanel().getBillModel().execFormulas(formulas);
            }
        }

    }

    private void onImportExcel(NCExcelImportAdapter<? extends SuperVO> excelImporter) throws Exception {
        if (1 == excelImporter.showMode()) {
            List importedResVOs = null;
            if (importedResVOs != null && ((List)importedResVOs).size() != 0) {
                BillModel billModel = this.getBillCardPanelWrapper().getBillCardPanel().getBillModel("jzpm_cm_stocklist");
                UITable billTable = this.getBillCardPanelWrapper().getBillCardPanel().getBillTable("jzpm_cm_stocklist");
                int i = 0;

                for(int size = ((List)importedResVOs).size(); i < size; ++i) {
                    this.getBillCardPanelWrapper().getBillCardPanel().addLine("jzpm_cm_stocklist");
                    int rowIndex = billTable.getSelectedRow();
                    billModel.setBodyRowVO((CircularlyAccessibleValueObject)((List)importedResVOs).get(i), rowIndex);
                }

                this.execFormula();
            }
        }

    }

    private void processImportedResVO(List<CmStocklistVO> importedResVOs) throws BusinessException {
        int i = 0;

        for(int size = importedResVOs.size(); i < size; ++i) {
            CmStocklistVO vo = (CmStocklistVO)importedResVOs.get(i);
            UFDouble originmny = SafeCompute.multiply(vo.getNcontnum(), vo.getNprice());
            CurrencyHelper currentHelper = new CurrencyHelper(this.getBillUI()._getCorp().getPk_corp());
            UFDouble basemny = currentHelper.getBaseMoney(originmny);
            vo.setNtotaloriginmny(originmny);
            vo.setNtotalbasemny(basemny);
            String invcode = vo.getPk_invmandoc();
            InvbasdocVO invVo = this.getInvbasdocPK(invcode);
            if (invVo == null) {
                throw new BusinessException("无此分类");
            }

            String invbasdocPK = invVo.getPrimaryKey();
            String invclPK = invVo.getPk_invcl();
            if (VOTool.isNull(invbasdocPK)) {
                vo.setPk_invbasdoc((String)null);
            } else {
                vo.setPk_invbasdoc(invbasdocPK);
            }

            if (VOTool.isNull(invclPK)) {
                vo.setPk_invcl((String)null);
            } else {
                vo.setPk_invcl(invclPK);
            }
        }

    }

    private InvbasdocVO getInvbasdocPK(String invcode) {
        InvbasdocVO result = new InvbasdocVO();

        try {
            String methodName = "getInvbasdocPKByInvcode";
            Class[] strClass = new Class[]{String.class};
            String[] paraValues = new String[]{invcode};
            JzInvocationParam param = new JzInvocationParam("nc.bs.jzpm.cm2615.StockContractDetailDMO", methodName, strClass, paraValues);
            result = (InvbasdocVO)JZPMProxy.getStatefulInvoker().invoke(param);
        } catch (BusinessException var7) {
            Logger.error("", var7);
        }

        return result;
    }

    public String getPk_invmandoc() {
        return this.getBillListPanelWrapper().getBillListPanel().getBodyItem("pk_invmandoc") != null ? this.getBillListPanelWrapper().getBillListPanel().getBodyItem("pk_invmandoc").toString() : null;
    }

    public static List<CmStocklistVO> getImportedResVO(String PK_INVMANDOC, CmStocklistVO[] importedResVOs) {
        List result = new ArrayList();
        if (importedResVOs != null) {
            result.addAll((Collection)Arrays.asList(importedResVOs));
        }

        return result;
    }

    protected void setbodyDatas(CMContractAggVO contAgg, TeConfirmDatas datas) {
        super.setbodyDatas(contAgg, datas);
        TeConfirmMateVO[] matevos = datas.getConfirmmatevo();
        if (matevos != null && matevos.length > 0) {
            CmStocklistVO[] contbodyvo = new CmStocklistVO[matevos.length];
            int i = 0;

            for(int len = matevos.length; i < len; ++i) {
                contbodyvo[i] = new CmStocklistVO();
                if (matevos[i].getIcontenttype() == 1) {
                    contbodyvo[i].setPk_invcl(matevos[i].getPk_invcl());
                } else if (matevos[i].getIcontenttype() == 0) {
                    contbodyvo[i].setPk_invbasdoc(matevos[i].getPk_invbasdoc());
                    contbodyvo[i].setPk_invmandoc(matevos[i].getPk_invmandoc());
                }

                UFDouble price = matevos[i].getNprice();
                UFDouble num = matevos[i].getNnum();
                contbodyvo[i].setNprice(price);
                contbodyvo[i].setNcontnum(num);
                contbodyvo[i].setNtotaloriginmny(SafeCompute.multiply(price, num));
                contbodyvo[i].setNtotalbasemny(SafeCompute.multiply(price, num));
            }

            contAgg.setTableVO("jzpm_cm_stocklist", contbodyvo);
        }

    }

    protected void setheadDatas(CMContractAggVO contAgg, AggregatedValueObject aggvo, TeConfirmDatas datas) {
        super.setheadDatas(contAgg, aggvo, datas);
        this.initHeadContTypeRef(datas);
    }

    protected int[] getContType() {
        return new int[]{2};
    }

    protected void initHeadContTypeRef(TeConfirmDatas datas) {
        if (datas != null) {
            TeConfirmMateVO[] matevos = datas.getConfirmmatevo();
            TeConfirmBVO[] confirmbvos = datas.getConfirmbvo();
            String pk_corp = null;
            int ninvclstyle = -1;
            if (matevos != null && matevos.length > 0) {
                Integer ct = -1;
                TeConfirmMateVO[] var10 = matevos;
                int var8 = 0;

                for(int var9 = matevos.length; var8 < var9; ++var8) {
                    TeConfirmMateVO matevo = var10[var8];
                    if (ct != -1 && ct != matevo.getIcontenttype()) {
                        ct = -1;
                        break;
                    }

                    ct = matevo.getIcontenttype();
                }

                if (ct == 0) {
                    ninvclstyle = 0;
                } else if (ct == 1) {
                    ninvclstyle = 1;
                }
            }

            ContTypeRefTreeModel contTypeModel = null;
            UIRefPane refpane = (UIRefPane)this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").getComponent();
            if (contTypeModel == null) {
                contTypeModel = new ContTypeRefTreeModel(this.getContType());
            }

            StringBuffer sql_cttype = new StringBuffer();
            sql_cttype.append(" SELECT jbc.pk_conttype FROM jzpm_bd_conttype jbc ");
            sql_cttype.append(" inner join jzpm_cm_jzscmctref jcj on jbc.pk_conttype = jcj.pk_conttype ");
            sql_cttype.append(" inner join ct_type ct on ct.pk_ct_type = jcj.pk_ct_type");
            sql_cttype.append(" where isnull(jbc.dr,0)=0 and isnull(jcj.dr,0)=0 and isnull(ct.dr,0)=0");
            if (ninvclstyle != -1) {
                sql_cttype.append(" and ct.ninvctlstyle = " + ninvclstyle);
            }

            if (confirmbvos != null && confirmbvos.length > 0) {
                pk_corp = confirmbvos[0].getPk_corp();
                if (pk_corp != null && pk_corp.length() > 0) {
                    sql_cttype.append(" and jcj.pk_corp = '" + pk_corp + "'");
                }
            }

            List typelist = new ArrayList();
            Vector vec = null;

            try {
                vec = JZPMProxy.getIJZPMPubBusi().executeQuerySQL(sql_cttype.toString());
            } catch (BusinessException var14) {
                Logger.error(var14.getMessage(), var14);
            }

            if (vec != null && vec.size() > 0) {
                for(int i = 0; i < vec.size(); ++i) {
                    String tmp = (Vector)((Vector)vec.get(i)) == null ? null : ((Vector)((Vector)vec.get(i))).toString();
                    if (tmp != null && tmp.length() > 0) {
                        String cttype = tmp.substring(1, tmp.length() - 1);
                        typelist.add(cttype);
                    }
                }
            }

            StringBuffer conttype = new StringBuffer();
            conttype.append(" and isnull(dr,0)=0 and pk_conttype in (" + changelist2String(typelist) + ")");
            contTypeModel.addWherePart(conttype.toString());
            contTypeModel.getRefSql();
            refpane.setRefModel(contTypeModel);
        }
    }

    private static String changelist2String(List<String> list) {
        if (list != null && list.size() != 0) {
            String strList = "";

            for(int i = 0; i < list.size(); ++i) {
                strList = strList + "'" + (String)list.get(i) + "'";
                if (i + 1 < list.size()) {
                    strList = strList + ',';
                }
            }

            return strList;
        } else {
            return "''";
        }
    }

    protected void onBoEdit() throws Exception {
        super.onBoEdit();
        this.getClientUI().isInfoSupply = false;
        Object vlastbilltype = this.getBillCardPanelWrapper().getBillCardPanel().getBillModel("jzpm_cm_stocklist").getBodyValueRowVO(0, CmStocklistVO.class.getName()).getAttributeValue("vlastbilltype");
        if (vlastbilltype != null && vlastbilltype.toString().equals("99F3")) {
            this.setReqPlanRef();
        } else {
            this.resetContType();
            this.getClientUI().setBodyInvItemEditable();
        }

    }

    protected void onBoLineAdd() throws Exception {
        String pk_conttype = this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").getValueObject() == null ? null : this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").getValueObject().toString();
        if (pk_conttype != null && pk_conttype.length() != 0) {
            super.onBoLineAdd();
            this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").setEnabled(false);
        } else {
            MessageDialog.showHintDlg(this.getClientUI(), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000001"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000002"));
        }
    }

    protected void onBoLineIns() throws Exception {
        String pk_conttype = this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").getValueObject() == null ? null : this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").getValueObject().toString();
        if (pk_conttype != null && pk_conttype.length() != 0) {
            super.onBoLineIns();
            this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").setEnabled(false);
        } else {
            MessageDialog.showHintDlg(this.getClientUI(), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000001"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000002"));
        }
    }

    protected void onBoLineDel() throws Exception {
        super.onBoLineDel();
        if (this.getClientUI().getBillCardPanel().getBillModel("jzpm_cm_stocklist").getRowCount() == 0) {
            this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").setEnabled(true);
            this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").setNull(true);
            this.getClientUI().initContType();
        }
        
        BillModel  model = this.getClientUI().getBillCardPanel().getBillModel("jzpm_cm_stocklist");
        int rowcount = model.getRowCount();
        if (rowcount > 0) {
         UFDouble tax = UFDouble.ZERO_DBL;
         UFDouble ncontsignoriginmny = UFDouble.ZERO_DBL;
         for (int i = 0; i < rowcount; i++) {
        	 CmStocklistVO bvo=  (CmStocklistVO) model.getBodyValueRowVO(i, CmStocklistVO.class.getName());
        	 String taxStr = bvo.getVdef8();
             tax = tax.add(taxStr == null ?UFDouble.ZERO_DBL: new UFDouble(taxStr));
             UFDouble ntotaloriginmny = bvo.getNtotaloriginmny();
             ncontsignoriginmny = ncontsignoriginmny.add(ntotaloriginmny == null ? UFDouble.ZERO_DBL: ntotaloriginmny);
          }
         this.getClientUI().getBillCardPanel().getHeadItem("vdef7").setValue(tax);
         this.getClientUI().getBillCardPanel().getHeadItem("ncontsignoriginmny").setValue(ncontsignoriginmny);
         this.getClientUI().getBillCardPanel().getHeadItem("vdef8").setValue(SafeCompute.sub(ncontsignoriginmny, tax));
        }
    }

    private void doQueryContDoCond() throws Exception {
        String pk_contract = null;
        if (!this.getClientUI().isListPanelSelected()) {
            pk_contract = this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_contract").getValueObject() == null ? null : this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_contract").getValueObject().toString();
        } else if (this.getBufferData().getCurrentVO() != null) {
            pk_contract = this.getBufferData().getCurrentVO().getParentVO().getAttributeValue("pk_contract") == null ? null : this.getBufferData().getCurrentVO().getParentVO().getAttributeValue("pk_contract").toString();
        }

        this.getLinkContCondData().setPk_contract(pk_contract);
        SFClientUtil.openLinkedQueryDialog("H511901025", (Component)null, this.getLinkContCondData());
    }

    private LinkContCondData getLinkContCondData() {
        if (this.m_linkContCondData == null) {
            this.m_linkContCondData = new LinkContCondData();
        }

        return this.m_linkContCondData;
    }

    public void importData() throws Exception {
        if (this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_ct_type") != null) {
            Object pk_ct_type = this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_ct_type").getValueObject();
            if (pk_ct_type != null && !pk_ct_type.equals("")) {
                Object[] o = this.getClientUI().queryColumnValue("ct_type", "pk_ct_type", "ninvctlstyle", new String[]{pk_ct_type.toString()});
                if (o != null && o.length > 0) {
                    int controltype = new Integer(o[0].toString());
                    this.scm_conttypectrlmode = controltype;
                }
            }
        }
        super.importData();
    }

    public ImportExcelBase getImportExcelData() {
        ImportExcelBase impData = null;
        impData = new StockImportExcelData();
        return impData;
    }

    public void setDataByImportData(ImportExcelBase impData, ImportExcelDialog dlg, int scm_conttypectrlmode) throws Exception {
        impData.setDatatoUI(this.getClientUI(), dlg, scm_conttypectrlmode);
    }

    public String[] getFieldCodes() {
        String[] importCodes = (String[])null;
        if (this.scm_conttypectrlmode == 0) {
            importCodes = new String[]{"vinvmandoccode", "define_mea", "ncontnum", "nprice", "vmanufacturer", "vqualitygrade", "dplanindate", "vmemo"};
        } else {
            importCodes = new String[]{"vinvclcode", "ncontnum", "nprice", "vmanufacturer", "vqualitygrade", "dplanindate", "vmemo"};
        }

        return importCodes;
    }

    public String[] getFieldNames() {
        String[] importNames = (String[])null;
        if (this.scm_conttypectrlmode == 0) {
            importNames = new String[]{NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJTH5112615-000137"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJTH5112615-000104"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0001113"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0000741"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJTH5112615-000093"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0003840"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0003472"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0001376")};
        } else {
            importNames = new String[]{NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0000567"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0001113"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0000741"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJTH5112615-000093"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0003840"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0003472"), NCLangRes4VoTransl.getNCLangRes().getStrByID("common", "UC000-0001376")};
        }

        return importNames;
    }

    private void isInvExceedPrice() throws Exception {
        CmStocklistVO[] stockvos = (CmStocklistVO[])null;
        stockvos = (CmStocklistVO[])((IExAggVO)this.getClientUI().getVOFromUI()).getTableVO("jzpm_cm_stocklist");
        if (stockvos != null && stockvos.length > 0) {
            if (this.isInvExceed(stockvos)) {
                this.getBillCardPanelWrapper().getBillCardPanel().setHeadItem("bisinvexceedprice", new UFBoolean(true));
            } else {
                this.getBillCardPanelWrapper().getBillCardPanel().setHeadItem("bisinvexceedprice", new UFBoolean(false));
            }
        }

    }

    @SuppressWarnings("unchecked")
	protected void onBoSave() throws Exception {
        this.isInvExceedPrice();
        if (this.isCTNotNullCtrl()) {
        	//1
            AggregatedValueObject billVO = this.getBillUI().getChangedVOFromUI();
            CmContractVO contractvo = (CmContractVO)billVO.getParentVO();
            Map<String, UFDouble> oldvaluemap = new HashMap();
            Map<String, String> vlastmap = new HashMap();
            if (contractvo != null && contractvo.getPk_contract() != null && contractvo.getPk_contract() != "") {
            	////未保存时，查询所有数据，做old数据      当前删除，上次删除，上上次删除  
                //List<CmStocklistVO> list = (List)((IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class)).retrieveByClause(CmStocklistVO.class, " pk_contract ='" + contractvo.getPk_contract() + "' ");
            	List<CmStocklistVO> list = (List)((IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class)).retrieveByClause(CmStocklistVO.class, " pk_contract ='" + contractvo.getPk_contract() + "'  and nvl(dr, 0) = 0 ");
            	if (list != null && list.size() > 0) {
                    Iterator var7 = list.iterator();
                    while(var7.hasNext()) {
                        CmStocklistVO vo = (CmStocklistVO)var7.next();
                        oldvaluemap.put(vo.getPk_stocklist(), vo.getNcontnum());
                        vlastmap.put(vo.getPk_stocklist(), vo.getVlastbillrowid());
                    }
                }
            }
            //2
            super.onBoSave();
            //3
            billVO = this.getBufferData().getCurrentVO();
            CmStocklistVO[] newbodyvo = (CmStocklistVO[])billVO.getChildrenVO();
            Map<String, UFDouble> changemap = new HashMap();
            int var8;
            int var9;
            CmStocklistVO[] var10;
			String billtype;String pk_head;
            String pk_bodyid;
            UFDouble ctnum;
            CmStocklistVO bvo;
            String key;
            if (oldvaluemap != null && oldvaluemap.size() >= 1) {
                var10 = newbodyvo;
                var8 = 0;
                for(var9 = newbodyvo.length; var8 < var9; ++var8) {
                    bvo = var10[var8];
                    billtype = bvo.getVlastbilltype();
                    pk_head = bvo.getVlastbillid();
                    pk_bodyid = bvo.getVlastbillrowid();
                    ctnum = bvo.getNcontnum();
                    String pk = bvo.getPk_stocklist();
                    if (oldvaluemap.containsKey(pk)) {  //没被删除   计算差值--大小等0
                        ctnum = ctnum.sub((UFDouble)oldvaluemap.get(pk));
                        changemap.put(pk_bodyid, ctnum);
                        oldvaluemap.remove(pk);
                    } else {//新增行
                        changemap.put(pk_bodyid, ctnum);
                    }
                }

                if (oldvaluemap != null && oldvaluemap.size() > 0) {//已经被删除  进行的是逻辑删除
                    Iterator var21 = oldvaluemap.keySet().iterator();
                    while(var21.hasNext()) {
                        String key1 = (String)var21.next();
                        UFDouble num = oldvaluemap.get(key1);
                       if(num==null){num = UFDouble.ZERO_DBL;}
                        changemap.put(vlastmap.get(key1), num.multiply(new UFDouble(-1)));
                    }
                }
            } else {//全新增
                var10 = newbodyvo;
                var8 = 0;
                for(var9 = newbodyvo.length; var8 < var9; ++var8) {
                    bvo = var10[var8];
                    billtype = bvo.getVlastbilltype();
                    pk_head = bvo.getVlastbillid();
                    pk_bodyid = bvo.getVlastbillrowid();
                    ctnum = bvo.getNcontnum();
                    changemap.put(pk_bodyid, ctnum);
                }
            }
            if (changemap != null && changemap.size() > 0) {//已使用数量更新
                IUAPQueryBS query = (IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class);
                List<MtCtplanBSumVO> sumvolist = new ArrayList();
                Iterator var24 = changemap.keySet().iterator();
                while(var24.hasNext()) {
                    key = (String)var24.next();//很关键     key--MtCtplanBSumVO的PK  消耗材料总计划那边“审批-弃审”，会删除PK
                    MtCtplanBSumVO vo = (MtCtplanBSumVO)query.retrieveByPK(MtCtplanBSumVO.class, key);
                    if(vo!=null){//表体，删除一行，以前但仍能拿到主键
                    	UFDouble usedNum = (vo.getUsednum() == null ? UFDouble.ZERO_DBL: vo.getUsednum()).add((UFDouble)changemap.get(key));
                    	if(vo.getNnum().compareTo(usedNum)<0){throw new Exception("消耗材料总控计划中，总控数量不足！请进行数量追加！");}
                    	vo.setUsednum(usedNum);
                    	sumvolist.add(vo);
                    }else{//计划改变，合同这边没刷新，整个合同数据挂掉----不重构，没办法解决
                    	throw new Exception("未找到总控计划对应的资源汇总表");
                    }
                }
                ((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).updateVOList(sumvolist);
            }
        }

    }

    @SuppressWarnings("unused")
	private void setContype() throws Exception {
        UIRefPane refPane = (UIRefPane)this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_conttype").getComponent();
        ContTypeRefTreeModel model = new ContTypeRefTreeModel(this.getContType());
        refPane.setRefModel(model);
    }

    @SuppressWarnings("unchecked")
	private boolean isCTNotNullCtrl() throws BusinessException {
        String pk_ct_type = null;
        String pk_corp = null;
        boolean pricenotnull = false;
        boolean numnotnull = false;
        boolean mnynotnull = false;
        int notctrl = -1;
        CmStocklistVO[] bodyvos = (CmStocklistVO[])null;
        if (this.getClientUI().isListPanelSelected()) {
            if (this.getClientUI().getBufferData() != null && this.getClientUI().getBufferData().getCurrentVO() != null) {
                pk_ct_type = ((CmContractVO)this.getClientUI().getBufferData().getCurrentVO().getParentVO()).getPk_ct_type();
                pk_corp = ((CmContractVO)this.getClientUI().getBufferData().getCurrentVO().getParentVO()).getPk_corp();
                bodyvos = (CmStocklistVO[])((IExAggVO)this.getClientUI().getBufferData().getCurrentVO()).getTableVO("jzpm_cm_stocklist");
            }
        } else {
            pk_ct_type = (String)this.getClientUI().getBillCardPanel().getHeadItem("pk_ct_type").getValueObject();
            pk_corp = (String)this.getClientUI().getBillCardPanel().getHeadItem("pk_corp").getValueObject();
            bodyvos = (CmStocklistVO[])this.getClientUI().getBillCardPanel().getBillModel("jzpm_cm_stocklist").getBodyValueVOs(CmStocklistVO.class.getName());
        }

        String sql = " select pk_ct_type,ndatactlstyle from ct_type where isnull(dr,0)=0 and pk_ct_type = '" + pk_ct_type + "' and pk_corp = '" + pk_corp + "'";
        Vector vec = JZPMProxy.getIJZPMPubBusi().executeQuerySQL(sql);
        if (vec != null && vec.size() > 0) {
            Vector newvec = (Vector)((Vector)vec.get(0));
            notctrl = (Integer)newvec.get(1);
        }

        if (notctrl == 0 || notctrl == 3 || notctrl == 4 || notctrl == 6) {
            pricenotnull = true;
        }

        if (notctrl == 1 || notctrl == 3 || notctrl == 5 || notctrl == 6) {
            numnotnull = true;
        }

        if (notctrl == 2 || notctrl == 4 || notctrl == 5 || notctrl == 6) {
            mnynotnull = true;
        }

        CmStocklistVO[] var13 = bodyvos;
        int var11 = 0;

        for(int var12 = bodyvos.length; var11 < var12; ++var11) {
            CmStocklistVO vo = var13[var11];
            UFDouble price = vo.getNprice();
            UFDouble num = vo.getNcontnum();
            UFDouble mny = vo.getNtotaloriginmny();
            if (SafeCompute.compare(price, UFDouble.ZERO_DBL) == 0 && pricenotnull) {
                MessageDialog.showHintDlg(this.getClientUI(), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000001"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000013"));
                return false;
            }

            if (SafeCompute.compare(num, UFDouble.ZERO_DBL) == 0 && numnotnull) {
                MessageDialog.showHintDlg(this.getClientUI(), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000001"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000013"));
                return false;
            }

            if (SafeCompute.compare(mny, UFDouble.ZERO_DBL) == 0 && mnynotnull) {
                MessageDialog.showHintDlg(this.getClientUI(), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000001"), NCLangRes4VoTransl.getNCLangRes().getStrByID("H5112615", "UJZH5112615-8000013"));
                return false;
            }
        }

        return true;
    }

    private boolean isInvExceed(CmStocklistVO[] vos) throws BusinessException {
        UFDouble price = null;
        UFDouble maxprice = null;
        boolean flag = false;
        if (vos != null && vos.length != 0) {
            CmStocklistVO[] var8 = vos;
            int var6 = 0;

            for(int var7 = vos.length; var6 < var7; ++var6) {
                CmStocklistVO vo = var8[var6];
                price = vo.getNprice();
                maxprice = vo.getNhistorymaxprice();
                if (maxprice != null && maxprice.doubleValue() > 0.0D && SafeCompute.sub(price, maxprice).doubleValue() > 0.0D) {
                    flag = true;
                    break;
                }
            }

            return flag;
        } else {
            return false;
        }
    }

    protected AggregatedValueObject getCopyVO() throws Exception {
        AggregatedValueObject aggvo = this.getClientUI().getVOFromUI();
        AggregatedValueObject result = aggvo;
        if (aggvo.getChildrenVO() != null) {
            Object vlastbilltype = aggvo.getChildrenVO()[0].getAttributeValue("vlastbilltype");
            if (vlastbilltype != null && vlastbilltype.toString().equals("99F3")) {
                String[] items = new String[]{"vinvclcode", "vinvmandoccode", "define_mea", "ncontnum"};
                String[] var8 = items;
                int var6 = 0;

                for(int var7 = items.length; var6 < var7; ++var6) {
                    String item = var8[var6];
                    this.getBillCardPanelWrapper().getBillCardPanel().getBodyItem("jzpm_cm_stocklist", item).setEnabled(true);
                }
            }
        }

        aggvo.setParentVO(aggvo.getParentVO());
        String[] tablecodes = new String[]{"jzpm_cm_stocklist", "jzpm_cm_stocklist_b"};

        for(int i = 0; i < tablecodes.length; ++i) {
            if ("jzpm_cm_stocklist".equals(tablecodes[i])) {
                CircularlyAccessibleValueObject[] vos = ((IExAggVO)aggvo).getTableVO(tablecodes[i]);
                this.filterItems(vos);
                ((IExAggVO)result).setTableVO(tablecodes[i], vos);
            } else {
                ((IExAggVO)result).setTableVO(tablecodes[i], (CircularlyAccessibleValueObject[])null);
            }
        }

        return result;
    }

    private void filterItems(CircularlyAccessibleValueObject[] vos) throws BusinessException {
        if (vos != null) {
            CircularlyAccessibleValueObject[] var5 = vos;
            int var3 = 0;

            for(int var4 = vos.length; var3 < var4; ++var3) {
                CircularlyAccessibleValueObject vo = var5[var3];
                vo.setAttributeValue("vlastbillid", (Object)null);
                vo.setAttributeValue("vlastbillrowid", (Object)null);
                vo.setAttributeValue("vlastbilltype", (Object)null);
                vo.setAttributeValue("vsourcebillid", (Object)null);
                vo.setAttributeValue("vsourcebillrowid", (Object)null);
                vo.setAttributeValue("vsourcebilltype", (Object)null);
            }
        }

    }

    protected void doFill(String pk_source, int contno, String pk_billtype) throws BusinessException {
        super.doFill(pk_source, contno, pk_billtype);
        StringBuffer sql = new StringBuffer();
        sql.append(" pk_confirm = '" + pk_source + "' and icontno = " + contno);
        TeConfirmMateVO[] matevos = (TeConfirmMateVO[])JZPMProxy.getIJZPMPubBusi().queryByWhereClause(TeConfirmMateVO.class, sql.toString(), (String)null, (String[])null);
        if (matevos != null && matevos.length > 0) {
            List invcllist = new ArrayList();
            List invlist = new ArrayList();
            Map invclmap = new HashMap();
            Map invdocmap = new HashMap();
            String ct_type = (String)this.getClientUI().getBillCardPanel().getHeadItem("pk_ct_type").getValueObject();
            if (ct_type == null || ct_type.toString().length() == 0) {
                return;
            }

            CmStocklistVO[] stocklistvos = (CmStocklistVO[])null;
            Object[] o = this.getClientUI().queryColumnValue("ct_type", "pk_ct_type", "ninvctlstyle", new String[]{ct_type});
            if (o != null && o.length > 0) {
                int controltype = new Integer(o[0].toString());
                TeConfirmMateVO matevo;
                int len;
                int var16;
                TeConfirmMateVO[] var17;
                String pk_invcl;
                int i;
                if (controltype == 0) {
                    var17 = matevos;
                    len = 0;

                    for(var16 = matevos.length; len < var16; ++len) {
                        matevo = var17[len];
                        pk_invcl = matevo.getPk_invbasdoc();
                        invlist.add(pk_invcl);
                        invdocmap.put(pk_invcl, matevo);
                    }

                    stocklistvos = new CmStocklistVO[invlist.size()];
                    i = 0;

                    for(len = stocklistvos.length; i < len; ++i) {
                        stocklistvos[i].setPk_invbasdoc((String)invlist.get(i));
                    }
                } else {
                    var17 = matevos;
                    len = 0;

                    for(var16 = matevos.length; len < var16; ++len) {
                        matevo = var17[len];
                        pk_invcl = matevo.getPk_invcl();
                        invcllist.add(pk_invcl);
                        invclmap.put(pk_invcl, matevo);
                    }

                    stocklistvos = new CmStocklistVO[invcllist.size()];
                    i = 0;

                    for(len = stocklistvos.length; i < len; ++i) {
                        stocklistvos[i].setPk_invcl((String)invcllist.get(i));
                    }
                }
            }

            if (stocklistvos != null && stocklistvos.length > 0) {
                try {
                    ((IExAggVO)this.getClientUI().getVOFromUI()).setTableVO("jzpm_cm_stocklist", stocklistvos);
                } catch (Exception var19) {
                    var19.printStackTrace();
                }
            }
        }

    }

    protected void onBoCopy() throws Exception {
        super.onBoCopy();
        this.resetContType();
        this.getClientUI().initInvMap();
    }

    private void resetContType() throws Exception {
        Object pk_conttype = this.getBillCardPanelWrapper().getBillCardPanel().getHeadItem("pk_conttype").getValueObject();
        this.getClientUI().initContType();
        this.getClientUI().getBillCardPanel().setHeadItem("pk_conttype", pk_conttype);
        if (pk_conttype != null) {
            this.getClientUI().getBillCardPanel().getHeadItem("pk_conttype").setEdit(false);
        }

    }

    protected void onBoDel() throws Exception {
    	super.onBoDel();
    }

    private ArrayList<String> changeStr2List(String[] str) {
        ArrayList result = new ArrayList();
        if (str != null && str.length != 0) {
            int i = 0;

            for(int len = str.length; i < len; ++i) {
                result.add(str[i]);
            }

            return result;
        } else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
	protected void doOnBoDel() throws Exception {
        BillItem[] items = (BillItem[])null;
        new ArrayList();
        ArrayList detaillist = new ArrayList();
        new ArrayList();
        ArrayList baselist = this.changeStr2List(IContract.CONT_BASE_FIELDS);
        ArrayList contlist = this.changeStr2List(IContract.CONT_DETAIL_FIELDS);
        if (!this.getClientUI().isListPanelSelected()) {
            items = this.getClientUI().getBillCardPanel().getHeadShowItems();
        } else {
            items = this.getClientUI().getBillListPanel().getBillListData().getHeadItems();
        }

        int i = 0;

        for(int len = items.length; i < len; ++i) {
            if (items[i].isShow() && !baselist.contains(items[i].getKey()) && !items[i].getKey().equals("pk_billtype") && !items[i].getKey().equals("vbillstatus") && contlist.contains(items[i].getKey())) {
                detaillist.add(items[i].getKey());
            }
        }

        AggregatedValueObject modelVo = this.getBufferData().getCurrentVO();
        if (modelVo instanceof HYBillVO) {
            ((HYBillVO)modelVo).setM_billField(this.getBillField());
        }

        IUAPQueryBS query = (IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class);
        List<CmStocklistVO> bodyvos = (List)query.retrieveByClause(CmStocklistVO.class, " pk_contract ='" + modelVo.getParentVO().getPrimaryKey() + "' and nvl(dr, 0) = 0 ");
        AggregatedValueObject delVo = this.getBusinessAction().delete(modelVo, this.getUIController().getBillType(), this.getBillUI()._getDate().toString(), detaillist);
        ArrayList bodylist=null;
        if (PfUtilClient.isSuccess()) {
            this.getBillUI().setBillOperate(2);
            delVo.getParentVO().setAttributeValue(this.getBillField().getField_BillStatus(), new Integer(4));
            this.updateHeadFixFields(modelVo, delVo, this.getFixFields(4, delVo.getParentVO()));
            
            //根据Vlastbillrowid：pk_mt_ctplan_b_sum 用减法回写数据
            //IUAPQueryBS query = (IUAPQueryBS)NCLocator.getInstance().lookup(IUAPQueryBS.class);
            //List<CmStocklistVO> bodyvos = (List)query.retrieveByClause(CmStocklistVO.class, " pk_contract ='" + delVo.getParentVO().getPrimaryKey() + "' ");
            if (bodyvos != null && bodyvos.size() > 0) {
                bodylist = new ArrayList();
                for(int d = 0; d < bodyvos.size(); ++d) {
                    CmStocklistVO stockvo = (CmStocklistVO)bodyvos.get(d);
                    String pk = stockvo.getVlastbillrowid();
                    UFDouble num = stockvo.getNcontnum();
                    Object vo = query.retrieveByPK(MtCtplanBSumVO.class, pk);//查询MtCtplanBSumVO
                    if (vo != null && vo instanceof MtCtplanBSumVO) {
                        MtCtplanBSumVO cmvo = (MtCtplanBSumVO)vo;
                        cmvo.setUsednum(cmvo.getUsednum().sub(num));
                        bodylist.add(cmvo);
                    }
                }
                ((IVOPersistence)NCLocator.getInstance().lookup(IVOPersistence.class)).updateVOList(bodylist);
            }
        }

        int count_before_refresh = this.getBufferData().getVOBufferSize();
        this.getBufferData().removeCurrentRow();
        int count_after_refresh = this.getBufferData().getVOBufferSize();
        if (count_before_refresh != count_after_refresh) {
            if (count_after_refresh == 0) {
                this.getClientUI().setListHeadData((CircularlyAccessibleValueObject[])null);
            } else {
                this.getClientUI().setListHeadData(this.getBufferData().getAllHeadVOsFromBuffer());
            }
        } else {
            bodylist = null;
            if (this.getBufferData().getCurrentVO() != null) {
                CircularlyAccessibleValueObject vo = this.getBufferData().getCurrentVO().getParentVO();
                this.getBillListPanelWrapper().updateListVo(vo, this.getBufferData().getCurrentRow());
            }
        }

    }

    protected void onBoDelete() throws Exception {
        super.onBoDelete();
    }
}
