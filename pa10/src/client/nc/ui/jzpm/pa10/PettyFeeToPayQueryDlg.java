//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.pa10;

import java.awt.Container;
import nc.bs.framework.common.NCLocator;
import nc.bs.logging.Logger;
import nc.itf.jzfdc.pub.ISysInitQryService;
import nc.ui.jz.pub.ref.JZAllObjProjRefTreeModel;
import nc.ui.jzpm.core.pub.dlg.InfoDialog;
import nc.ui.pub.ClientEnvironment;
import nc.ui.pub.beans.MessageDialog;
import nc.ui.pub.bill.BillEditEvent;
import nc.ui.pub.bill.BillItem;
import nc.vo.jz.pub.util.CommonTool;
import nc.vo.jzfdc.pub.currency.CurrencyRate;
import nc.vo.pub.BusinessException;
import nc.vo.pub.lang.UFBoolean;

public class PettyFeeToPayQueryDlg extends InfoDialog {
    private static final long serialVersionUID = 1L;
    private static final String templetID = "0001XZ1000000003NWGR";
    private String pk_corp = null;
    private boolean m_bisToCMP = false;
    private String projPowerSql = null;

    public PettyFeeToPayQueryDlg(Container parent) {
        super(parent, "0001XZ1000000003NWGR");
        this.pk_corp = ClientEnvironment.getInstance().getCorporation().getPk_corp();
        this.setDefaultOriginType();
        this.initFinanceParam();
    }

    private void setDefaultOriginType() {
        BillItem originItem = this.getBillCardPanel().getHeadItem("pk_origintype");
        originItem.setValue(this.getBaseMnyType());
    }

    private void initFinanceParam() {
        ISysInitQryService service = (ISysInitQryService)NCLocator.getInstance().lookup(ISysInitQryService.class.getName());

        try {
            boolean[] nodeFlags = service.queryJZFinanceSysInitInfo("H51710", this.pk_corp);
            if (nodeFlags != null && nodeFlags.length > 1) {
                this.m_bisToCMP = nodeFlags[1];
            }
        } catch (BusinessException var3) {
            Logger.error("查询建筑财务接口设置出错：", var3);
        }

    }

    public void afterEdit(BillEditEvent e) {
        if ("pk_cumandoc".equals(e.getKey())) {
            this.setValueByCode("cfreecustid", (Object)null);
            this.setValueByCode("pk_inuser", (Object)null);
            this.getBillCardPanel().execHeadEditFormulas();
            this.setFreeEnble();
        }

        if ("pk_inuser".equals(e.getKey())) {
            this.setValueByCode("pk_cumandoc", (Object)null);
            this.setValueByCode("cfreecustid", (Object)null);
            this.getBillCardPanel().execHeadEditFormulas();
            this.setFreeEnble();
        }

        super.afterEdit(e);
    }

    private void setFreeEnble() {
        if (this.isFreeCust()) {
            this.getBillItemByCode("cfreecustid").setEnabled(true);
        } else {
            this.getBillItemByCode("cfreecustid").setEnabled(false);
        }

    }

    private String getBaseMnyType() {
        CurrencyRate cr = new CurrencyRate();
        String pk_basetype = null;

        try {
            pk_basetype = cr.getLocalCurrPK(ClientEnvironment.getInstance().getCorporation().getPrimaryKey());
        } catch (BusinessException var4) {
            Logger.error("获取本币币种错误", var4);
        }

        return pk_basetype;
    }

    public String getPk_project() {
        return (String)this.getValueByCode("pk_project");
    }

    public String getPk_cumandoc() {
        return (String)this.getValueByCode("pk_cumandoc");
    }

    public String getPk_origintype() {
        return (String)this.getValueByCode("pk_origintype");
    }

    public String getCfreecustid() {
        return (String)this.getValueByCode("cfreecustid");
    }

    public String getPk_inuser() {
        return (String)this.getValueByCode("pk_inuser");
    }

    public boolean isFreeCust() {
        return !this.isNull(this.getBillItemByCode("freecustflag").getValueObject()) && (new UFBoolean(this.getBillItemByCode("freecustflag").getValueObject().toString())).booleanValue();
    }

    private String getProjPowerSql() {
        if (this.projPowerSql == null) {
            JZAllObjProjRefTreeModel jzprojmodel = new JZAllObjProjRefTreeModel();
            if (jzprojmodel != null && jzprojmodel.isUseDataPower()) {
                this.projPowerSql = jzprojmodel.getDataPowerSubSql(jzprojmodel.getTableName(), jzprojmodel.getRefNodeName());
                if (this.projPowerSql == null) {
                    this.projPowerSql = "";
                }
            }
        }

        return this.projPowerSql;
    }

    public String getWhereSql() {
        StringBuffer sb = new StringBuffer(" isnull(dr,0)=0 and vbillstatus=1 and pk_corp='" + this.pk_corp + "' and (isnull(noriginmny,0)-isnull(npaidoriginmny,0))>0 ");
        sb.append("and isnull(vreserve50,0)=0 ");//add only one line by HMX 2019-3-28  根据vreserve50做过滤，关联代码nc.ui.jzpm.pa10.ClientEventHandler.updatePettyFeeVreservr50()
        if (!this.isNull(this.getPk_project())) {
            sb.append(" and pk_project='" + this.getPk_project() + "'");
        }

        if (this.isNull(this.getPk_project()) && !this.isNull(this.getProjPowerSql())) {
            sb.append(" and pk_project in(" + this.getProjPowerSql() + ")");
        }

        if (!this.isNull(this.getPk_origintype())) {
            sb.append(" and pk_origintype='" + this.getPk_origintype() + "' ");
        }

        if (!this.isNull(this.getPk_cumandoc())) {
            sb.append(" and pk_cumandoc='" + this.getPk_cumandoc() + "' ");
        }

        if (!this.isNull(this.getCfreecustid())) {
            sb.append(" and cfreecustid='" + this.getCfreecustid() + "' ");
        }

        if (!this.isNull(this.getPk_inuser())) {
            sb.append(" and pk_inuser='" + this.getPk_inuser() + "' ");
        }

        return sb.toString();
    }

    private boolean isNull(Object o) {
        return CommonTool.chkNull(o);
    }

    protected boolean specialCheck() {
        if (this.m_bisToCMP && this.isNull(this.getPk_cumandoc()) && this.isNull(this.getCfreecustid()) && this.isNull(this.getPk_inuser())) {
            MessageDialog.showHintDlg(this, "提示：", "付款单需要传现金管理平台，请选择收款单位！");
            return false;
        } else {
            return super.specialCheck();
        }
    }
}
