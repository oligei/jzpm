//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.bdh28001;

import nc.bs.logging.Logger;
import nc.ui.jzpm.bdh28001.refpooder.AddGroupBtnVO;
import nc.ui.jzpm.bdh28001.refpooder.RefPoOrderBtnVO;
import nc.ui.jzpm.pa0605.switchaction.CloseBtnVO;
import nc.ui.jzpm.pa0605.switchaction.OpenBtnVO;
import nc.ui.jzpm.pa0605.switchaction.SwitchGroupBtnVO;
import nc.ui.jzpm.pub.eventhandler.ManageEventHandler;
import nc.ui.jzpm.pub.ui.MultiVarChildBillManageUI;
import nc.ui.pub.FramePanel;
import nc.ui.pub.beans.UIComboBox;
import nc.ui.pub.bill.BillEditEvent;
import nc.ui.pub.bill.BillItem;
import nc.ui.pub.linkoperate.ILinkApproveData;
import nc.ui.trade.bill.AbstractManageController;
import nc.vo.jz.pub.BillDateGetter;
import nc.vo.jz.pub.JZProxy;
import nc.vo.jzpm.bdh28001.BdDeftableVO;
import nc.vo.jzpm.pub.JZPMProxy;
import nc.vo.ml.NCLangRes4VoTransl;
import nc.vo.pp.ask.AddVO;
import nc.vo.pub.BusinessException;
import nc.vo.pub.CircularlyAccessibleValueObject;
import nc.vo.pub.billtype.BilltypeVO;
import nc.vo.trade.button.ButtonVO;
import nc.vo.trade.pub.IBillStatus;

public class ClientUI extends MultiVarChildBillManageUI {
    private static final long serialVersionUID = 1L;

    public ClientUI() {
        this.getBillCardPanel().setAutoExecHeadEditFormula(true);
    }

    public ClientUI(FramePanel fp) {
        super(fp);
        this.getBillCardPanel().setAutoExecHeadEditFormula(true);

        try {
            this.initTransType();
            this.setBillOperate(4);
        } catch (Exception var3) {
            Logger.error("构造方法出现错误！", var3);
            var3.printStackTrace();
        }

    }
    
    @Override
    protected void initPrivateButton() {
    	super.initPrivateButton();
    	initAddGroupBtn();
    }
    /**
     * create by HMX 2019-3-29
     * 添加一个按钮组
     * */
    private void initAddGroupBtn(){
    	addPrivateButton(new RefPoOrderBtnVO().getButtonVO());
    	ButtonVO addGroupBtnVO = new AddGroupBtnVO().getButtonVO();
    	addGroupBtnVO.setChildAry(new int[]{RefPoOrderBtnVO.REFPOORDER_BTN_CODE});//1代表增加
    	addPrivateButton(addGroupBtnVO);
    }
    
    
    private void initTransType() throws BusinessException {
        String moudelcode = this.getModuleCode();
        if (moudelcode != null && !moudelcode.equals("H5H28001")) {
            String sqlwhere = " parentbilltype='9908'  and nodecode='" + moudelcode + "' and isnull(dr,0)=0";

            try {
                BilltypeVO[] vos = (BilltypeVO[])((BilltypeVO[])JZPMProxy.getIJZPMPubBusi().queryByClause(BilltypeVO.class, sqlwhere, "ts"));
                if (vos != null && vos.length > 0 && vos[0].getPk_billtypecode() != null) {
                    this.setvtranstype(vos[0].getPk_billtypecode());
                }

            } catch (Exception var5) {
                Logger.error("初始化交易类型失败!", var5);
                throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H5H28001", "UJZH5H28001-8000003"));
            }
        }
    }

    private void ResertPanel() {
        String moudelcode = this.getModuleCode();
        if (moudelcode != null && !moudelcode.equals("H5H28001")) {
            String sqlwhere = " parentbilltype='9908'  and nodecode='" + moudelcode + "' and isnull(dr,0)=0";

            try {
                BilltypeVO[] vos = (BilltypeVO[])((BilltypeVO[])JZPMProxy.getIJZPMPubBusi().queryByClause(BilltypeVO.class, sqlwhere, "ts"));
                if (vos != null && vos.length > 0 && vos[0].getPk_billtypecode() != null) {
                    this.setCardPanel(vos[0].getPk_billtypecode());
                }
            } catch (Exception var5) {
                var5.printStackTrace();
            }
        }
    }

    protected void initComboBox() {
        super.initComboBox();
        this.getBillCardWrapper().initHeadComboBox("vbillstatus", IBillStatus.strStateRemark, true);
        this.initHeadComboBox("vbillstatus", IBillStatus.strStateRemark, true);
    }

    public void initHeadComboBox(String key, Object[] values, boolean isWhithIndex) {
        if (key != null && values != null) {
            try {
                BillItem billItem = this.getBillListPanel().getHeadItem(key);
                if (billItem != null && billItem.getDataType() == 6) {
                    UIComboBox cmb = (UIComboBox)billItem.getComponent();
                    cmb.removeAllItems();

                    for(int i = 0; i < values.length; ++i) {
                        cmb.addItem(values[i]);
                    }

                    billItem.setWithIndex(isWhithIndex);
                }
            } catch (Exception var7) {
                var7.printStackTrace();
            }

        }
    }

    protected AbstractManageController createController() {
        return new ClientCtrl();
    }

    protected ManageEventHandler createEventHandler() {
        return new ClientEventHandler(this, this.getUIControl());
    }

    public void setBodySpecialData(CircularlyAccessibleValueObject[] vos) throws Exception {
    }

    protected void setHeadSpecialData(CircularlyAccessibleValueObject vo, int intRow) throws Exception {
    }

    protected void setTotalHeadSpecialData(CircularlyAccessibleValueObject[] vos) throws Exception {
    }

    protected void initSelfData() {
        this.initComboBox();
        super.initSelfData();
    }

    public void setDefaultData() throws Exception {
        this.getBillCardPanel().setHeadItem("vtranstype", this.getvtranstype());
        this.getBillCardPanel().setHeadItem("vmodulecode", this.getModuleCode());
        this.getBillCardPanel().setHeadItem("pk_corp", this.getCorpPrimaryKey());
        this.getBillCardPanel().setHeadItem("dbilldate", BillDateGetter.getBillDate());
        this.getBillCardPanel().setHeadItem("pk_billtype", this.getUIControl().getBillType());
        this.getBillCardPanel().setHeadItem("vbillstatus", new Integer(8));
        this.getBillCardPanel().setTailItem("voperatorid", this._getOperator());
        this.getBillCardPanel().setTailItem("dmakedate", BillDateGetter.getMakeDate());
    }

    public void afterEdit(BillEditEvent e) {
        super.afterEdit(e);
        this.getBillCardPanel().execHeadEditFormulas();
    }

    public void updateListVo() throws Exception {
        super.updateListVo();
    }

    public void doApproveAction(ILinkApproveData approvedata) {
        this.reloadTempletForDoApproveAction(approvedata);
        super.doApproveAction(approvedata);
    }
    
    

    private void reloadTempletForDoApproveAction(ILinkApproveData approvedata) {
        try {
            BdDeftableVO vo = (BdDeftableVO)JZProxy.getIJZUifService().queryByPrimaryKeyAtJZ(BdDeftableVO.class, approvedata.getBillID());
            this.setCardPanel(vo.getVtranstype());
        } catch (Exception var3) {
            Logger.error(var3.getMessage(), var3);
        }

    }
}
