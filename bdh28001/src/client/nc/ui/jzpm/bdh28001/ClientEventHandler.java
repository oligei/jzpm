//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.bdh28001;

import java.util.ArrayList;

import nc.ui.jzpm.bdh28001.refpooder.RefPoOrderBtnVO;
import nc.ui.jzpm.pub.eventhandler.ManageEventHandler;
import nc.ui.pub.ButtonObject;
import nc.ui.pub.ClientEnvironment;
import nc.ui.pub.pf.PfUtilClient;
import nc.ui.pub.workflownote.FlowStateDlg;
import nc.ui.scm.pub.sourceref.SourceRefDlg;
import nc.ui.tbm.tbm_010.SouthPanel;
import nc.ui.trade.controller.IControllerBase;
import nc.ui.trade.manage.BillManageUI;
import nc.vo.jcom.lang.StringUtil;
import nc.vo.ml.NCLangRes4VoTransl;
import nc.vo.pub.AggregatedValueObject;
import nc.vo.pub.BusinessException;
import nc.vo.pub.CircularlyAccessibleValueObject;
import nc.vo.pub.SuperVO;
import nc.vo.rc.receive.ArriveorderVO;
import nc.vo.trade.pub.IExAggVO;

public class ClientEventHandler extends ManageEventHandler {
    public ClientEventHandler(BillManageUI billUI, IControllerBase control) {
        super(billUI, control);
    }

    private ClientUI getClientUI() {
        return (ClientUI)this.getBillUI();
    }

    public void onBoAdd(ButtonObject bo) throws Exception {
        super.onBoAdd(bo);
    }

    protected void onBoLineAdd() throws Exception {
        super.onBoLineAdd();
    }

    public String getBufferVtranstype() throws BusinessException {
        if (this.getBufferData() != null && this.getBufferData().getCurrentRow() >= 0) {
            Object retObj = this.getBufferData().getCurrentVO().getParentVO().getAttributeValue("vtranstype");
            if (retObj != null && retObj.toString().trim().length() != 0) {
                return retObj.toString();
            } else {
                throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H5H28001", "UJZH5H28001-8000001"));
            }
        } else {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H5H28001", "UJZH5H28001-8000001"));
        }
    }

    protected void onBoCancelAudit() throws Exception {
        AggregatedValueObject modelVo = this.getBufferData().getCurrentVOClone();
        this.setCheckManAndDate(modelVo);
        if (this.checkVOStatus(modelVo, new int[]{8})) {
            System.out.println("无效的鼠标处理机制");
        } else {
            this.beforeOnBoAction(27, modelVo);
            AggregatedValueObject retVo = this.getBusinessAction().unapprove(modelVo, this.getBufferVtranstype(), this.getBillUI()._getDate().toString(), this.getBillUI().getUserObject());
            if (retVo.getParentVO() != null) {
                modelVo.getParentVO().setAttributeValue("ts", retVo.getParentVO().getAttributeValue("ts"));
                modelVo.getParentVO().setAttributeValue(this.getBillManageUI().getBillField().getField_BillStatus(), retVo.getParentVO().getAttributeValue(this.getBillManageUI().getBillField().getField_BillStatus()));
                modelVo.getParentVO().setAttributeValue(this.getBillManageUI().getBillField().getField_CheckNote(), retVo.getParentVO().getAttributeValue(this.getBillManageUI().getBillField().getField_CheckNote()));
                modelVo.getParentVO().setAttributeValue(this.getBillManageUI().getBillField().getField_CheckMan(), retVo.getParentVO().getAttributeValue(this.getBillManageUI().getBillField().getField_CheckMan()));
            }

            if (PfUtilClient.isSuccess()) {
                this.afterOnBoAction(27, modelVo);
                Integer intState = (Integer)modelVo.getParentVO().getAttributeValue(this.getBillField().getField_BillStatus());
                if (intState == 8) {
                    modelVo.getParentVO().setAttributeValue(this.getBillField().getField_CheckMan(), (Object)null);
                    modelVo.getParentVO().setAttributeValue(this.getBillField().getField_CheckDate(), (Object)null);
                }

                this.updateHeadFixFields(modelVo, retVo, this.getFixFields(27, retVo.getParentVO()));
            }

        }
    }

    @SuppressWarnings("unchecked")
	protected void onBoCommit() throws Exception {
        AggregatedValueObject modelVo1 = this.getBufferData().getCurrentVOClone();
        String operator = this.getBillUI()._getOperator();
        String maker = modelVo1.getParentVO().getAttributeValue(this.getBillField().getField_Operator()).toString();
        if (operator != null && maker != null && maker.equals(operator)) {
            AggregatedValueObject modelVo = this.getBufferData().getCurrentVOClone();
            modelVo.getParentVO().setAttributeValue(this.getBillField().getField_Operator(), this.getBillUI()._getOperator());
            this.beforeOnBoAction(28, modelVo);
            String strTime = ClientEnvironment.getServerTime().toString();
            ArrayList retList = this.getBusinessAction().commit(modelVo, this.getBufferVtranstype(), this.getBillUI()._getDate().toString() + strTime.substring(10), this.getBillUI().getUserObject());
            if (PfUtilClient.isSuccess()) {
                Object o = retList.get(1);
                if (o instanceof AggregatedValueObject) {
                    AggregatedValueObject retVo = (AggregatedValueObject)o;
                    this.afterOnBoAction(28, retVo);
                    this.updateHeadFixFields(modelVo, retVo, this.getFixFields(28, retVo.getParentVO()));
                }
            }

        } else {
            throw new BusinessException(NCLangRes4VoTransl.getNCLangRes().getStrByID("H5H28001", "UJZH5H28001-8000002"));
        }
    }

    public void onBoAudit() throws Exception {
        AggregatedValueObject modelVo = this.getBufferData().getCurrentVOClone();
        this.setCheckManAndDate(modelVo);
        if (this.checkVOStatus(modelVo, new int[]{1})) {
            System.out.println("无效的鼠标处理机制");
        } else {
            this.beforeOnBoAction(26, modelVo);
            AggregatedValueObject retVo = this.getBusinessAction().approve(modelVo, this.getBufferVtranstype(), this.getBillUI()._getDate().toString(), this.getBillUI().getUserObject());
            if (PfUtilClient.isSuccess()) {
                this.afterOnBoAction(26, modelVo);
                this.updateHeadFixFields(modelVo, retVo, this.getFixFields(26, retVo.getParentVO()));
            }

        }
    }

    private void setCheckManAndDate(AggregatedValueObject vo) throws Exception {
        vo.getParentVO().setAttributeValue(this.getBillField().getField_CheckDate(), this.getBillUI()._getDate());
        vo.getParentVO().setAttributeValue(this.getBillField().getField_CheckMan(), this.getBillUI()._getOperator());
    }

    @SuppressWarnings("unused")
	private CircularlyAccessibleValueObject[] getChildVO(AggregatedValueObject retVo) {
        CircularlyAccessibleValueObject[] childVos = null;
        if (retVo instanceof IExAggVO) {
            childVos = ((IExAggVO)retVo).getAllChildrenVO();
        } else {
            childVos = retVo.getChildrenVO();
        }

        return childVos;
    }

    /**
     * 添加自定义按钮事件
     * create by HMX 2019-6-3
     * */
    @Override
    protected void onBoElse(int intBtn) throws Exception {
    	super.onBoElse(intBtn);
    	switch (intBtn) {
		case RefPoOrderBtnVO.REFPOORDER_BTN_CODE:
			RefPoOrderBtnAction();
			break;

		default:
			break;
		}
    }
    /**
     * 参照采购订单
     * create by HMX 2019-6-3
     * */
    public void RefPoOrderBtnAction(){
    	ClientUI ui = this.getClientUI();
    	 ButtonObject bo = this.getClientUI().getButtonManager().getButton(RefPoOrderBtnVO.REFPOORDER_BTN_CODE);
    	 bo.setTag("21:1012A11000000009OEHO");
		SourceRefDlg.childButtonClicked(bo, getCorpPrimaryKey(ui), "H5H280XX02", getOperatorId(ui), "990898", ui);
         if (SourceRefDlg.isCloseOK()) {
        	 AggregatedValueObject[]  vos = SourceRefDlg.getRetsVos();
        	 System.out.println(vos.length);
    	         //onExitFrmOrd(retVOs);
          }
    }
    
    public String getOperatorId(ClientUI ui){
        String operatorid = ui.getEnvironment().getUser().getPrimaryKey();
         if ((operatorid == null) || (operatorid.trim().equals("")) || (operatorid.equals("88888888888888888888"))) {
           operatorid = "10013488065564590288";
         }
         return operatorid;
     }
    
    protected String getCorpPrimaryKey(ClientUI ui){
         return ui.getEnvironment().getCorporation().getPrimaryKey();
    }
    
    protected void onBoQuery() throws Exception {
        StringBuffer strWhere = new StringBuffer();
        if (this.askForQueryCondition(strWhere)) {
            String sqlWhere = "";
            if (strWhere.length() == 0) {
                sqlWhere = "vmodulecode='" + this.getBillUI().getModuleCode() + "' ";
            } else {
                sqlWhere = strWhere.toString() + " and vmodulecode='" + this.getBillUI().getModuleCode() + "' ";
            }

            SuperVO[] queryVos = this.queryHeadVOs(sqlWhere);
            this.getBufferData().clear();
            this.addDataToBuffer(queryVos);
            this.updateBuffer();
        }
    }

    public void onBoApproveInfo() throws Exception {
        if (this.getBufferData().getCurrentVO() != null) {
            String strBilltype = "vtranstype";
            String billType = (String)this.getBufferData().getCurrentVO().getParentVO().getAttributeValue(strBilltype);
            String billId = this.getBufferData().getCurrentVO().getParentVO().getPrimaryKey();
            if (StringUtil.isEmptyWithTrim(billType)) {
                billType = this.getUIController().getBillType();
            }

            FlowStateDlg dlg = new FlowStateDlg(this.getBillUI().getParent(), billType, "KHHH0000000000000001", billId);
            dlg.setVisible(true);
        }
    }
}
