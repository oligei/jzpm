package nc.ui.jzpm.bdh28001.refpooder;

import nc.ui.jzpm.pub.buttonstate.JZPMButtonVO;
import nc.ui.trade.base.IBillOperate;
import nc.vo.trade.button.ButtonVO;

public class RefPoOrderBtnVO {

	public static final int REFPOORDER_BTN_CODE = 21;

	JZPMButtonVO btnVO = null;
	 
	  public ButtonVO getButtonVO() {
	     if (btnVO == null) {
	      btnVO = new JZPMButtonVO();
	       btnVO.setBtnNo(REFPOORDER_BTN_CODE);
	       btnVO.setBtnName("采购订单");
	       btnVO.setHintStr("采购订单");
	       btnVO.setBtnChinaName("采购订单");
	       btnVO.setOperateStatus(new int[] { IBillOperate.OP_ALL, 4 });
	     }
	      return btnVO;
	   }
}
