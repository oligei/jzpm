package nc.ui.jzpm.bdh28001.refpooder;

import nc.ui.jzpm.pub.buttonstate.JZPMButtonVO;
import nc.ui.trade.base.IBillOperate;
import nc.vo.trade.pub.IBillStatus;

/**
 * create by HMX 2019-3-29
 * 按钮组定义
 * */
public class AddGroupBtnVO {
	public static final int ADDGROUP_BTN_CODE = 95;
	
	 JZPMButtonVO btnVO = null;
	 
	 public JZPMButtonVO getButtonVO() {
	     if (btnVO == null) {
	      btnVO = new JZPMButtonVO();
	       btnVO.setBtnNo(ADDGROUP_BTN_CODE);
	       btnVO.setBtnName("新增");
	       btnVO.setHintStr("新增");
	       btnVO.setBtnChinaName("新增");
	       btnVO.setOperateStatus(new int[] { IBillOperate.OP_ALL});
	       btnVO.setBusinessStatus(new int[]{IBillStatus.ALL});//数据状态
	     }
	      return btnVO;
	   }

}
