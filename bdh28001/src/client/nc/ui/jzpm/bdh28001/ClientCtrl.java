//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package nc.ui.jzpm.bdh28001;

import nc.ui.jzpm.bdh28001.refpooder.AddGroupBtnVO;
import nc.ui.jzpm.bdh28001.refpooder.RefPoOrderBtnVO;
import nc.ui.jzpm.pub.buttonsort.JZPMPubButtonSort;
import nc.ui.trade.bill.AbstractManageController;
import nc.vo.jzpm.bdh28001.BdDeftableBVO;
import nc.vo.jzpm.bdh28001.BdDeftableVO;
import nc.vo.trade.pub.HYBillVO;

public class ClientCtrl extends AbstractManageController {
    public String[] m_BillVoName = new String[]{HYBillVO.class.getName(), BdDeftableVO.class.getName(), BdDeftableBVO.class.getName()};
    public String m_pk_billtype = "9908";

    public ClientCtrl() {
    }

    public String[] getCardBodyHideCol() {
        return null;
    }

    public int[] getCardButtonAry() {
        return JZPMPubButtonSort.getInstance().sort(new int[]{1, 3, 0, 7, 10, 4, 25, 511, 16, 8, 5, 31, 20, 6,27});
    }

    public boolean isShowCardRowNo() {
        return true;
    }

    public boolean isShowCardTotal() {
        return true;
    }

    public String getBillType() {
        return this.m_pk_billtype;
    }

    public void setBillType(String pk_billtype) {
        this.m_pk_billtype = pk_billtype;
    }

    public String[] getBillVoName() {
        return this.m_BillVoName;
    }

    public String getBodyCondition() {
        return null;
    }

    public String getBodyZYXKey() {
        return null;
    }

    public int getBusinessActionType() {
        return 0;
    }

    public String getChildPkField() {
        return "pk_parent";
    }

    public String getHeadZYXKey() {
        return null;
    }

    public String getPkField() {
        return "pk_parent";
    }

    public Boolean isEditInGoing() throws Exception {
        return null;
    }

    public boolean isExistBillStatus() {
        return true;
    }

    public boolean isLoadCardFormula() {
        return true;
    }

    public String[] getListBodyHideCol() {
        return null;
    }

    public int[] getListButtonAry() {
        return JZPMPubButtonSort.getInstance().sort(new int[]{1,3, 4, 25, 511, 8, 16, 5, 30,AddGroupBtnVO.ADDGROUP_BTN_CODE});
    }

    public String[] getListHeadHideCol() {
        return null;
    }

    public boolean isShowListRowNo() {
        return true;
    }

    public boolean isShowListTotal() {
        return true;
    }
}
